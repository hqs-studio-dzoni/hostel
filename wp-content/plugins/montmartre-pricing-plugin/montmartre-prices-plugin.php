<?php
/*
Plugin Name: Montmatre Prices Settings
Plugin URI: http://pixel2go.com
Description: Simple prices change plugin
Author: Nikola Vojvodic
Author URI: http://pixel2go.com
*/

// create custom plugin settings menu
add_action('admin_menu', 'mont_create_menu');

function mont_create_menu() {

	//create new top-level menu
	add_menu_page('Montmartre Prices', 'Prices Settings', 'administrator', __FILE__, 'mont_settings_page',plugins_url('/images/settings2.png', __FILE__));//Icon by Designmodo


	//call register settings function
	add_action( 'admin_init', 'register_mysettings' );
}

function register_mysettings() {
	//register our settings
	register_setting( 'mont-settings-group', '8_bed' );
	register_setting( 'mont-settings-group', '6_bed' );
	register_setting( 'mont-settings-group', '4_bed' );
	register_setting( 'mont-settings-group', 'private_twin_triple' );
	register_setting( 'mont-settings-group', 'mont_for_one' );
	register_setting( 'mont-settings-group', 'mont_for_two' );
}

function mont_settings_page() {
?>

<div class="wrap">
<h2>Montmartre Prices Settings</h2>
<p><i>Enter desired prices for the rooms and click "Save Changes" button.</i></p>
<form method="post" action="options.php">
    <?php settings_fields( 'mont-settings-group' ); ?>
    <table class="form-table">
        <tr valign="top">
	        <th scope="row">Paul Gauguin 8-bed mixed dorm</th>
	        <td><input type="text" name="8_bed" value="<?php echo get_option('8_bed'); ?>" /></td>
        </tr>
         
        <tr valign="top">
	        <th scope="row">Toulouse Lautrec 6-bed mixed dorm</th>
	        <td><input type="text" name="6_bed" value="<?php echo get_option('6_bed'); ?>" /></td>
        </tr>
        
        <tr valign="top">
	        <th scope="row">Van Gogh 4-bed mixed dorm</th>
	        <td><input type="text" name="4_bed" value="<?php echo get_option('4_bed'); ?>" /></td>
        </tr>

        <tr valign="top">
	        <th scope="row">Pablo Picasso private twin/triple room</th>
	        <td><input type="text" name="private_twin_triple" value="<?php echo get_option('private_twin_triple'); ?>" /></td>
        </tr>
        <tr valign="top">
	        <th scope="row">Montmartre En Suite for one person</th>
	        <td><input type="text" name="mont_for_one" value="<?php echo get_option('mont_for_one'); ?>" /></td>
        </tr>
        <tr valign="top">
	        <th scope="row">Montmartre En Suite for two persons</th>
	        <td><input type="text" name="mont_for_two" value="<?php echo get_option('mont_for_two'); ?>" /></td>
        </tr>
    </table>
    <p class="submit">
    <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
    </p>
</form>
<?php if( isset($_GET['settings-updated']) ) { ?>
    <div id="message" class="updated">
        <p><strong><?php _e('Prices settings saved.') ?></strong></p>
    </div>
<?php } ?>
</div>
<?php } 
//add shortcode for the prices
function montmartre_prices_shortcode( $theme_option ) {
	return get_option( $theme_option['key'] );
}
?>