<?php  
$template_directory = get_bloginfo('template_directory');
get_header();
?>

<div class="middle">
<div class="container padding50px">

	<?php 
		if ( get_post_meta($post->ID, PREFIX.'_page_sidebar_position', true)==1 ) { $content = 'suffix_1'; $sidebar = ''; }
	elseif ( get_post_meta($post->ID, PREFIX.'_page_sidebar_position', true)==2 ) { $content = ''; $sidebar = 'suffix_1'; }
	elseif ( get_option(PREFIX.'_sidebar_position')==1 ) { $content = ''; $sidebar = 'suffix_1'; }
	else { $content = 'suffix_1'; $sidebar = ''; }
	?>
	
	<!-- middle content -->
	<div class="container_24">
	
		<?php if ( $sidebar!=='' ) { ?>
			<!-- sidebar -->
			<div class="grid_6 <?php echo $sidebar; ?>">
				<?php get_sidebar(); ?>
			</div>
			<!--/ sidebar -->
		<?php } ?>

		<!-- content -->
		<div class="grid_17 <?php echo $content; ?>">
		  <div class="text">
				<div class="post-title">
					<h1><?php _e('404 Error', 'tfuse') ?></h1>
				</div>					
				<div class="entry">				
					<p><?php _e('Page not found', 'tfuse') ?></p>
					<p><?php _e('The page you were looking for doesn&rsquo;t seem to exist', 'tfuse') ?>.</p>
				</div>
		  </div>
		</div>
		<!--/ content -->

		<?php if ( $content!='' ) { ?>
			<!-- sidebar -->
			<div class="grid_6 <?php echo $sidebar; ?>">
				<?php get_sidebar(); ?>
			</div>
			<!--/ sidebar -->
		<?php } ?>
	
		<div class="clear"></div>
	</div>
	<!--/ middle content -->
    
<?php get_footer(); ?>