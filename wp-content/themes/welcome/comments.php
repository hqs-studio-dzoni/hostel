<?php
	
// Do not delete these lines

if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
	die ('Please do not load this page directly. Thanks!');

if ( post_password_required() ) { ?>
	<p class="nocomments"><?php _e('This post is password protected. Enter the password to view comments.', 'tfuse') ?></p>

<?php return; } ?>

<?php $comments_by_type = &separate_comments($comments); ?>    

<!-- You can start editing here. -->

<div class="comment-list" id="comments">

<?php if ( have_comments() ) : ?>

	<?php if ( ! empty($comments_by_type['comment']) ) : ?>
                            
       	<h2><?php _e('Comments', 'tfuse') ?></h2>

		<ol>
	
			<?php wp_list_comments('avatar_size=48&callback=custom_comment&type=comment'); ?>
		
		</ol>    

		<!--
		<div class="navigation">
			<div class="fl"><?php previous_comments_link() ?></div>
			<div class="fr"><?php next_comments_link() ?></div>
			<div class="fix"></div>
		</div>-->
	<?php endif; ?>
		    
	<?php if ( ! empty($comments_by_type['pings']) ) : ?>
    	
		<!--	
        <h3 id="pings"><?php _e('Trackbacks/Pingbacks', 'tfuse') ?></h3>
    
        <ol class="pinglist">
            <?php wp_list_comments('type=pings&callback=list_pings'); ?>
        </ol>
		-->
    	
	<?php endif; ?>
    	
<?php else : // this is displayed if there are no comments so far ?>

		<?php if ('open' == $post->comment_status) : ?>
			<!-- If comments are open, but there are no comments. -->
			<p class="nocomments"><?php _e('No comments yet.', 'tfuse') ?></p>

		<?php else : // comments are closed ?>
			<!-- If comments are closed. -->
			<p class="nocomments"><?php _e('Comments are closed.', 'tfuse') ?></p>

		<?php endif; ?>

<?php endif; ?>

</div> <!-- /#comments_wrap -->

<?php if ('open' == $post->comment_status) : ?>

<div id="respond">

	<div class="contact-form" id="addcomments">
	
 			<h2><?php _e('ADD YOUR COMMENT', 'tfuse') ?></h2>
 		
			<div class="cancel-comment-reply">
				<small><?php cancel_comment_reply_link(); ?></small>
			</div><!-- /.cancel-comment-reply -->
	
			<?php if ( get_option('comment_registration') && !$user_ID ) : //If registration required & not logged in. ?>
		
				<p><?php _e('You must be', 'tfuse') ?> <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php echo urlencode(get_permalink()); ?>"><?php _e('logged in', 'tfuse') ?></a> <?php _e('to post a comment.', 'tfuse') ?></p>
		
			<?php else : //No registration required ?>
			
				<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" name="commentform" id="commentform">
		
				<?php if ( $user_ID ) : //If user is logged in ?>
		
					<p><?php _e('Logged in as', 'tfuse') ?> <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(); ?>" title="<?php _e('Log out of this account', 'tfuse') ?>"><?php _e('Logout', 'tfuse') ?> &raquo;</a></p>
		
				<?php else : //If user is not logged in ?>
				
					<div class="row field_text alignleft">
						<label for="author"><?php _e('Name', 'tfuse') ?> <?php if ($req) echo "*"; ?></label><br />
						<input type="text" name="author" class="inputtext input_middle required" id="author" size="40" value="<?php echo $comment_author; ?>" tabindex="1" />
					</div>
		
					<div class="row field_text alignleft">
						<label for="email"><?php _e('Email', 'tfuse') ?> <?php if ($req) echo '*'; ?></label><br />
						<input type="text" name="email" class="inputtext input_middle required" id="email" size="40" value="<?php echo $comment_author_email; ?>" tabindex="2" />
					</div>
					<div class="clear"></div>

					<div style="display:none;" class="row">
						<label for="url"><?php _e('Website', 'tfuse') ?></label>
						<input type="text" name="url" class="inputtext input_middle required" id="url" value="<?php echo $comment_author_url; ?>" tabindex="3" />
					</div>
		
				<?php endif; // End if logged in ?>
		
				<!--<p><strong>XHTML:</strong> <?php _e('You can use these tags', 'tfuse'); ?>: <?php echo allowed_tags(); ?></p>-->
		
				<div class="row field_textarea">
					<label><?php _e('Message', 'tfuse') ?> *</label><br />
					<textarea name="comment" class="textarea textarea_middle required" id="comment" cols="40" rows="10" tabindex="4"></textarea>
				</div>
				<div class="clear"></div>

				<div class="row field_submit">
					<span class="reset-link"><a href="#" onclick="document.commentform.reset();return false"><?php _e('reset all fields', 'tfuse'); ?></a></span>
					<input name="submit" type="submit" id="submit" class="contact-submit submit" tabindex="5" value="<?php _e('Submit Comment', 'tfuse') ?>" />
					<input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />
				</div>
				
				<?php comment_id_fields(); ?>
				<?php do_action('comment_form', $post->ID); ?>
		
				</form><!-- /#commentform -->
		
			<?php endif; // If registration required ?>
			
	</div>

</div><!-- /#respond -->

<?php endif; // if you delete this the sky will fall on your head ?>
