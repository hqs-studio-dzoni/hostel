    <div class="newsletter_box">
        <h3><?php _e('Sign up for Newsletter', 'tfuse') ?>:</h3>
		<?php if ( $_POST['newsletter']!='' ) { ?>
		<h2><?php _e('Thank you for your subscription.', 'tfuse') ?></h2>
		<?php } else { ?>
        <form action="#" method="post">
        	<input type="text" value="<?php _e('Enter your e-mail address', 'tfuse') ?>" onfocus="if (this.value == '<?php _e('Enter your e-mail address', 'tfuse') ?>') {this.value = '';}" onblur="if (this.value == '') {this.value = '<?php _e('Enter your e-mail address', 'tfuse') ?>';}" name="newsletter" class="inputField" />
			<input type="submit" value="<?php _e('Submit', 'tfuse') ?>" class="btn-submit" />
        </form>
		<?php } ?>
        <div class="clear"></div>
    </div>
</div>
</div>

<div class="footer">
	<div class="container_24">    
		<?php if ( get_option(PREFIX.'_enable_footer_shortcodes')!="true" ) { ?>
			
			<div class="col_1_4 col">
				<div class="inner">
					<?php dynamic_sidebar('Footer 1'); ?>
				</div>
			</div>
			
			<div class="col_1_4 col">
				<div class="inner">
					<?php dynamic_sidebar('Footer 2'); ?>
				</div>
			</div>
			
			<div class="col_1_4 col">
				<div class="inner">
					<?php dynamic_sidebar('Footer 3'); ?>
				</div>
			</div>
			
			<div class="col_1_4 col">
				<div class="inner">
					<?php dynamic_sidebar('Footer 4'); ?>
				</div>
			</div>
			<div class="divider_space"></div>
		
		<?php } else {
				//footer area for shortcodes
				$footer_shortcodes = html_entity_decode(get_option(PREFIX.'_footer_shortcodes'), ENT_QUOTES, 'UTF-8'); 
				echo $footer_shortcodes = apply_filters('themefuse_shortcodes',$footer_shortcodes);
			}					
		?>			
		<div class="clear"></div>
	</div>
</div>

<?php $template_directory = get_bloginfo('template_directory'); ?>
<!-- SyntaxHighlighter -->
<script type="text/javascript" language="javascript" src="<?php echo $template_directory ?>/js/shCore.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo $template_directory ?>/js/shBrushPlain.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $template_directory ?>/css/shCore.css"  />
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $template_directory ?>/css/shThemeDefault.css" />
<!--/ SyntaxHighlighter -->
<!-- Code SyntaxHighlighter -->
<script type="text/javascript">
SyntaxHighlighter.defaults['gutter'] = false;
SyntaxHighlighter.defaults['toolbar'] = true;
SyntaxHighlighter.all();
</script>
<!--/ Code SyntaxHighlighter -->

<?php wp_footer(); ?>
<?php $analitycs = get_option(PREFIX.'_google_analytics'); if ( $analitycs <> '' ) { echo html_entity_decode($analitycs,ENT_QUOTES, 'UTF-8'); } ?>

</body>
</html>