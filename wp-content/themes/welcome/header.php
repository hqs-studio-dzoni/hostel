<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	
	<title><?php wp_title() ?></title>

	<?php global $template; $template_directory = get_bloginfo('template_directory'); ?>
	<link href="<?php bloginfo('stylesheet_url') ?>?v=3" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $template_directory ?>/styles/default.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo $template_directory ?>/styles/responsive.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo $template_directory ?>/styles/uniform.default.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>

    <?php wp_head(); ?>
	
	<script type="text/javascript" src="<?php echo $template_directory ?>/js/jquery.min.js"></script>
	<script type="text/javascript">
		$(function(){
			$(".widget_recent_entries li:even").addClass("even");
	  		$(".table-price tbody tr:odd").addClass("odd");
		});
	</script>
	<link rel="stylesheet" type="text/css" href="<?php echo $template_directory ?>/css/wi-theme/jquery-ui-1.8.9.custom.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $template_directory ?>/css/ui.selectmenu.css" />
	<script type="text/javascript" language="javascript" src="<?php echo $template_directory ?>/js/jquery-ui-1.8.9.custom.min.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo $template_directory ?>/js/ui.selectmenu.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo $template_directory ?>/js/styled.selectmenu.js"></script>
	<script type="text/javascript" src="<?php echo $template_directory ?>/js/slides.jquery.js"></script>
	<link rel="stylesheet" href="<?php echo $template_directory ?>/css/prettyPhoto.css" type="text/css" media="screen" charset="utf-8" />
	<script src="<?php echo $template_directory ?>/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" charset="utf-8">
			$(document).ready(function(){
				$("a[rel^='prettyPhoto']").prettyPhoto({animationSpeed:'slow', slideshow:3000, autoplay_slideshow: true});
			});
	</script>
	<link rel="stylesheet" type="text/css" href="<?php echo $template_directory ?>/css/slides.css" />
	<script type="text/javascript">
			$(function(){
				$('#slides').slides({
					width: 960,
					height: 379,
					preload: true,
					preloadImage: '<?php echo $template_directory ?>/images/loading.gif',
					play: 5000,
					pause: 2500,
					effect: 'fade, fade',
					hoverPause: true,
					animationStart: function(){
						$('.caption').animate({
							bottom:-96
						},100);
					},
					animationComplete: function(current){
						$('.caption').animate({
							bottom:0
						},200);
					}
				});
				
			});
	</script>
	
	<script type="text/javascript" language="javascript" src="<?php echo $template_directory ?>/js/jcarousellite_1.3.min.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo $template_directory ?>/js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo $template_directory ?>/js/custom.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo $template_directory ?>/js/general.js"></script>
	
	<script type="text/javascript" language="javascript" src="<?php echo $template_directory ?>/js/jquery.nivo.slider.js"></script>
	<script type="text/javascript" src="<?php echo $template_directory ?>/js/anythingSlider.js"></script>
        <script type="text/javascript" language="javascript" src="<?php echo $template_directory ?>/js/jquery.uniform.js"></script>
	<script type="text/javascript" charset="utf-8">
	$(document).ready(function(){
         $(".styleme").uniform();
    	$('.slideshow').anythingSlider({
    	        easing: "easeInOutExpo",
    	        autoPlay: false,
    	        startStopped: false,
    	        animationTime: 600,
    	        hashTags: false,
    	        buildNavigation: true,
    	        buildArrows: false,
    			pauseOnHover: true,
    			startText: "Go",
    	        stopText: "Stop"
    	    });
    	    
	});

		
		$(document).ready(function() {
			$(".minigallery").jCarouselLite({
				btnNext: ".next",
				btnPrev: ".prev",
				scroll: 2,
				<?php if ($template=='full_width')  { ?>
				visible: 6,
				<?php } else { ?>
				visible: 4,
				<?php } ?>
				speed: 400,
				mouseWheel: true,
				circular:false,
				easing: "easeInOutCubic"
			});
		
			$(".minigallery a[rel^='prettyPhoto']").prettyPhoto({theme:'facebook',overlay_gallery: false});
		});
	</script>
	<script type="text/javascript">
	// <![CDATA[
	$(function(){
	  var bookedDays = ["2011-2-22","2011-2-23","2011-3-8","2011-3-9","2011-3-10"];
	  
	  function assignCalendar(id){
		$('<div class="calendar" />')
		  .insertAfter( $(id) )
		  .datepicker({ 
			dateFormat: 'dd-mm-yy', 
			minDate: new Date(), 
			maxDate: '+1y', 
			altField: id, 
			firstDay: 1,
			showOtherMonths: true,
			dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
			beforeShowDay: isAvailable })
		  .prev().hide();
	  }
	  
	  function isAvailable(date){
		var dateAsString = date.getFullYear().toString() + "-" + (date.getMonth()+1).toString() + "-" + date.getDate();
		var result = $.inArray( dateAsString, bookedDays ) ==-1 ? [true] : [false];
		return result
	  }
	
	  assignCalendar('#date_in_input');
	  assignCalendar('#date_out_input');
	});
	// ]]>
	</script>

    <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php if ( get_option(PREFIX.'_feedburner_url') <> "" ) { echo get_option(PREFIX.'_feedburner_url'); } else { echo get_bloginfo_rss('rss2_url'); } ?>" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
</head>
<body>
<div class="head">
  <div class="container">
  	<div class="logo"><a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('description'); ?>"><?php $logo = get_option(PREFIX.'_logo');  if ( $logo == '' ) { $logo = $template_directory . '/images/logo.png'; } ?><img src="<?php echo $logo ?>" alt="<?php bloginfo('name'); ?>" /></a></div>                                                                                                        
	<div class="head_right">
	<?php 
		$header_shortcodes = html_entity_decode(get_option(PREFIX.'_header_shortcodes'), ENT_QUOTES, 'UTF-8'); 
		echo $header_shortcodes = apply_filters('themefuse_shortcodes',$header_shortcodes);
	?>
	</div>
  </div>
</div>
<!-- topmenu -->
<div class="menu-header">
  <div class="container">
	<?php include( THEME_MODULES . '/page-nav.php' ); ?>
  </div>
</div>
<!--/ topmenu -->
