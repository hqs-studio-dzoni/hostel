<?php get_header(); ?>

<div class="middle">
<div class="container padding50px">

    <!-- middle content -->
    <div class="container_24">
    	
        <!-- content -->
    	<div class="grid_17 suffix_1">
       	  <div class="text">
            
				<h2 class="title_border">Setting up the Homepage</h2>

				<p>The homepage of the Welcome Inn theme was created with flexibility in mind, so you could customize it any way you'd like. It can be set up as 5 different types of pages:</p>

					<ul>
					<li> Page with left or right sidebar</li>
					<li>Reservation Page</li>
					<li> Full Width Page</li>
					<li> Contact Page</li>
					<li> Blog Page</li>
					</ul>

				<h2>Let's get started</h2>

				<p>
					The first step is to create a page to serve as your new home page. For the purpose of this example, we'll call it "My New Homepage"

			<ul>
					<li>In your WP control panel, select the Pages tab.</li>
					<li>Click on Add New.</li>
					<li>Give the page a title, "My New Home Page."</li>
					<li>Create the content for your home page.</li>
					<li>Click the Publish button.</li>
			</ul>
				</p>

				<p>
				Now let's tell Wordpress to use this new page as Homepage:
			<ul>
					<li>In the Settings panel on the left navigation select Reading.</li>
					<li>To change the front page to your new home page, select "A static page" for Front page displays.</li>
					<li>Select "My New Home Page" from the Front page drop down menu.</li>
			</ul>
				</p>

				<a target="_blank" href="http://bloggingexperiment.com/archives/wordpress-page-as-home-page.php" class="link-more">more details</a>

       	  </div>
        </div>
        <!--/ content -->
        <!-- sidebar -->
        <div class="grid_6">
       	  <div class="text">
			<h2 class="title_border">Homepage Templates</h2>
					<ul>
					<li> Page with left or right sidebar</li>
					<li>Reservation Page</li>
					<li> Full Width Page</li>
					<li> Contact Page</li>
					<li> Blog Page</li>
					</ul>
		  </div>            
        </div>
        <!--/ sidebar -->
        
        <div class="clear"></div>
	</div>
    <!--/ middle content -->

<?php get_footer(); ?>