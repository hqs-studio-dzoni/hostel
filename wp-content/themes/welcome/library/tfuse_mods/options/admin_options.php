<?php
/* Initializes all the theme settings option fields for admin area. */ 	
function admin_option_fields(){
	global $admin_options, $tfuse;	
	$prefix = $tfuse->prefix;
	
	$admin_options = array();  
  	
	/* General Tab */
 	$admin_options[] = array( 	"name"  	=> "General",
								"type"  	=> "tab",
								"id"    	=> "general");
	
	/* General Settings Panel */
	$admin_options[] = array( 	"name"  	=> "General Settings",
								"type"  	=> "heading");
	
	// Custom Logo Option  
	 $admin_options[] = array( "name" 		=> "Custom Logo",
								"desc" 		=> "Upload a logo for your theme, or specify the image address of your online logo. (http://yoursite.com/logo.png)",
								"id" 		=> "{$prefix}_logo",
								"std" 		=> "",
								"type" 		=> "upload"); 
	 
	// Custom Favicon Option  
	$admin_options[] = array( 	"name"  	=> "Custom Favicon",
								"desc"  	=> "Upload a 16px x 16px Png/Gif image that will represent your website's favicon.",
								"id"    	=> "{$prefix}_custom_favicon",
								"std"   	=> "",
								"type"  	=> "upload");

	// Theme Stylesheet Option
	$admin_options[] = array(	"name" 		=> "Theme Stylesheet",
								"desc" 		=> "Please select your colour scheme here.",
								"id" 		=> "{$prefix}_alt_stylesheet",
								"std" 		=> "",
								"type" 		=> "select",
								"options" 	=> tfuse_styles());
	
	// Tracking Code Option  
	$admin_options[] = array( 	"name"  	=> "Tracking Code",
								"desc"  	=> "Paste your Google Analytics (or other) tracking code here. This will be added into the footer template of your theme.",
								"id"    	=> "{$prefix}_google_analytics",
								"std"   	=> "",
								"type"  	=> "textarea");        
	
	// RSS URL Option  
	$admin_options[] = array( 	"name"  	=> "RSS URL",
								"desc"  	=> "Enter your preferred RSS URL. (Feedburner or other)",
								"id"    	=> "{$prefix}_feedburner_url",
								"std"   	=> "",
								"type"  	=> "text");
								
	// E-Mail URL Option  
	$admin_options[] = array(	"name"  	=> "E-Mail URL",
								"desc"  	=> "Enter your preferred E-mail subscription URL. (Feedburner or other)",
								"id"    	=> "{$prefix}_feedburner_id",
								"std"   	=> "",
								"type"  	=> "text");
	
	// Custom CSS Option  
	$admin_options[] = array( 	"name"  	=> "Custom CSS",
							  	"desc"  	=> "Quickly add some CSS to your theme by adding it to this block.",
								"id"    	=> "{$prefix}_custom_css",
								"std"   	=> "",
								"type"  	=> "textarea");
	
	/* Sidebar Panel */
	$admin_options[] = array(	"name"  	=> "Sidebar",
					    	  	"type"  	=> "heading"); 
								
	// Sidebar Position  
	$admin_options[] = array( 	"name"  	=> "Default Sidebars Position",
								"desc"  	=> "Select your preferred Default Sidebars Position.",
								"id"    	=> "{$prefix}_sidebar_position",
								"std"   	=> "",
								"options" 	=> array("Right", "Left"),
								"type"  	=> "select");        

	// Extra Widget Areas for specific Pages Option  
 	$admin_options[] = array(	"name"  	=> "Extra Widget Areas for specific Pages",
								"desc"  	=> "Here you can add widget areas for single pages. That way you can put different content for each page into your sidebar.<br/>
												After you have choosen the Pages press the 'Save Changes' button and then start adding widgets to your new widget areas <a href='widgets.php'>here</a>.<br/><br/>
												<strong>Attention</strong> when removing areas: You have to be carefull when deleting widget areas that are not the last one in the list.<br/> It is recommended to avoid this. If you want to know more about this topic please read the documentation that comes with this theme.<br/>",
								"id"    	=> "{$prefix}_multi_widget_pages",
								"type"  	=> "multi",
								"subtype" 	=> "page");
	
	// Extra Widget Areas for specific Categories Option  
	$admin_options[] = array(	"name"  	=> "Extra Widget Areas for specific Categories",
								"desc"  	=> "Here you can add widget areas for single categories. That way you can put different content for each category into your sidebar.<br/>
												After you have choosen the Pages press the 'Save Changes' button and then start adding widgets to your new widget areas <a href='widgets.php'>here</a>.<br/><br/>
												<strong>Attention</strong> when removing areas: You have to be carefull when deleting widget areas that are not the last one in the list.<br/> It is recommended to avoid this. If you want to know more about this topic please read the documentation that comes with this theme.<br/>",
								"id"    	=> "{$prefix}_multi_widget_categories",
								"type"  	=> "multi",
								"subtype" 	=> "category");
	
	// Extra Widget Areas for specific Posts  
	$admin_options[] = array(	"name"  	=> "Extra Widget Areas for specific Posts",
								"desc"  	=> "Here you can add widget areas for single post. That way you can put different content for each post into your sidebar.<br/>
												After you have choosen the Posts press the 'Save Changes' button and then start adding widgets to your new widget areas <a href='widgets.php'>here</a>.<br/><br/>
												<strong>Attention</strong> when removing areas: You have to be carefull when deleting widget areas that are not the last one in the list.<br/> It is recommended to avoid this. If you want to know more about this topic please read the documentation that comes with this theme.<br/>",
								"id"    	=> "{$prefix}_multi_widget_posts",
								"type"  	=> "multi",
								"subtype" 	=> "post");
	
	/* Lightbox (prettyPhoto) Panel */
	$admin_options[] = array(   "name"  	=> "Lightbox (prettyPhoto)",
								"type"  	=> "heading");    

	// Disable posts lightbox (prettyPhoto) Option  
	$admin_options[] = array(	"name"  	=> "Disable lightbox",
								"desc"  	=> "Disable lightbox (prettyPhoto)",
								"id"    	=> "{$prefix}_disable_lightbox",
								"std"   	=> "false",
								"type"  	=> "checkbox");

	/* Disable SEO options */
	$admin_options[] = array(   "name"  	=> "Disable SEO options",
								"type"  	=> "heading");

	// Disable SEO
	$admin_options[] = array(	"name"  	=> "Disable SEO",
								"desc"  	=> "Disable framework SEO options",
								"id"    	=> "{$prefix}_deactivate_tfuse_seo",
								"std"   	=> "false",
								"type"  	=> "checkbox");
	
	/* Dynamic Images Panel */
	$admin_options[] = array( 	"name"  	=> "Dynamic Images",
								"type"  	=> "heading");    
	
	// Enable Dynamic Image Resizer Option  
	$admin_options[] = array( 	"name"  	=> "Enable Dynamic Image Resizer",
								"desc"  	=> "This will enable the thumb.php script. It dynamicaly resizes images on your site.",
								"id"    	=> "{$prefix}_resize",
								"std"   	=> "false",
								"type"  	=> "checkbox");    
 
 	 /* HeaderTab */
	$admin_options[] = array( 	"name"  	=> "Header",
								"type"  	=> "tab",
								"id"    	=> "header");
	
	$admin_options[] = array(   "name"  	=> "Header Content",
								"type"  	=> "heading");
		
	$admin_options[] = array( 	"name" 		=> "Header Shortcodes",
								"desc" 		=> "Enter header shortcodes.",
								"id" 		=> "{$prefix}_header_shortcodes",
								"std" 		=> "",
								"type" 		=> "textarea");
 
 	 /* Footer Tab */
	$admin_options[] = array( 	"name"  	=> "Footer",
								"type"  	=> "tab",
								"id"    	=> "footer");
	
	$admin_options[] = array(   "name"  	=> "Footer Content",
								"type"  	=> "heading");
	
	$admin_options[] = array( 	"name" 		=> "Enable Footer Shortcodes",
								"desc" 		=> "This will enable footer shortcodes.",
								"id" 		=> "{$prefix}_enable_footer_shortcodes",
								"std" 		=> "",
								"type" 		=> "checkbox");
	
	$admin_options[] = array( 	"name" 		=> "Footer Shortcodes",
								"desc" 		=> "Enter footer shortcodes.",
								"id" 		=> "{$prefix}_footer_shortcodes",
								"std" 		=> "",
								"type" 		=> "textarea");

	if(get_option("{$prefix}_deactivate_tfuse_seo")!='true') { 
 	 /* SEO Tab */
	$admin_options[] = array( 	"name"  	=> "SEO",
								"type"  	=> "tab",
								"id"    	=> "seo");
	
	$admin_options[] = array(   "name"  	=> "META Data for HomePage",
								"type"  	=> "heading");
	
	$admin_options[] = array( 	"name" 		=> "Home Page Title",
								"desc" 		=> "Enter custom title for home page.",
								"id" 		=> "{$prefix}_homepage_title",
								"std" 		=> "",
								"type" 		=> "text");
	
	$admin_options[] = array( 	"name" 		=> "Keywords",
								"desc" 		=> "Enter custom keywords for home page.",
								"id" 		=> "{$prefix}_homepage_keywords",
								"std" 		=> "",
								"type" 		=> "textarea");
	
	$admin_options[] = array( 	"name" 		=> "Description",
								"desc" 		=> "Enter custom description for home page.",
								"id" 		=> "{$prefix}_homepage_description",
								"std" 		=> "",
								"type" 		=> "textarea");
	

	$admin_options[] = array(   "name"  	=> "General META",
								"type"  	=> "heading");
	
	$admin_options[] = array( 	"name" 		=> "Keywords",
								"desc" 		=> "Enter general keywords for home page, categories, arhives and other pages than single posts and pages.",
								"id" 		=> "{$prefix}_general_keywords",
								"std" 		=> "",
								"type" 		=> "textarea");
	
	$admin_options[] = array( 	"name" 		=> "Description",
								"desc" 		=> "Enter general description for home page, categories, arhives and other pages than single posts and pages.",
								"id" 		=> "{$prefix}_general_description",
								"std" 		=> "",
								"type" 		=> "textarea");
	}
									
	/* END admin_option_fields() */
	update_option("{$tfuse->prefix}_admin_options",$admin_options);
	// END admin_option_fields()
}

?>