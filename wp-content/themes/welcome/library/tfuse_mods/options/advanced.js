jQuery(document).ready(function() {

	var options = new Array();

	//******* PAGES *******//
	options['page_template'] 					= jQuery('#page_template').val();
	options['envision_blog_page_cat'] 			= jQuery('#envision_blog_page_cat').val();
	options['envision_page_welcome_bar'] 		= jQuery('#envision_page_welcome_bar').attr('checked')?1:0;
	options['envision_page_welcome_bar_source'] = jQuery('.envision_page_welcome_bar_source').val();
	options['envision_page_enable_slider'] 		= jQuery('#envision_page_enable_slider').attr('checked')?1:0;
	options['envision_page_slider_type'] 		= jQuery('#envision_page_slider_type').val();

	//******* POSTS *******//
	options['envision_post_welcome_bar'] 		= jQuery('#envision_post_welcome_bar').attr('checked')?1:0;
	options['envision_post_welcome_bar_source'] = jQuery('.envision_post_welcome_bar_source').val();
	options['envision_post_enable_slider'] 		= jQuery('#envision_post_enable_slider').attr('checked')?1:0;
	options['envision_post_slider_type'] 		= jQuery('#envision_post_slider_type').val();

	//******* Categories *******//
	var tag_ID = jQuery("input[name='tag_ID']").val();
	if(tag_ID)
	{
		options['envision_category_template'] 			= jQuery('#envision_category_template_'+tag_ID).val();
		options['envision_category_welcome_bar'] 		= jQuery('#envision_category_welcome_bar_'+tag_ID).attr('checked')?1:0;
		options['envision_category_welcome_bar_source'] = jQuery('.envision_category_welcome_bar_source_'+tag_ID).val();
	}



	//-------------------------------------------------//
	jQuery('#page_template').bind('change', function() {
		options['page_template'] = jQuery(this).val();
		tfuse_toggle_options(options);
	});

	jQuery('#envision_blog_page_cat').bind('change', function() {
		options['envision_blog_page_cat'] = jQuery(this).val();
		tfuse_toggle_options(options);
	});

	jQuery('#envision_page_welcome_bar').bind('change', function() {
		options['envision_page_welcome_bar'] = jQuery(this).attr('checked')?1:0;
		tfuse_toggle_options(options);
	});

	jQuery('.envision_page_welcome_bar_source').bind('change', function() {
		options['envision_page_welcome_bar_source'] = jQuery(this).val();
		tfuse_toggle_options(options);
	});

	jQuery('#envision_page_enable_slider').bind('change', function() {
		options['envision_page_enable_slider'] = jQuery(this).attr('checked')?1:0;
		tfuse_toggle_options(options);
	});

	jQuery('#envision_page_slider_type').bind('change', function() {
		options['envision_page_slider_type'] = jQuery(this).val();
		tfuse_toggle_options(options);
	});


	//******* POSTS *******//
	jQuery('#envision_post_welcome_bar').bind('change', function() {
		options['envision_post_welcome_bar'] = jQuery(this).attr('checked')?1:0;
		tfuse_toggle_options(options);
	});

	jQuery('.envision_post_welcome_bar_source').bind('change', function() {
		options['envision_post_welcome_bar_source'] = jQuery(this).val();
		tfuse_toggle_options(options);
	});

	jQuery('#envision_post_enable_slider').bind('change', function() {
		options['envision_post_enable_slider'] = jQuery(this).attr('checked')?1:0;
		tfuse_toggle_options(options);
	});

	jQuery('#envision_post_slider_type').bind('change', function() {
		options['envision_post_slider_type'] = jQuery(this).val();
		tfuse_toggle_options(options);
	});


	//******* Categories *******//
	if(tag_ID)
	{
		jQuery('#envision_category_template_'+tag_ID).bind('change', function() {
			options['envision_category_template'] = jQuery(this).val();
			tfuse_toggle_options(options);
		});

		jQuery('#envision_category_welcome_bar_'+tag_ID).bind('change', function() {
			options['envision_category_welcome_bar'] = jQuery(this).attr('checked')?1:0;
			tfuse_toggle_options(options);
		});

		jQuery('.envision_category_welcome_bar_source_'+tag_ID).bind('change', function() {
			options['envision_category_welcome_bar_source'] = jQuery(this).val();
			tfuse_toggle_options(options);
		});
	}



	tfuse_toggle_options(options);

	function tfuse_toggle_options(options) {

		//------------------------------------------------------
		if(options['page_template'] == 'template-full_width.php') {
			jQuery('#envision_page_sidebar_position, #envision_blog_page_cat').parents('.option-inner').hide();
		}
		else if(options['page_template'] == 'template-blog.php')
		{
			jQuery('#envision_page_sidebar_position, #envision_blog_page_cat').parents('.option-inner').show();
		}
		else
		{
			jQuery('#envision_page_sidebar_position').parents('.option-inner').show();
			jQuery('#envision_blog_page_cat').parents('.option-inner').hide();
		}

		//------------------------------------------------------
		if(options['envision_page_welcome_bar']) {
			jQuery('.envision_page_welcome_bar_source, #envision_page_header_title, #envision_page_header_subtitle, #envision_page_header_image_upload, #envision_page_header_rightshortcode, #envision_page_header_custombar').parents('.option-inner').hide();
		}
		else
		{
			jQuery('.envision_page_welcome_bar_source').parents('.option-inner').show();
			if(options['envision_page_welcome_bar_source'] == 'fields')
			{
				jQuery('#envision_page_header_title, #envision_page_header_subtitle, #envision_page_header_image_upload, #envision_page_header_rightshortcode').parents('.option-inner').show();
				jQuery('#envision_page_header_custombar').parents('.option-inner').hide();
			}
			else
			{
				jQuery('#envision_page_header_title, #envision_page_header_subtitle, #envision_page_header_image_upload, #envision_page_header_rightshortcode').parents('.option-inner').hide();
				jQuery('#envision_page_header_custombar').parents('.option-inner').show();
			}

		}

		//------------------------------------------------------
		if(!options['envision_page_enable_slider']) {
			jQuery('#envision_page_slider_type, #envision_page_slider1_data_upload, #envision_page_slider2_data_upload, #envision_page_slider3_data_upload, #envision_page_slider4_data_upload').parents('.option-inner').hide();
			jQuery('#envision_page_custom_single_image_upload, #envision_page_custom_single_image_url, #envision_page_custom_single_image_target').parents('.option-inner').hide();
		}
		else
		{
			jQuery('#envision_page_slider_type').parents('.option-inner').show();
			if(options['envision_page_slider_type'] == 'type1')
			{
				jQuery('#envision_page_slider1_data_upload').parents('.option-inner').show();
				jQuery('#envision_page_slider2_data_upload, #envision_page_slider3_data_upload, #envision_page_slider4_data_upload').parents('.option-inner').hide();
				jQuery('#envision_page_custom_single_image_upload, #envision_page_custom_single_image_url, #envision_page_custom_single_image_target').parents('.option-inner').hide();
			}
			else if(options['envision_page_slider_type'] == 'type2')
			{
				jQuery('#envision_page_slider2_data_upload').parents('.option-inner').show();
				jQuery('#envision_page_slider1_data_upload, #envision_page_slider3_data_upload, #envision_page_slider4_data_upload').parents('.option-inner').hide();
				jQuery('#envision_page_custom_single_image_upload, #envision_page_custom_single_image_url, #envision_page_custom_single_image_target').parents('.option-inner').hide();
			}
			else if(options['envision_page_slider_type'] == 'type3')
			{
				jQuery('#envision_page_slider3_data_upload').parents('.option-inner').show();
				jQuery('#envision_page_slider1_data_upload, #envision_page_slider2_data_upload, #envision_page_slider4_data_upload').parents('.option-inner').hide();
				jQuery('#envision_page_custom_single_image_upload, #envision_page_custom_single_image_url, #envision_page_custom_single_image_target').parents('.option-inner').hide();
			}
			else if(options['envision_page_slider_type'] == 'type4')
			{
				jQuery('#envision_page_slider4_data_upload').parents('.option-inner').show();
				jQuery('#envision_page_slider1_data_upload, #envision_page_slider2_data_upload, #envision_page_slider3_data_upload').parents('.option-inner').hide();
				jQuery('#envision_page_custom_single_image_upload, #envision_page_custom_single_image_url, #envision_page_custom_single_image_target').parents('.option-inner').hide();
			}
			else
			{
				jQuery('#envision_page_custom_single_image_upload, #envision_page_custom_single_image_url, #envision_page_custom_single_image_target').parents('.option-inner').show();
				jQuery('#envision_page_slider1_data_upload, #envision_page_slider2_data_upload, #envision_page_slider3_data_upload, #envision_page_slider4_data_upload').parents('.option-inner').hide();
			}

		}


		//******* POSTS *******//
		//------------------------------------------------------
		if(options['envision_post_welcome_bar']) {
			jQuery('.envision_post_welcome_bar_source, #envision_post_header_title, #envision_post_header_subtitle, #envision_post_header_image_upload, #envision_post_header_rightshortcode, #envision_post_header_custombar').parents('.option-inner').hide();
		}
		else
		{
			jQuery('.envision_post_welcome_bar_source').parents('.option-inner').show();
			if(options['envision_post_welcome_bar_source'] == 'fields')
			{
				jQuery('#envision_post_header_title, #envision_post_header_subtitle, #envision_post_header_image_upload, #envision_post_header_rightshortcode').parents('.option-inner').show();
				jQuery('#envision_post_header_custombar').parents('.option-inner').hide();
			}
			else
			{
				jQuery('#envision_post_header_title, #envision_post_header_subtitle, #envision_post_header_image_upload, #envision_post_header_rightshortcode').parents('.option-inner').hide();
				jQuery('#envision_post_header_custombar').parents('.option-inner').show();
			}

		}

		//------------------------------------------------------
		if(!options['envision_post_enable_slider']) {
			jQuery('#envision_post_slider_type, #envision_post_slider1_data_upload, #envision_post_slider2_data_upload, #envision_post_slider3_data_upload, #envision_post_slider4_data_upload').parents('.option-inner').hide();
			jQuery('#envision_post_custom_single_image_upload, #envision_post_custom_single_image_url, #envision_post_custom_single_image_target').parents('.option-inner').hide();
		}
		else
		{
			jQuery('#envision_post_slider_type').parents('.option-inner').show();
			if(options['envision_post_slider_type'] == 'type1')
			{
				jQuery('#envision_post_slider1_data_upload').parents('.option-inner').show();
				jQuery('#envision_post_slider2_data_upload, #envision_post_slider3_data_upload, #envision_post_slider4_data_upload').parents('.option-inner').hide();
				jQuery('#envision_post_custom_single_image_upload, #envision_post_custom_single_image_url, #envision_post_custom_single_image_target').parents('.option-inner').hide();
			}
			else if(options['envision_post_slider_type'] == 'type2')
			{
				jQuery('#envision_post_slider2_data_upload').parents('.option-inner').show();
				jQuery('#envision_post_slider1_data_upload, #envision_post_slider3_data_upload, #envision_post_slider4_data_upload').parents('.option-inner').hide();
				jQuery('#envision_post_custom_single_image_upload, #envision_post_custom_single_image_url, #envision_post_custom_single_image_target').parents('.option-inner').hide();
			}
			else if(options['envision_post_slider_type'] == 'type3')
			{
				jQuery('#envision_post_slider3_data_upload').parents('.option-inner').show();
				jQuery('#envision_post_slider1_data_upload, #envision_post_slider2_data_upload, #envision_post_slider4_data_upload').parents('.option-inner').hide();
				jQuery('#envision_post_custom_single_image_upload, #envision_post_custom_single_image_url, #envision_post_custom_single_image_target').parents('.option-inner').hide();
			}
			else if(options['envision_post_slider_type'] == 'type4')
			{
				jQuery('#envision_post_slider4_data_upload').parents('.option-inner').show();
				jQuery('#envision_post_slider1_data_upload, #envision_post_slider2_data_upload, #envision_post_slider3_data_upload').parents('.option-inner').hide();
				jQuery('#envision_post_custom_single_image_upload, #envision_post_custom_single_image_url, #envision_post_custom_single_image_target').parents('.option-inner').hide();
			}
			else
			{
				jQuery('#envision_post_custom_single_image_upload, #envision_post_custom_single_image_url, #envision_post_custom_single_image_target').parents('.option-inner').show();
				jQuery('#envision_post_slider1_data_upload, #envision_post_slider2_data_upload, #envision_post_slider3_data_upload, #envision_post_slider4_data_upload').parents('.option-inner').hide();
			}

		}

		//******* CATEGORIES *******//
		if(tag_ID)
		{
			//------------------------------------------------------
			if(options['envision_category_template'] == 'portfolio-3col.php' || options['envision_category_template'] == 'portfolio-4col.php') {
				jQuery('#envision_category_sidebar_position_'+tag_ID).parents('.form-field').hide();
			}
			else
			{
				jQuery('#envision_category_sidebar_position_'+tag_ID).parents('.form-field').show();
			}

			//------------------------------------------------------
			if(options['envision_category_welcome_bar']) {
				jQuery('.envision_category_welcome_bar_source_'+tag_ID+', #envision_category_header_title_'+tag_ID+', #envision_category_header_subtitle_'+tag_ID+', #envision_category_header_image_'+tag_ID+'_upload, #envision_category_header_rightshortcode_'+tag_ID+', #envision_category_header_custombar_'+tag_ID+'').parents('.form-field').hide();
			}
			else
			{
				jQuery('.envision_category_welcome_bar_source_'+tag_ID).parents('.form-field').show();
				if(options['envision_category_welcome_bar_source'] == 'fields')
				{
					jQuery('#envision_category_header_title_'+tag_ID+', #envision_category_header_subtitle_'+tag_ID+', #envision_category_header_image_'+tag_ID+'_upload, #envision_category_header_rightshortcode_'+tag_ID+'').parents('.form-field').show();
					jQuery('#envision_category_header_custombar_'+tag_ID).parents('.form-field').hide();
				}
				else
				{
					jQuery('#envision_category_header_title_'+tag_ID+', #envision_category_header_subtitle_'+tag_ID+', #envision_category_header_image_'+tag_ID+'_upload, #envision_category_header_rightshortcode_'+tag_ID+'').parents('.form-field').hide();
					jQuery('#envision_category_header_custombar_'+tag_ID).parents('.form-field').show();
				}
			}
		}


	}


});