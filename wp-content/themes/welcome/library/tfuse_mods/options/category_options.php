<?php
/* Initializes all the theme settings option fields for categories area. */
function category_option_fields(){
	global $tfuse, $category_options;
	$prefix = $tfuse->prefix;
	
	$category_options = array();
	
	/* Choise Header Bar Type */
	$category_options[] = array("name" 		=> "Choice Category Template",
								"desc" 		=> "Some themes have custom templates you can use for certain categories that might have additional features or custom layouts. If so, you'll see them above.",
								"id" 		=> "{$prefix}_category_template",
								"std" 		=> "social",
								"type"		=> "select",
								"options" 	=> tfuse_category_template());
								
	/* Header Title */
	$category_options[] = array("name" 		=> "Disable Category Header Welcome Bar",
								"desc" 		=> "",
								"id" 		=> "{$prefix}_category_disable_header_welcome_bar",
								"std" 		=> "",
								"type"		=> "checkbox");

	// Header Page Title
	$category_options[] = array("name" 		=> "Title",
								"desc" 		=> "Enter your preferred Header Bar Title for this category.",
								"id" 		=> "{$prefix}_category_header_title",
								"std" 		=> "Custom Bar Title",
								"type"		=> "text");

	// Custom Logo Option
	$category_options[] = array("name" 		=> "Image",
								"desc" 		=> "Upload an image for your Header Bar, or specify the image address of your online image. (http://yoursite.com/image.png)",
								"id" 		=> "{$prefix}_category_header_image",
								"std" 		=> "",
								"type" 		=> "upload");

	// Custom Logo Option
	$category_options[] = array("name" 		=> "Image URL",
								"desc" 		=> "Enter Image URL for this category.",
								"id" 		=> "{$prefix}_category_header_image_url",
								"std" 		=> "",
								"type" 		=> "text");

	// Custom Logo Option
	$category_options[] = array("name" 		=> "Image URL Target",
								"desc" 		=> "Enter Image URL for this category.",
								"id" 		=> "{$prefix}_category_header_image_url_target",
								"std" 		=> "_self",
								"type" 		=> "select",
								'options'	=> array('_self' => 'Open link in same window', '_blank' => 'Open link in new window')  
								);
	
	// Sidebar Position  
	$category_options[] = array("name"  	=> "Category Sidebar Position",
								"desc"  	=> "Select your preferred Category Sidebar Position.",
								"id"    	=> "{$prefix}_category_sidebar_position",
								"std"   	=> "",
								"options" 	=> array("Default", "Right", "Left"),
								"type"  	=> "select");
	
	$category_options[] = array("name" 		=> "Custom Top Content",
								"desc" 		=> "Enter your prefered custom content (text, HTML, shortcodes).<br /> This will appear between the header bar and the content.",
								"id" 		=> "{$prefix}_category_content_top",
								"std" 		=> "",
								"type" 		=> "textarea");
	
	$category_options[] = array("name" 		=> "Custom Bottom Content",
								"desc" 		=> "Enter your prefered custom content (text, HTML, shortcodes).<br /> This will appear between the content and the footer.",
								"id" 		=> "{$prefix}_category_content_bottom",
								"std" 		=> "",
								"type" 		=> "textarea");

	if(get_option("{$prefix}_deactivate_tfuse_seo")!='true') {
	$category_options[] = array("name" 		=> "SEO - Title",
								"desc" 		=> "Enter your prefered custom title or leave blank for deafault value.",
								"id" 		=> "{$prefix}_cat_seo_title",
								"std" 		=> "",
								"type" 		=> "text");
	
	$category_options[] = array("name" 		=> "SEO - Keywords",
								"desc" 		=> "Enter your prefered custom keywords or leave blank for deafault value.",
								"id" 		=> "{$prefix}_cat_seo_keywords",
								"std" 		=> "",
								"type" 		=> "textarea");
	
	$category_options[] = array("name" 		=> "SEO - Description",
								"desc" 		=> "Enter your prefered custom description or leave blank for deafault value.",
								"id" 		=> "{$prefix}_cat_seo_description",
								"std" 		=> "",
								"type" 		=> "textarea");
	}

	
	/* END custom_option_fields() */
	update_option("{$tfuse->prefix}_category_options",$category_options);
	// END custom_option_fields()
}

?>