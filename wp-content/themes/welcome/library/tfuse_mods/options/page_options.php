<?php
/* Initializes all the theme settings option fields for pages area. */
function page_option_fields(){
	global $tfuse, $page_options;
	$prefix = $tfuse->prefix;

	$page_options = array();

 	/***********************************************************
		Sidebar
	************************************************************/

 	/* Single Page */
	$page_options[] = array(	"name"    	=> "Single Page",
								"id"      	=> "{$prefix}_page_side_media",
								"page"		=> "page",
								"type"		=> "metabox",
								"context"	=> "side");

	// Enable Single Page Comments
	$page_options[] = array( 	"name" 		=> "Enable Single Page Comments",
								"desc" 		=> "",
								"id" 		=> "{$prefix}_page_single_comments",
								"std" 		=> "false",
								"type"		=> "checkbox");

	// Sidebar Position
	$page_options[] = array( 	"name"  	=> "Page Sidebar Position",
								"desc"  	=> "Select your preferred Page Sidebar Position.",
								"id"    	=> "{$prefix}_page_sidebar_position",
								"std"   	=> "",
								"options" 	=> array("Default", "Right", "Left"),
								"type"  	=> "select");

 	// Blog Category Option
	$page_options[] = array(	"name"  	=> "Homepage Blog Category",
								"desc"  	=> "Which category to display in case if you choose this page like a blog themplate.",
								"id"    	=> "{$prefix}_blog_page_cat",
								"std"   	=> "",
								"type"  	=> "dropdown_categories",
								"install"   => 'cat');


 	/***********************************************************
		Normal
	************************************************************/

	/* Header Panel */
	$page_options[] = array(	"name"    	=> "Header Welcome Bar",
								"id"      	=> "{$prefix}_page_information",
								"page"		=> "page",
								"type"		=> "metabox",
								"context"	=> "normal");
								
	/* Header Title */
	$page_options[] = array( 	"name" 		=> "Disable Page Header Welcome Bar",
								"desc" 		=> "This will disable the Page Header Welcome Bar.",
								"id" 		=> "{$prefix}_page_disable_header_welcome_bar",
								"std" 		=> "",
								"type"		=> "checkbox");

	//Header Bar Info from
	$page_options[] = array( 	"name" 		=> "Use",
								"desc" 		=> "",
								"id" 		=> "{$prefix}_page_welcome_bar_source",
								"std" 		=> "header1",
								"options" 	=> array("header1" => "Header 1 Image", "header3" => "Header 3 Images"),
								"type"  	=> "radio");

	// Header Page Title
	$page_options[] = array( 	"name" 		=> "Title",
								"desc" 		=> "Enter your preferred Header Bar Title for this page.",
								"id" 		=> "{$prefix}_page_header_title",
								"std" 		=> "Custom Bar Title",
								"type"		=> "text");

	// Custom Logo Option
	$page_options[] = array(	"name" 		=> "Image",
								"desc" 		=> "Upload an image for your Header Bar, or specify the image address of your online image. (http://yoursite.com/image.png)",
								"id" 		=> "{$prefix}_page_header_image",
								"std" 		=> "",
								"type" 		=> "upload");

	// Custom Logo Option
	$page_options[] = array(	"name" 		=> "Image URL",
								"desc" 		=> "Enter Image URL for this page.",
								"id" 		=> "{$prefix}_page_header_image_url",
								"std" 		=> "",
								"type" 		=> "text");

	// Custom Logo Option
	$page_options[] = array(	"name" 		=> "Image URL Target",
								"desc" 		=> "Enter Image URL for this page.",
								"id" 		=> "{$prefix}_page_header_image_url_target",
								"std" 		=> "_self",
								"type" 		=> "select",
								'options'	=> array('_self' => 'Open link in same window', '_blank' => 'Open link in new window')  
								);

	/* Images Option */
	$page_options[] = array( 	"name" 		=> "Header Images",
								"desc" 		=> "Add content for Page Header Images",
								"id" 		=> "{$prefix}_page_images_data",
								"std" 		=> "",
								"type"		=> "slider",
								"c_field"	=> "{$prefix}_page_images_image",
								"fields"	=> array( 
 													
													array(	'id' 		=> 'image_page_title',
															'desc' 		=> 'image title',
															'type' 		=> 'text',
															'std' 		=> ''),
													
 													
													array(	'id' 		=> 'image_page_link',
															'desc' 		=> 'http://',
															'type' 		=> 'text',
															'std' 		=> ''),
													
													array(	'id' 		=> 'image_page_target',
															'desc' 		=> 'http://',
															'type' 		=> 'select',
															'std' 		=> '_self',
															'options'	=> array('_self' => 'Open link in same window', '_blank' => 'Open link in new window') )
													));

 	/* Header Panel */
	$page_options[] = array(	"name"    	=> "Page Slider Options",
								"id"      	=> "{$prefix}_page_slider_option",
								"page"		=> "page",
								"type"		=> "metabox",
								"context"	=> "normal");

	/* Header Title */
	$page_options[] = array( 	"name" 		=> "Disable Page Slider",
								"desc" 		=> "This will disable the Page Slider.",
								"id" 		=> "{$prefix}_page_disable_slider",
								"std" 		=> "",
								"type"		=> "checkbox");
								
	/* Slider 1 Option */
	$page_options[] = array( 	"name" 		=> "Header Slider",
								"desc" 		=> "Add content for Page Header Slider",
								"id" 		=> "{$prefix}_page_slider_data",
								"std" 		=> "",
								"type"		=> "slider",
								"c_field"	=> "{$prefix}_page_slider_image",
								"fields"	=> array( 
 													
													array(	'id' 		=> 'slider_page_title',
															'desc' 		=> 'slider title',
															'type' 		=> 'text',
															'std' 		=> ''),
													
 													
													array(	'id' 		=> 'slider_page_link',
															'desc' 		=> 'http://',
															'type' 		=> 'text',
															'std' 		=> ''),
													
													array(	'id' 		=> 'slider_page_target',
															'desc' 		=> 'http://',
															'type' 		=> 'select',
															'std' 		=> '_self',
															'options'	=> array('_self' => 'Open link in same window', '_blank' => 'Open link in new window') )
													));

	if(get_option("{$prefix}_deactivate_tfuse_seo")!='true') {
	/* SEO Panel */
	$page_options[] = array(	"name"    	=> "SEO",
								"id"      	=> "{$prefix}_page_seo",
								"page"		=> "page",
								"type"		=> "metabox",
								"context"	=> "normal");

	$page_options[] = array(	"name" 		=> "Custom Post Title",
								"desc" 		=> "Enter your prefered custom title or leave blank for deafault value.",
								"id" 		=> "{$prefix}_page_seo_title",
								"std" 		=> "",
								"type" 		=> "text");

	$page_options[] = array(	"name" 		=> "Custom Post Keywords",
								"desc" 		=> "Enter your prefered custom keywords or leave blank for deafault value.",
								"id" 		=> "{$prefix}_page_seo_keywords",
								"std" 		=> "",
								"type" 		=> "textarea");

	$page_options[] = array(	"name" 		=> "Custom Post Description",
								"desc" 		=> "Enter your prefered custom description or leave blank for deafault value.",
								"id" 		=> "{$prefix}_page_seo_description",
								"std" 		=> "",
								"type" 		=> "textarea");
	}


 	/***********************************************************
		Advanced
	************************************************************/


	/* END custom_option_fields() */
	update_option("{$tfuse->prefix}_page_options",$page_options);
	// END custom_option_fields()
}

?>