<?php
/* Initializes all the theme settings option fields for posts area. */
function post_option_fields(){
	global $tfuse, $post_options;
	$prefix = $tfuse->prefix;
	
	$post_options = array();

	
 	/***********************************************************
		Normal
	************************************************************/

	/* Single Post */ 
	$post_options[] = array(	"name"    	=> "Single Post",
								"id"      	=> "{$prefix}_post_side_media",
								"page"		=> "post", 					 
								"type"		=> "metabox",				 
								"context"	=> "side");				 
	
	/* Disable Single Post Image */						
	$post_options[] = array( 	"name" 		=> "Disable Single Post Image",
								"desc" 		=> "",
								"id" 		=> "{$prefix}_post_single_image",
								"std" 		=> "false",
								"type"		=> "checkbox");
	
	/* Disable Single Post Video */						
	$post_options[] = array( 	"name" 		=> "Disable Single Post Video",
								"desc" 		=> "",
								"id" 		=> "{$prefix}_post_single_video",
								"std" 		=> "false",
								"type"		=> "checkbox");
	
	/* Disable Single Post Video */						
	$post_options[] = array( 	"name" 		=> "Disable Single Post Comments",
								"desc" 		=> "",
								"id" 		=> "{$prefix}_post_single_comments",
								"std" 		=> "false",
								"type"		=> "checkbox");
	
		// Sidebar Position
	$post_options[] = array( 	"name"  	=> "Post Sidebar Position",
								"desc"  	=> "Select your preferred Post Sidebar Position.",
								"id"    	=> "{$prefix}_post_sidebar_position",
								"std"   	=> "",
								"options" 	=> array("Default", "Right", "Left", "Full Width"),
								"type"  	=> "select");

		
	/* Post Data Panel */ 
	$post_options[] = array(	"name"    	=> "Post Media",
								"id"      	=> "{$prefix}_post_data",
								"page"		=> "post", 					 
								"type"		=> "metabox",				 
								"context"	=> "normal");	
	
	 /* Custom Header Image */
	 $post_options[] = array(	"name" 		=> "Main Image <br> (Large format)",
								"desc" 		=> "Upload an image for your post, or specify an online address for your image (http://yoursite.com/image.png). This image is the one that loads in the lightbox and also used to replace the rest of the images if you don't specify otherwise.",
								"id" 		=> "{$prefix}_post_image",
								"std" 		=> "",
								"type" 		=> "upload"); 
	
	 /* Custom Header Image */
	 $post_options[] = array(	"name" 		=> "Single Image <br> (606 x 145px)",
								"desc" 		=> "Upload an image for your post, or specify an online address for your image (http://yoursite.com/image.png). This image appears when you open the post. If you don't upload an image, the resized main image will be used.",
								"id" 		=> "{$prefix}_post_image_medium",
								"std" 		=> "",
								"type" 		=> "upload"); 
	
	 /* Custom Header Image */
	 $post_options[] = array(	"name" 		=> "Small Image <br> (234 x 112px)",
								"desc" 		=> "Upload an image for your post, or specify an online address for your image (http://yoursite.com/image.png). This image will appear on the 1 Column Portfolio page. If you don't upload an image, the resized main image will be used.",
								"id" 		=> "{$prefix}_post_image_small",
								"std" 		=> "",
								"type" 		=> "upload"); 
	
	 /* Custom Header Image */
	 $post_options[] = array(	"name" 		=> "Custom Post Video",
								"desc" 		=> "Read <a target='_blank' href='http://www.no-margin-for-errors.com/projects/prettyphoto-jquery-lightbox-clone/'>prettyPhoto documentation</a> for info on how to add video or flash in this URL.",
								"id" 		=> "{$prefix}_post_video",
								"std" 		=> "",
								"type" 		=> "text"); 
  
	/* Header Panel */
	$post_options[] = array(	"name"    	=> "Header Welcome Bar",
								"id"      	=> "{$prefix}_post_information",
								"page"		=> "post",
								"type"		=> "metabox",
								"context"	=> "normal");
								
	/* Header Title */
	$post_options[] = array( 	"name" 		=> "Disable Post Header Welcome Bar",
								"desc" 		=> "This will disable the Post Header Welcome Bar.",
								"id" 		=> "{$prefix}_post_disable_header_welcome_bar",
								"std" 		=> "",
								"type"		=> "checkbox");

	//Header Bar Info from
	$post_options[] = array( 	"name" 		=> "Use",
								"desc" 		=> "",
								"id" 		=> "{$prefix}_post_welcome_bar_source",
								"std" 		=> "header1",
								"options" 	=> array("header1" => "Header 1 Image", "header3" => "Header 3 Images"),
								"type"  	=> "radio");

	// Header Page Title
	$post_options[] = array( 	"name" 		=> "Title",
								"desc" 		=> "Enter your preferred Header Bar Title for this post.",
								"id" 		=> "{$prefix}_post_header_title",
								"std" 		=> "Custom Bar Title",
								"type"		=> "text");

	// Custom Logo Option
	$post_options[] = array(	"name" 		=> "Image",
								"desc" 		=> "Upload an image for your Header Bar, or specify the image address of your online image. (http://yoursite.com/image.png)",
								"id" 		=> "{$prefix}_post_header_image",
								"std" 		=> "",
								"type" 		=> "upload");

	// Custom Logo Option
	$post_options[] = array(	"name" 		=> "Image URL",
								"desc" 		=> "Enter Image URL for this post.",
								"id" 		=> "{$prefix}_post_header_image_url",
								"std" 		=> "",
								"type" 		=> "text");

	// Custom Logo Option
	$post_options[] = array(	"name" 		=> "Image URL Target",
								"desc" 		=> "Enter Image URL for this post.",
								"id" 		=> "{$prefix}_post_header_image_url_target",
								"std" 		=> "_self",
								"type" 		=> "select",
								'options'	=> array('_self' => 'Open link in same window', '_blank' => 'Open link in new window')  
								);

	/* Images Option */
	$post_options[] = array( 	"name" 		=> "Header Images",
								"desc" 		=> "Add content for Post Header Images",
								"id" 		=> "{$prefix}_post_images_data",
								"std" 		=> "",
								"type"		=> "slider",
								"c_field"	=> "{$prefix}_post_images_image",
								"fields"	=> array( 
 													
													array(	'id' 		=> 'image_post_title',
															'desc' 		=> 'image title',
															'type' 		=> 'text',
															'std' 		=> ''),
													
 													
													array(	'id' 		=> 'image_post_link',
															'desc' 		=> 'http://',
															'type' 		=> 'text',
															'std' 		=> ''),
													
													array(	'id' 		=> 'image_post_target',
															'desc' 		=> 'http://',
															'type' 		=> 'select',
															'std' 		=> '_self',
															'options'	=> array('_self' => 'Open link in same window', '_blank' => 'Open link in new window') )
													));

 	/* Header Panel */
	$post_options[] = array(	"name"    	=> "Post Slider Options",
								"id"      	=> "{$prefix}_post_slider_option",
								"page"		=> "post",
								"type"		=> "metabox",
								"context"	=> "normal");

	/* Header Title */
	$post_options[] = array( 	"name" 		=> "Disable Post Slider",
								"desc" 		=> "This will disable the Post Slider.",
								"id" 		=> "{$prefix}_post_disable_slider",
								"std" 		=> "",
								"type"		=> "checkbox");
								
	/* Slider 1 Option */
	$post_options[] = array( 	"name" 		=> "Header Slider",
								"desc" 		=> "Add content for Post Header Slider",
								"id" 		=> "{$prefix}_post_slider_data",
								"std" 		=> "",
								"type"		=> "slider",
								"c_field"	=> "{$prefix}_post_slider_image",
								"fields"	=> array( 
 													
													array(	'id' 		=> 'slider_post_title',
															'desc' 		=> 'slider title',
															'type' 		=> 'text',
															'std' 		=> ''),
													
 													
													array(	'id' 		=> 'slider_post_link',
															'desc' 		=> 'http://',
															'type' 		=> 'text',
															'std' 		=> ''),
													
													array(	'id' 		=> 'slider_post_target',
															'desc' 		=> 'http://',
															'type' 		=> 'select',
															'std' 		=> '_self',
															'options'	=> array('_self' => 'Open link in same window', '_blank' => 'Open link in new window') )
													));
													
	if(get_option("{$prefix}_deactivate_tfuse_seo")!='true') {
 	/* SEO Panel */ 
	$post_options[] = array(	"name"    	=> "SEO",
								"id"      	=> "{$prefix}_post_seo",
								"page"		=> "post", 					 
								"type"		=> "metabox",				 
								"context"	=> "normal");
	
	$post_options[] = array(	"name" 		=> "Custom Post Title",
								"desc" 		=> "Enter your prefered custom title or leave blank for deafault value.",
								"id" 		=> "{$prefix}_post_seo_title",
								"std" 		=> "",
								"type" 		=> "text");
	
	$post_options[] = array(	"name" 		=> "Custom Post Keywords",
								"desc" 		=> "Enter your prefered custom keywords or leave blank for deafault value.",
								"id" 		=> "{$prefix}_post_seo_keywords",
								"std" 		=> "",
								"type" 		=> "textarea");
	
	$post_options[] = array(	"name" 		=> "Custom Post Description",
								"desc" 		=> "Enter your prefered custom description or leave blank for deafault value.",
								"id" 		=> "{$prefix}_post_seo_description",
								"std" 		=> "",
								"type" 		=> "textarea");													
	}


	/* END custom_option_fields() */
	update_option("{$tfuse->prefix}_post_options",$post_options);
	// END custom_option_fields()
}

?>