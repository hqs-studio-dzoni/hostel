<?php 
get_header(); 
	
	$cat_ID = get_query_var('cat'); 
	$disable_header = get_option(PREFIX . '_category_disable_header_welcome_bar_' . $cat_ID); 
?>

<?php if ( $disable_header == 'false' ) { ?> 
	<div class="header">
		<div class="container">&nbsp;</div>  
	</div>
	<div class="header-line"></div>
<?php } ?>

<div class="middle">
<div class="container <?php if ($disable_header == 'true') { ?>padding50px<?php } ?>">

	<?php 
		if ( get_option(PREFIX . '_category_sidebar_position_' . $cat_ID)==1 ) { $content = 'suffix_1'; $sidebar = ''; }
	elseif ( get_option(PREFIX . '_category_sidebar_position_' . $cat_ID)==2 ) { $content = ''; $sidebar = 'suffix_1'; }
	elseif ( get_option(PREFIX.'_sidebar_position')==1 ) { $content = ''; $sidebar = 'suffix_1'; }
	else { $content = 'suffix_1'; $sidebar = ''; }
	?>

	<?php if ($disable_header == 'false') {  

 			$category_image			= get_option(PREFIX . '_category_header_image_' . $cat_ID);
			$category_image_url 	= get_option(PREFIX . '_category_header_image_url_' . $cat_ID);
			$category_image_target 	= get_option(PREFIX . '_category_header_image_url_target_' . $cat_ID);
			$category_title 		= tfuse_qtranslate( get_option(PREFIX . '_category_header_title_' . $cat_ID) );
			?>
			<div class="header-title-image">
				<?php if ( $category_image!='' && $category_image_url!='' ) { ?><div class="image"><a href="<?php echo $category_image_url; ?>" target="<?php echo $category_image_target; ?>"><?php tfuse_get_image(708, 124, 'img', $category_image, '', false, 'slider-image'); ?></a></div><?php } 
				  elseif ( $category_image!='' ) { ?><div class="image"><?php tfuse_get_image(708, 124, 'img', $category_image, '', false, 'slider-image'); ?></div><?php } ?>
				<?php if ( $category_title!='' ) { ?><h1><?php echo $category_title ?></h1><?php } ?>
			</div>
			
 	<?php 
	}
	?>
	
	<!-- middle content -->
	<div class="container_24">
	
		<?php if ( $sidebar!=='' ) { ?>
			<!-- sidebar -->
			<div class="grid_6 <?php echo $sidebar; ?>">
				<?php get_sidebar(); ?>
			</div>
			<!--/ sidebar -->
		<?php } ?>
		
    	<div class="grid_17 <?php echo $content; ?>">
		  <div class="text">
			<?php if(have_posts()) : $count = 0; ?>
				<?php
					//top area for shortcodes
					$category_content_top = tfuse_qtranslate( get_option(PREFIX . '_category_content_top_' . $cat_ID) );
					if($category_content_top)
					{
						echo $category_content_top = apply_filters('themefuse_shortcodes',$category_content_top);
						echo '<div class="divider_space"></div>';
					}
				?>
			<?php while(have_posts()) : the_post(); $count++; 
			
					$large_image = get_post_meta($post->ID, PREFIX . "_post_image", true);
					$medium_image = get_post_meta($post->ID, PREFIX . "_post_image_medium", true);
					$small_image = get_post_meta($post->ID, PREFIX . "_post_image_small", true);
					$src_image = '';

					if($small_image != '')
						$src_image = $small_image;
					elseif($large_image != '')
						$src_image = $large_image;
					elseif($medium_image != '')
						$src_image = $medium_image;

					if($src_image != '')
					{
						$img_width = 234;
						$img_height = 112;

						$img_in = '<img src="' . tfuse_get_image($img_width, $img_height, 'src', $src_image, '', true) . '" alt="' . get_the_title() . '" class="alignleft" width="'.$img_width.'" height="'.$img_height.'" />';
					}
			?>
			
			    <div class="news-item">
	                <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
					<?php if ( $img_in!='' && $post->post_excerpt=='' ) echo $img_in; ?>
					<div class="entry">
						<?php the_excerpt(); ?>
       				</div>
                    <div class="news-meta"><a href="<?php the_permalink() ?>" class="link-more alignleft">Find out more</a> 
					<?php if(get_post_meta($post->ID, PREFIX . "_post_single_comments", true) != 'true') { ?>
					<a href="<?php comments_link(); ?>"><?php comments_number('0 comments', '1 comment', '% comments') ?> </a>
					<?php } ?>
					</div>
              	</div>

			<?php endwhile; ?>
				<?php
					//bottom area for shortcodes
					$category_content_bottom = tfuse_qtranslate( get_option(PREFIX . '_category_content_bottom_' . $cat_ID) );
					if($category_content_bottom)
					{
						echo $category_content_bottom = apply_filters('themefuse_shortcodes',$category_content_bottom);
						echo '<div class="divider_space"></div>';
					}
				?>
					<?php tfuse_pagination('cat=' . $cat_ID); ?>
			<?php else: ?>
				<h5><?php _e('Sorry, no posts matched your criteria.', 'tfuse') ?></h5>
			<?php endif; ?>
		   </div>
		</div>
		
		<?php if ( $content!='' ) { ?>
			<!-- sidebar -->
			<div class="grid_6 <?php echo $sidebar; ?>">
				<?php get_sidebar(); ?>
			</div>
			<!--/ sidebar -->
		<?php } ?>
	
		<div class="clear"></div>
	</div>
	<!--/ middle content -->
    
<?php get_footer(); ?>