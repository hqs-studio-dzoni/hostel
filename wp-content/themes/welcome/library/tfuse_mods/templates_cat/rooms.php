<?php 
get_header(); 
	
	$cat_ID = get_query_var('cat'); 
	$disable_header = get_option(PREFIX . '_category_disable_header_welcome_bar_' . $cat_ID); 
?>

<?php if ( $disable_header == 'false' ) { ?> 
	<div class="header">
		<div class="container">&nbsp;</div>  
	</div>
	<div class="header-line"></div>
<?php } ?>

<div class="middle">
<div class="container <?php if ($disable_header == 'true') { ?>padding50px<?php } ?>">

	<?php 
		if ( get_option(PREFIX . '_category_sidebar_position_' . $cat_ID)==1 ) { $content = 'suffix_1'; $sidebar = ''; }
	elseif ( get_option(PREFIX . '_category_sidebar_position_' . $cat_ID)==2 ) { $content = ''; $sidebar = 'suffix_1'; }
	elseif ( get_option(PREFIX.'_sidebar_position')==1 ) { $content = ''; $sidebar = 'suffix_1'; }
	else { $content = 'suffix_1'; $sidebar = ''; }
	?>

	<?php if ($disable_header == 'false') {  

 			$category_image			= get_option(PREFIX . '_category_header_image_' . $cat_ID);
			$category_image_url 	= get_option(PREFIX . '_category_header_image_url_' . $cat_ID);
			$category_image_target 	= get_option(PREFIX . '_category_header_image_url_target_' . $cat_ID);
			$category_title 		= tfuse_qtranslate( get_option(PREFIX . '_category_header_title_' . $cat_ID) );
			?>	
			<div class="header-title-image">
				<?php if ( $category_image!='' && $category_image_url!='' ) { ?><div class="image"><a href="<?php echo $category_image_url; ?>" target="<?php echo $category_image_target; ?>"><?php tfuse_get_image(708, 124, 'img', $category_image, '', false, 'slider-image'); ?></a></div><?php } 
				  elseif ( $category_image!='' ) { ?><div class="image"><?php tfuse_get_image(708, 124, 'img', $category_image, '', false, 'slider-image'); ?></div><?php } ?>
				<?php if ( $category_title!='' ) { ?><h1><?php echo $category_title ?></h1><?php } ?>
			</div>
			
 	<?php 
	}
	?>
	
	<!-- middle content -->
	<div class="container_24">
	
		<?php if ( $sidebar!=='' ) { ?>
			<!-- sidebar -->
			<div class="grid_6 <?php echo $sidebar; ?>">
				<?php get_sidebar(); ?>
			</div>
			<!--/ sidebar -->
		<?php } ?>
		
    	<div class="grid_17 <?php echo $content; ?>">
		  <div class="text">
		  <!-- Test Prices -->
		  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table-price custom-table">
                <thead>
                  <tr>
                    <td class="first"><span class="text-white"><strong>Rooms</strong></span></td>
                    <td><span><strong>Prices*</strong></span></td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Paul Gauguin 8-bed mixed dorm</td>
                    <td><strong><?php echo get_option('8_bed'); ?></strong></td>
                  </tr>
                  <tr>
                    <td>Toulouse Lautrec 6-bed mixed dorm</td>
                    <td><strong><?php echo get_option('6_bed'); ?></strong></td>
                  </tr>
                  <tr>
                    <td>Van Gogh 4-bed mixed dorm</td>
                    <td><strong><?php echo get_option('4_bed'); ?></strong></td>
                  </tr>
                  <tr>
                    <td>Pablo Picasso private twin/triple room</td>
                    <td><strong><?php echo get_option('private_twin_triple'); ?></strong></td>
                  </tr>
                  <tr>
                    <td>Montmartre En Suite for one person</td>
                    <td><strong><?php echo get_option('mont_for_one'); ?></strong></td>
                  </tr>
                   <tr>
                    <td>Montmartre En Suite for two persons</td>
                    <td><strong><?php echo get_option('mont_for_two'); ?></strong></td>
                  </tr>
                </tbody>
           </table>
            <p class="text-right"><span class="text-gray"><em>* prices per night, and are subject to change</em></span></p>
		  <!-- End Test Prices -->
			<?php if(have_posts()) : $count = 0; ?>
				<?php
					//top area for shortcodes
					$category_content_top = tfuse_qtranslate( get_option(PREFIX . '_category_content_top_' . $cat_ID) );
					if($category_content_top)
					{
						echo $category_content_top = apply_filters('themefuse_shortcodes',$category_content_top);
						echo '<div class="divider_space"></div>';
					}
				?>
			<?php while(have_posts()) : the_post(); $count++; ?>
			
                <div class="room-item">
                	<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
                    
                	<div class="image">
					
						<?php
						$post_video = get_post_meta($post->ID, PREFIX . "_post_video", true);
						$large_image = get_post_meta($post->ID, PREFIX . "_post_image", true);
						$small_image = get_post_meta($post->ID, PREFIX . "_post_image_small", true);
						$disablevideo = get_post_meta($post->ID, PREFIX . "_post_single_video", true);
						$disableimage = get_post_meta($post->ID, PREFIX . "_post_single_image", true);
						$disableprety = get_option(PREFIX . "_disable_lightbox");
						$src_image = '';

						if($post_video != '' && $disablevideo == 'true')
							$media = $post_video;
						elseif($large_image != '')
							$media = $large_image;
						elseif($medium_image != '')
							$media = $medium_image;
						else
							$media = $small_image;

						if($small_image != '')
							$src_image = $small_image;
						elseif($large_image != '')
							$src_image = $large_image;

						if($src_image != '')
						{
							$img_width = 234;
							$img_height = 112;

							$img_in = '<img src="' . tfuse_get_image($img_width, $img_height, 'src', $src_image, '', true) . '" alt="' . get_the_title() . '" width="'.$img_width.'" height="'.$img_height.'" />';

							//if($img_in != '' && $disableimage != 'true') Max asha o zis :)
							if($img_in != '')
							{
								if($disableprety != 'true')
								{
								?>
									<a href="<?php echo $media; ?>" rel="prettyPhoto[gallery<?php echo $post->ID; ?>]"><?php echo $img_in ?></a>
									<br />
									<a href="<?php echo $media; ?>" class="link-zoom" rel="prettyPhoto[gallery1<?php echo $post->ID; ?>]"><?php _e('View photo gallery', 'tfuse') ?></a>
								<?php
								}
								else
								{
									echo $img_in;
									?>
									<br />
									<a href="<?php echo $media; ?>" class="link-zoom" rel="prettyPhoto[gallery1<?php echo $post->ID; ?>]"><?php _e('View photo gallery', 'tfuse') ?></a>
								<?php 
								}
							}
							?>
						
							<div style="display:none;" class="gallery-image">
							<?php
							//get image from medial ibrary
							$attachments = get_children( array(
									'post_parent' => $post->ID,
									'numberposts' => -1,
									'post_type' => 'attachment',
									'post_mime_type' => 'image')
									);
		
							if($post_video != '' && $disablevideo == 'true')
							{
								if($large_image != '') $media = $large_image; elseif($medium_image != '') $media = $medium_image; else $media = $small_image;
							?>
								<a href="<?php echo $media; ?>" rel="prettyPhoto[gallery<?php echo $post->ID; ?>]"><?php echo $img_in ?></a>
								<a href="<?php echo $media; ?>" rel="prettyPhoto[gallery1<?php echo $post->ID; ?>]"><?php echo $img_in ?></a>
							<?php
							}
		
							if(!empty( $attachments ))
							{
								$size = 'full';
								foreach ($attachments as $att_id => $attachment)
								{
									$src = wp_get_attachment_image_src($att_id, $size, true);
									$image_link_attach = $src[0];
								?>
									<a href="<?php echo $image_link_attach; ?>" rel="prettyPhoto[gallery<?php echo $post->ID; ?>]"><?php echo wp_get_attachment_image($att_id, $size); ?></a>
									<a href="<?php echo $image_link_attach; ?>" rel="prettyPhoto[gallery1<?php echo $post->ID; ?>]"><?php echo wp_get_attachment_image($att_id, $size); ?></a>
								<?php
								}
							}
							//end attachement
							?>
							</div>
							
						<?php } ?>
					</div>
                    <div class="description">
                		<?php the_excerpt(); ?>
                    </div>
                    <div class="clear"></div>
                </div>

			<?php endwhile; ?>
				<?php
					//bottom area for shortcodes
					$category_content_bottom = tfuse_qtranslate( get_option(PREFIX . '_category_content_bottom_' . $cat_ID) );
					if($category_content_bottom)
					{
						echo $category_content_bottom = apply_filters('themefuse_shortcodes',$category_content_bottom);
						echo '<div class="divider_space"></div>';
					}
				?>
					<?php tfuse_pagination('cat=' . $cat_ID); ?>
			<?php else: ?>
				<h5><?php _e('Sorry, no posts matched your criteria.', 'tfuse') ?></h5>
			<?php endif; ?>
		   </div>
		</div>
		
		<?php if ( $content!='' ) { ?>
			<!-- sidebar -->
			<div class="grid_6 <?php echo $sidebar; ?>">
				<?php get_sidebar(); ?>
			</div>
			<!--/ sidebar -->
		<?php } ?>
	
		<div class="clear"></div>
	</div>
	<!--/ middle content -->
    
<?php get_footer(); ?>