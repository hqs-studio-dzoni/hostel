<div class="widget-container widget_search">
	<form method="get" id="searchform" action="<?php echo home_url( '/' ) ?>">
		<div>
			<input type="text" name="s" id="s" value="<?php _e('Search', 'tfuse') ?>" onfocus="if (this.value == '<?php _e('Search', 'tfuse') ?>') {this.value = '';}" onblur="if (this.value == '') {this.value = '<?php _e('Search', 'tfuse') ?>';}"/>
			<input type="submit" id="searchsubmit" value="<?php _e('Search', 'tfuse') ?>" onfocus="if (this.value == '<?php _e('Search', 'tfuse') ?>') {this.value = '';}" onblur="if (this.value == '') {this.value = '<?php _e('Search', 'tfuse') ?>';}"/>
		</div>
	</form>
</div>