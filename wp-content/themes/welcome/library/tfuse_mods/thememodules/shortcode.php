<?php

//************************************* FAQ
if ( is_file(THEME_MODULES.'/shortcodes/faq.php') ) require_once( THEME_MODULES.'/shortcodes/faq.php' );

//************************************* Buttons
if ( is_file(THEME_MODULES.'/shortcodes/button.php') ) require_once( THEME_MODULES.'/shortcodes/button.php' );

//************************************* Dropcaps
if ( is_file(THEME_MODULES.'/shortcodes/drop_cap.php') ) require_once( THEME_MODULES.'/shortcodes/drop_cap.php' );

//************************************* List Styles
if ( is_file(THEME_MODULES.'/shortcodes/list.php') ) require_once( THEME_MODULES.'/shortcodes/list.php' );

//************************************* Toggle Content
if ( is_file(THEME_MODULES.'/shortcodes/toggle_code.php') ) require_once( THEME_MODULES.'/shortcodes/toggle_code.php' );

//************************************* Quotes
if ( is_file(THEME_MODULES.'/shortcodes/quotes.php') ) require_once( THEME_MODULES.'/shortcodes/quotes.php' );

//************************************* Divider Styles
if ( is_file(THEME_MODULES.'/shortcodes/divider.php') ) require_once( THEME_MODULES.'/shortcodes/divider.php' );

//************************************* Rows
if ( is_file(THEME_MODULES.'/shortcodes/rows.php') ) require_once( THEME_MODULES.'/shortcodes/rows.php' );

//************************************* Columns
if ( is_file(THEME_MODULES.'/shortcodes/columns.php') ) require_once( THEME_MODULES.'/shortcodes/columns.php' );
//************************************* Link more
if ( is_file(THEME_MODULES.'/shortcodes/link_more.php') ) require_once( THEME_MODULES.'/shortcodes/link_more.php' );

//************************************* H Titles
if ( is_file(THEME_MODULES.'/shortcodes/title.php') ) require_once( THEME_MODULES.'/shortcodes/title.php' );

//************************************* Testimonials
if ( is_file(THEME_MODULES.'/shortcodes/testimonials.php') ) require_once( THEME_MODULES.'/shortcodes/testimonials.php' );

//************************************* Widget
if ( is_file(THEME_MODULES.'/shortcodes/widget.php') ) require_once( THEME_MODULES.'/shortcodes/widget.php' );

//************************************* Image Frames
if ( is_file(THEME_MODULES.'/shortcodes/image_frame.php') ) require_once( THEME_MODULES.'/shortcodes/image_frame.php' );

//************************************* Minigallery
if ( is_file(THEME_MODULES.'/shortcodes/minigallery.php') ) require_once( THEME_MODULES.'/shortcodes/minigallery.php' );

//************************************* Framed Tabs
if ( is_file(THEME_MODULES.'/shortcodes/framed_tabs.php') ) require_once( THEME_MODULES.'/shortcodes/framed_tabs.php' );

//************************************* Icons
if ( is_file(THEME_MODULES.'/shortcodes/icons.php') ) require_once( THEME_MODULES.'/shortcodes/icons.php' );

//************************************* Search form
if ( is_file(THEME_MODULES.'/shortcodes/search.php') ) require_once( THEME_MODULES.'/shortcodes/search.php' );

//************************************* Newsletter
if ( is_file(THEME_MODULES.'/shortcodes/newsletter.php') ) require_once( THEME_MODULES.'/shortcodes/newsletter.php' );

//********************************** LightBox
if ( is_file(THEME_MODULES.'/shortcodes/lightbox.php') ) require_once( THEME_MODULES.'/shortcodes/lightbox.php' );

//************************Video Players
if ( is_file(THEME_MODULES.'/shortcodes/video_player.php') ) require_once( THEME_MODULES.'/shortcodes/video_player.php' );

//************************Styled Boxs
if ( is_file(THEME_MODULES.'/shortcodes/style_box.php') ) require_once( THEME_MODULES.'/shortcodes/style_box.php' );

//********************* tablesshortcodesss
if ( is_file(THEME_MODULES.'/shortcodes/table.php') ) require_once( THEME_MODULES.'/shortcodes/table.php' );

//********************* HighLight
if ( is_file(THEME_MODULES.'/shortcodes/highlight.php') ) require_once( THEME_MODULES.'/shortcodes/highlight.php' );

//********************* Flickr
if ( is_file(THEME_MODULES.'/shortcodes/flickr.php') ) require_once( THEME_MODULES.'/shortcodes/flickr.php' );

//********************* Chart
if ( is_file(THEME_MODULES.'/shortcodes/chart.php') ) require_once( THEME_MODULES.'/shortcodes/chart.php' );

//********************* Slides
if ( is_file(THEME_MODULES.'/shortcodes/slides.php') ) require_once( THEME_MODULES.'/shortcodes/slides.php' );

// ************************Latest
if ( is_file(THEME_MODULES.'/shortcodes/latestposts.php') ) require_once( THEME_MODULES.'/shortcodes/latestposts.php' );

// ************************Popular
if ( is_file(THEME_MODULES.'/shortcodes/recent_comment.php') ) require_once( THEME_MODULES.'/shortcodes/recent_comment.php' );

// ************************Google Map
if ( is_file(THEME_MODULES.'/shortcodes/map.php') ) require_once( THEME_MODULES.'/shortcodes/map.php' );

//**************************Box
if ( is_file(THEME_MODULES.'/shortcodes/box.php') ) require_once( THEME_MODULES.'/shortcodes/box.php' );

//**************************Box
if ( is_file(THEME_MODULES.'/shortcodes/social.php') ) require_once( THEME_MODULES.'/shortcodes/social.php' );

?>