<?php

	/* Tfuse Shortcodes Generator Page */
	function tfuse_generate_shortcode_page(){
		global $tfuse;
		$prefix = $tfuse->prefix;
	 
	    $options     =  get_option("{$prefix}_template");      
	    $themeauthor =  get_option("{$prefix}_themeauthor");      
	    $themename   =  get_option("{$prefix}_themename");      
	    $authorurl1  =  get_option("{$prefix}_authorurl1");      
	    $authorurl2  =  get_option("{$prefix}_authorurl2");      
	    $authorname1 =  get_option("{$prefix}_authorname1");      
	    $authorname2 =  get_option("{$prefix}_authorname2");
	    $forumurl	 =  get_option("{$prefix}_forumurl");      
	    $manualurl   =  get_option("{$prefix}_manual"); 
	    
	     
	    $theme_data = get_theme_data( TEMPLATEPATH . '/style.css' );
	    $local_version = $theme_data['Version'];
	    $theme_version = '<span class="version">version '. $local_version .'</span>';
	?>
</strong>

		<style>
		 #contextual-help-link-wrap{
			display: none;
			}
		</style>

<div class="wrap" id="tfuse_fields">
		<div style="height:15px;">&nbsp;</div>
		<div class="tfuse_header">
			<div class="header_icon_bg">
				<a href="http://www.themefuse.com" target="_blank" title="Go to ThemeFuse"><img class="header_icon" src="<?php echo ADMIN_IMAGES;?>/thumb.png" width="70%" height="70%" /></a>
			</div>
			<!-- .header_icon_bg -->
			
			<div class="header_text">
				<h3><?php echo $themename; ?></h3>
				<a href="http://www.themefuse.com" target="_blank" title="Go to ThemeFuse"><img src="<?php echo ADMIN_IMAGES;?>/by_tfuse.png" /></a>
				<div class="clear"></div>
				
				<div class="links">
					<a target="_blank" href="<?php echo $manualurl; ?>">Online documentation</a>&nbsp;&nbsp;<span>|</span>&nbsp;&nbsp;<a target="_blank" href="<?php echo $forumurl; ?>">Support Forums</a>
					<?php echo $theme_version; ?>
				</div>
			</div>
			<!-- .header_text -->
			
			<div class="clear"></div>
		</div>
		<!-- .tfuse_fheader -->
	<?php 
	
	//Begin shortcode array
		$shortcodes = array();
		
		if ( is_file(THEME_MODULES.'/shortcodes/drop_cap.php') ) {
			
			$shortcodes['dropcap1'] = array(
				'attr' => array(),
				'desc' => array(),
				'content' => TRUE, 
			);
			
			$shortcodes['dropcap2'] = array(
				'attr' => array(),
				'desc' => array(),
				'content' => TRUE, 
			);
		}
		
		if ( is_file(THEME_MODULES.'/shortcodes/quotes.php') ) {
		
			$shortcodes['quote_right'] = array(
				'attr' => array(),
				'desc' => array(),
				'content' => TRUE,
			);
				
			$shortcodes['quote_left'] = array(
				'attr' => array(),
				'desc' => array(),
				'content' => TRUE,
			);
				
			$shortcodes['blockquote'] = array(
				'attr' => array(),
				'desc' => array(),
				'content' => TRUE,
			);
		}

		if ( is_file(THEME_MODULES.'/shortcodes/button.php') ) {
			
			$shortcodes['button'] = array(
				'attr' => array(
					'link' => 'text',
					'style' => 'text',
					'target' => 'select',
				),
				'desc' => array(
					'link' => 'Enter URL for button',
					'style' => 'Enter your style',
					'target' => 'Select where the new document will be displayed',
					),
					'options' => array(
				'_self' => 'Open link in same link',
				'_blank' => 'Open link in new window',
				),
				'content' => TRUE,
				'content_text' => 'Enter text on button',
			);
		}		
	
		if ( is_file(THEME_MODULES.'/shortcodes/lightbox.php') ) {
	
			$shortcodes['lightbox'] = array(
				'attr' => array(
					'title' => 'text',
					'color' => 'text',
					'type' => 'select',
					'title' => 'text',
					'link' => 'text',
				),
				'desc' => array(
					'link' => 'Enter URL for Lightbox',
					'align' => 'Button Alignment',
					'color' => 'Enter background color code ex. #000000',
					'title' => 'Enter your title for lightbox',
				),
				'options' => array(
					'image' => 'Image',
					'iframe' => 'iFrame',
					'youtube' => 'Youtube Video',
					'vimeo' => 'Vimeo Video',
				),
				'content' => TRUE,
				'content_text' => 'Enter content (can be normal text, HTML code or shortcode)',
				);
	
			$shortcodes['lightbox_btn'] = array(
				'attr' => array(
					'title' => 'text',
					'color' => 'text',
					'type' => 'select',
					'title' => 'text',
					'link' => 'text',
				),
				'desc' => array(
					'link' => 'Enter URL for Lightbox Button',
					'align' => 'Button Alignment',
					'color' => 'Enter background color code ex.green',
					'title' => 'Enter your title for lightbox',
				),
				'options' => array(
					'image' => 'Image',
					'iframe' => 'iFrame',
					'youtube' => 'Youtube Video',
					'vimeo' => 'Vimeo Video',
				),
				'content' => TRUE,
				'content_text' => 'Enter content (can be normal text, HTML code or shortcode)',
				);
		}
		
		if ( is_file(THEME_MODULES.'/shortcodes/image_frame.php') ) {
			
			$shortcodes['frame'] = array(
				'attr' => array(
					  'title' => 'text',
					  'href' => 'text',
					  'src' => 'text',
					  'width' => 'text',
					  'height' => 'text',
					  'type' => 'select',
					  'target' => 'text'
				 ),
				'desc' => array(
					 'src' => 'Enter image URL',
					 'href' => 'Enter hyperlink URL for image',
					 'type' => 'Select prefered image position',
					 'width' => 'Enter width for image: 250',
					 'height' => 'Enter width for image: 250',	
					 'title' => 'Enter title for this frame',
					 'target' => 'Enter where the image will be displayed, ex: <strong>_blank</strong> or <strong>_self</strong>'
				 ),
				 'options' => array(
					'left' => 'Left Position',
					'center' => 'Center Position',
					'right' => 'Right Position',
				),
				'content' => FALSE,
				'content_text' => 'Image Caption',
				);
		}
		
		if ( is_file(THEME_MODULES.'/shortcodes/columns.php') ) {
		
			$shortcodes['col_1'] = array(
				'attr' => array(),
				'content' => TRUE,
				'style' => 'text'
			);
			$shortcodes['col_1_2'] = array(
				'attr' => array(),
				'content' => TRUE,
				'style' => 'text'
			);
			$shortcodes['col_1_3'] = array(
				'attr' => array(),
				'content' => TRUE,
				'style' => 'text'
			);
			$shortcodes['col_1_4'] = array(
				'attr' => array(),
				'content' => TRUE,
				'style' => 'text'
			);
			$shortcodes['col_1_5'] = array(
				'attr' => array(),
				'content' => TRUE,
				'style' => 'text'
			);
			$shortcodes['col_1_6'] = array(
				'attr' => array(),
				'content' => TRUE,
				'style' => 'text'
			);
			$shortcodes['col_1_7'] = array(
				'attr' => array(),
				'content' => TRUE,
				'style' => 'text'
			);
			$shortcodes['col_1_12'] = array(
				'attr' => array(),
				'content' => TRUE,
				'style' => 'text'
			);
			$shortcodes['col_2_3'] = array(
				'attr' => array(),
				'content' => TRUE,
				'style' => 'text'
			);
			$shortcodes['col_2_5'] = array(
				'attr' => array(),
				'content' => TRUE,
				'style' => 'text'
			);
			$shortcodes['col_3_4'] = array(
				'attr' => array(),
				'content' => TRUE,
				'style' => 'text'
			);
			$shortcodes['col_3_5'] = array(
				'attr' => array(),
				'content' => TRUE,
				'style' => 'text'
			);
			$shortcodes['col_3_8'] = array(
				'attr' => array(),
				'content' => TRUE,
				'style' => 'text'
			);
			$shortcodes['col_4_5'] = array(
				'attr' => array(),
				'content' => TRUE,
				'style' => 'text'
			);
			$shortcodes['col_5_6'] = array(
				'attr' => array(),
				'content' => TRUE,
				'style' => 'text'
			);
			$shortcodes['col_5_8'] = array(
				'attr' => array(),
				'content' => TRUE,
				'style' => 'text'
		
			);
		}
		
		if ( is_file(THEME_MODULES.'/shortcodes/style_box.php') ) {
		
			$shortcodes['styled_box'] = array(
				'attr' => array(
					'title' => 'text',
					'color' => 'select',
				),
				'desc' => array(
					'href' => 'Enter URL for button',
					'color' => 'Select box color',
				),
				'options' => array(
					'black' => 'Black',
					'gray' => 'Gray',
					'white' => 'White',
					'blue' => 'Blue',
					'yellow' => 'Yellow',
					'red' => 'Red',
					'orange' => 'Orange',
					'green' => 'Green',
					'pink' => 'Pink',
					'purple' => 'Purple',
				),
				'content' => TRUE,
				'content_text' => 'Enter Content',
			);
		}
	
		if ( is_file(THEME_MODULES.'/shortcodes/video_player.php') ) {
		
			$shortcodes['youtube'] = array(
				'attr' => array(
					'width' => 'text',
					'height' => 'text',
					'link' => 'text',
				),
				'desc' => array(
					'width' => 'Video width in pixels',
					'height' => 'Video height in pixels',
					'video_id' => 'Youtube video ID something like Js9Z8UQAA4E',
				),
				'content' => FALSE,
				);	
		
			$shortcodes['vimeo'] = array(
				'attr' => array(
					'width' => 'text',
					'height' => 'text',
					'link' => 'text',
				),
				'desc' => array(
					'width' => 'Video width in pixels',
					'height' => 'Video height in pixels',
					'video_id' => 'Youtube video ID something like Js9Z8UQAA4E',
				),
				'content' => FALSE,
				);	
	
			$shortcodes['html5video'] = array(
				'attr' => array(
					'width' => 'text',
					'height' => 'text',
					'poster' => 'text',
					'mp4' => 'text',
					'webm' => 'text',
					'ogg' => 'text',
				),
				'desc' => array(
					'width' => 'Video width in pixels',
					'height' => 'Video height in pixels',
					'poster' => 'Poster image for video',
					'mp4' => 'Video URL in mp4 format',
					'webm' => 'Video URL in mp4 webm',
					'ogg' => 'Video URL in mp4 ogg',
				),
				'content' => FALSE,
			);
		}
		
	if ( is_file(THEME_MODULES.'/shortcodes/slides.php') ) {
		
		$shortcodes['slideshow'] = array(
			'attr' => array(
				'width' => 'text',
				'height' => 'text',
			),
			'desc' => array(
				'width' => 'Slideshow width in pixels',
				'height' => 'Slideshow height in pixels',
			),
			'content' => TRUE,
			'content_text' => htmlentities('Your Images URL (line by line) ex. /example/photo1.jpg'),
		);
		
		$shortcodes['nivoslide'] = array(
			'attr' => array(
				'width' => 'text',
				'height' => 'text',
				'effect' => 'select',
				'pauseTime' => 'text',
			),
			'options' => array(
				'sliceDown' => 'sliceDown',
				'sliceDownLeft' => 'sliceDownLeft',
				'sliceUp' => 'sliceUp',
				'sliceUpLeft' => 'sliceUpLeft',
				'sliceUpDown' => 'sliceUpDown',
				'sliceUpDownLeft' => 'sliceUpDownLeft',
				'fold' => 'fold',
				'fade' => 'fade',
				'random' => 'random',
			),
			'desc' => array(
				'width' => 'Slideshow width in pixels',
				'height' => 'Slideshow height in pixels',
				'effect' => 'The effect parameter can be any of the following',
				'pauseTime' => 'Enter pause time for each slide (in seconds)',
			),
			'content' => TRUE,
			'content_text' => htmlentities('Your Images URL (line by line) ex. /example/photo1.jpg'),
			);
		}
		
	if ( is_file(THEME_MODULES.'/shortcodes/raw.php') ) {
		
		$shortcodes['raw'] = array(
			'attr' => array(),
			'content' => TRUE,
		);
	}
						
	if ( is_file(THEME_MODULES.'/shortcodes/faq.php') ) {
	
		$shortcodes['faq'] = array(
			'attr' => array(),
			'content' => TRUE,
		);
	
		$shortcodes['faq_question'] = array(
			'attr' => array(),
			'content' => TRUE,
		);
	
		$shortcodes['faq_answer'] = array(
			'attr' => array(),
			'content' => TRUE,
		);
	}							

	if ( is_file(THEME_MODULES.'/shortcodes/list.php') ) {
		
		$shortcodes['arrow_list'] = array(
			'attr' => array(),
			'content' => TRUE,
		);
	
		$shortcodes['check_list'] = array(
			'attr' => array(),
			'content' => TRUE,
		);
	
		$shortcodes['delete_list'] = array(
			'attr' => array(),
			'content' => TRUE,
		);
	}
	
	if ( is_file(THEME_MODULES.'/shortcodes/toggle_code.php') ) {
		
		$shortcodes['toggle_content'] = array(
			'attr' => array(
			'title' => 'text',
			),
			'content' => TRUE,
		);
		$shortcodes['toggle_code'] = array(
			'attr' => array(
			'title' => 'text',
			'brush' => 'text',
			),
			'content' => TRUE,
		);
		$shortcodes['code'] = array(
			'attr' => array(
			'brush' => 'text',
			),
			'content' => TRUE,
		);
	}
	
	if ( is_file(THEME_MODULES.'/shortcodes/divider.php') ) {
	
		$shortcodes['divider_space'] = array(
			'attr' => array(),
			'content' => FALSE,
		);
		
		$shortcodes['divider_thin'] = array(
			'attr' => array(),
			'content' => FALSE,
		);
		
		$shortcodes['divider'] = array(
			'attr' => array(),
			'content' => FALSE,
		);
		
		$shortcodes['clear'] = array(
			'attr' => array(),
			'content' => FALSE,
		);
		
		$shortcodes['clearboth'] = array(
			'attr' => array(),
			'content' => FALSE,
		);
	}
	
	if ( is_file(THEME_MODULES.'/shortcodes/rows.php') ) {
	
		$shortcodes['row_box'] = array(
			'attr' => array(
			'style' => 'text',
			),
			'content' => TRUE,
		);
		
		$shortcodes['row'] = array(
			'attr' => array(),
			'content' => TRUE,
		);
	}
	
	if ( is_file(THEME_MODULES.'/shortcodes/link_more.php') ) {
		
		$shortcodes['link_more'] = array(
			'attr' => array(),
			'url' => 'text',
			'text' => 'text',
			'content' => FALSE,
		);
	}
	
	if ( is_file(THEME_MODULES.'/shortcodes/title.php') ) {
	
		$shortcodes['title'] = array(
			'attr' => array(
				'title' => 'select',
			),
			'options' => array(
				'h1' => 'h1',
				'h2' => 'h2',
				'h3' => 'h3',
				'h4' => 'h4',
				'h5' => 'h5',
				'h6' => 'h6',
			),
			'content' => TRUE,
		);
	}
	
	if ( is_file(THEME_MODULES.'/shortcodes/testimonials.php') ) {
		
		$shortcodes['testimonials'] = array(
			'attr' => array(),
			'content' => FALSE,
		);
	}
	
	if ( is_file(THEME_MODULES.'/shortcodes/widget.php') ) {
	
		$shortcodes['widget'] = array(
			'attr' => array(),
			'content' => FALSE,			
			'widget_name' => 'text'
		);
	}
	
	if ( is_file(THEME_MODULES.'/shortcodes/minigallery.php') ) {
	
		$shortcodes['minigallery'] = array(
			'attr' => array(
				'order'      => 'text',
				'orderby'    => 'text',
				'id'         => 'text',
				'include'    => 'text',
				'exclude'    => 'text',
				'style'    => 'text'
			),
			'content' => FALSE,	
			'desc' => array(
				'order'      => 'ASC',
				'orderby'    => 'menu_order ID',
				'id'         => 33,
				'include'    => '',
				'exclude'    => '',
				'style'    => 'box border box_white'
			),
		);
	}
	
	if ( is_file(THEME_MODULES.'/shortcodes/framed_tabs.php') ) {
	
		$shortcodes['framed_tabs'] = array(
			'attr' => array(
				'title' => 'text'
			),
			'content' => TRUE,
		);
		
		$shortcodes['tab'] = array(
			'attr' => array(
				'title' => '', 
				'icon' => 'icon.png', 
				'width' => '51', 
				'height' => '42'
			),
			'content' => TRUE,
		);
	}
	
	if ( is_file(THEME_MODULES.'/shortcodes/icons.php') ) {
	
		$shortcodes['icon_check'] = array(
			'attr' => array(),
			'content' => FALSE,
		);
		
		$shortcodes['icon_x'] = array(
			'attr' => array(),
			'content' => FALSE,
		);
	}
	
	if ( is_file(THEME_MODULES.'/shortcodes/search.php') ){
	
		$shortcodes['search'] = array(
			'attr' => array(),
			'content' => FALSE,
		);
	}
	
	if ( is_file(THEME_MODULES.'/shortcodes/newsletter.php') ) {
	
		$shortcodes['newsletter'] = array(
			'attr' => array(
				'action' => 'text', 
				'title' => 'text', 
				'text' => 'text'
			),
			'content' => FALSE,
		);
	}
	
	if ( is_file(THEME_MODULES.'/shortcodes/table.php') ) {
	
		$shortcodes['table'] = array(
			'attr' => array(
				'color' =>'text'
			),
			'content' => TRUE,
		);
	}
	
	if ( is_file(THEME_MODULES.'/shortcodes/highlight.php') ){
	
		$shortcodes['highlight'] = array(
			'attr' => array(
				'color' =>'text'
			),
			'content' => TRUE,
			'desc' => array(
				'color' => 'yellow'
			),
		);
	}
	
	if ( is_file(THEME_MODULES.'/shortcodes/flickr.php') ) {
	
		
		$shortcodes['flickr'] = array(
			'attr' => array(
				'items' => 'text',
				'flickr_id' => 'text'
			),
			'desc' => array(
				'items' => 8,
				'flickr_id' => '51362473@N05'
			),
			'content' => FALSE,
		);
	}
	
	if ( is_file(THEME_MODULES.'/shortcodes/chart.php') ) {
	
		$shortcodes['chart'] = array(
			'attr' => array(
				'width' => 'text',
				'height' => 'text',
				'type' => 'select',
				'title' => 'text',
				'data' => 'text',
				'label' => 'text',
				'colors' => 'text',
			),
			'options' => array(
				'3dpie' => '3D PIE CHART',
				'pie' => '2D PIE CHART',
				 'line'=> 'LINE CHART',
				'bvs' => 'BAR CHART',
			),
			'desc' => array(
				'width' => 590,
				'height' => 250,
				'type' => 'select of chart',
				'title' => '',
				'data' => '',
				'label' => '',
				'colors' => '4f762a,2c353d,999999,cccccc',
			),
			'content' => FALSE,
		);
	}
	
	if ( is_file(THEME_MODULES.'/shortcodes/latestposts.php') ) {
		$shortcodes['latestpost'] = array(
			'attr' => array(
				'title' => 'text',
				'width' => 'text',
				'height' => 'text',
				'number' => 'text',
			),
				'content' => FALSE,
			'desc' => array(
				'title' => 'Recent Posts',
				'width' => 96,
				'height' => 96,
				'number' => 5
			),
		);
	}
	
	if ( is_file(THEME_MODULES.'/shortcodes/latestposts.php') ) {
		$shortcodes['recent_comment'] = array(
			'attr' => array(
				'title' => 'text',
				'width' => 'text',
				'height' => 'text',
				'number' => 'text',
			),
				'content' => FALSE,
			'desc' => array(
				'title' => 'Recent Posts',
				'width' => 96,
				'height' => 96,
				'number' => 5
			),
		);
	}
	
	if ( is_file(THEME_MODULES.'/shortcodes/map.php') ) {
		
		$shortcodes['map'] = array(
			'attr' => array(
				'width' => 'text',
				'height' => 'text',
				'lat' => 'text',
				'long' => 'text',
				'zoom' => 'text',
				'type' => 'text',
				'address' => 'text',
			),
			'content' => FALSE,
			'desc' => array(
				'width' => 400,
				'height' => 300,
				'lat' => 0,
				'long' => 0,
				'zoom' => 12,
				'type' => '',
				'address' => '',
			),
		);
	}
	
	if ( is_file(THEME_MODULES.'/shortcodes/box.php') ) {
		
			$shortcodes['box'] = array(
			'attr' => array(
				'style' => 'text',
				'type' => 'select',
			),
			'options' => array(
				'download_box' => 'Download Box',
				'info_box' => 'Info Box',
				'note_box'=> 'Note Box',
				'warning_box' => 'Warning Box',
			),
			'content' => TRUE,
			'desc' => array(
				'style' => 'Enter style for box',
				'type' => 'Select box type',
			),
		);
	}
	
	if ( is_file(THEME_MODULES.'/shortcodes/social.php') ) {
		
			$shortcodes['twitter'] = array(
			'attr' => array(
				'username' => 'text',
				'items' => 'text',
			),
			'content' => FALSE,
			'desc' => array(
				'username' => 'Enter your usermane of twitter',
				'items' => 'Enter number of itmes, ex: 7',
			),
		);
	}
?>	
<script>
jQuery(document).ready(function(){ 
	jQuery('#shortcode_select').change(function() {
  		var target = jQuery(this).val();
  		jQuery('.rm_section').css('display', 'none');
  		jQuery('#div_'+target).css('display', '');
	});	
	
	jQuery('.code_area').click(function() { 
		document.getElementById(jQuery(this).attr('id')).focus();
    	document.getElementById(jQuery(this).attr('id')).select();
	});
	
	jQuery('.button').click(function() { 
		var target = jQuery(this).attr('id');
		var gen_shortcode = '';
  		gen_shortcode+= '['+target;
  		
  		if(jQuery('#'+target+'_attr_wrapper .attr').length > 0)
  		{
  			jQuery('#'+target+'_attr_wrapper .attr').each(function() {
				gen_shortcode+= ' '+jQuery(this).attr('name')+'="'+jQuery(this).val()+'"';
			});
		}
		
		gen_shortcode+= ']\n';
		
		if(jQuery('#'+target+'_content').length > 0)
  		{
  			gen_shortcode+= jQuery('#'+target+'_content').val()+'\n[/'+target+']\n';
  			
  			var repeat = jQuery('#'+target+'_content_repeat').val();
  			for (count=1;count<=repeat;count=count+1)
			{
				if(count<repeat)
				{
					gen_shortcode+= '['+target+']\n';
					gen_shortcode+= jQuery('#'+target+'_content').val()+'\n[/'+target+']\n';
				}
				else
				{
					gen_shortcode+= '['+target+'_last]\n';
					gen_shortcode+= jQuery('#'+target+'_content').val()+'\n[/'+target+'_last]';
				}
			}
  		}
  		
  		jQuery('#'+target+'_code').val(gen_shortcode);
	});
});
</script>

<div style="padding:30px 20px 30px 20px;background:none; width:715px;">
	<h3>Shortcodes Generator</h3>
	<?php
		if(!empty($shortcodes))
		{
	?>
			<strong>Select Shortcode:</strong>
			<select id="shortcode_select">
				<option value="">---Select---</option>
			
	<?php
			foreach($shortcodes as $shortcode_name => $shortcode)
			{
	?>
	
			<option value="<?php echo $shortcode_name; ?>"><?php echo $shortcode_name; ?></option>
	
	<?php
			}
	?>
			</select>
	<?php
		}
	?>
	
	<br/><br/>
	
	<?php
		if(!empty($shortcodes))
		{
			foreach($shortcodes as $shortcode_name => $shortcode)
			{
	?>
	
			<div id="div_<?php echo $shortcode_name; ?>" class="rm_section" style="display:none">
				<div class="rm_title">
					<h3 style="margin-left:15px"><?php echo ucfirst($shortcode_name); ?></h3>
					<div class="clearfix"></div>
				</div>
				
				<div class="rm_input rm_text" style="padding-left:20px">
				
				<!-- img src="<?php echo $plugin_url.'/'.$shortcode_name.'.png'; ?>" alt=""/><br/><br/><br/ -->
				
				<?php
					if(isset($shortcode['content']) && $shortcode['content'])
					{
						if(isset($shortcode['content_text']))
						{
							$content_text = $shortcode['content_text'];
						}
						else
						{
							$content_text = 'Your Content';
						}
				?>
				
				<strong><?php echo $content_text; ?>:</strong><br/>
				<input type="hidden" id="<?php echo $shortcode_name; ?>_content_repeat" value="<?php echo $shortcode['repeat']; ?>"/>
				<textarea id="<?php echo $shortcode_name; ?>_content" style="width:90%;height:70px" rows="3" wrap="off"></textarea><br/><br/>
				
				<?php
					}
				?>
			
				<?php
					if(isset($shortcode['attr']) && !empty($shortcode['attr']))
					{
				?>
						
						<div id="<?php echo $shortcode_name; ?>_attr_wrapper">
						
				<?php
						foreach($shortcode['attr'] as $attr => $type)
						{
				?>
				
							<?php echo '<strong>'.ucfirst($attr).'</strong>: '.$shortcode['desc'][$attr]; ?><br/>
							
							<?php
								switch($type)
								{
									case 'text':
							?>
							
									<input type="text" id="<?php echo $shortcode_name; ?>_text" style="width:90%" class="attr" name="<?php echo $attr; ?>"/>
							
							<?php
									break;
									
									case 'select':
							?>
							
									<select id="<?php echo $shortcode_name; ?>_select" style="width:25%" class="attr" name="<?php echo $attr; ?>">
									
										<?php
											if(isset($shortcode['options']) && !empty($shortcode['options']))
											{
												foreach($shortcode['options'] as $select_key => $option)
												{
										?>
										
													<option value="<?php echo $select_key; ?>"><?php echo $option; ?></option>
										
										<?php	
												}
											}
										?>							
									
									</select>
							
							<?php
									break;
								}
							?>
							
							<br/><br/>
				
				<?php
						} //end attr foreach
				?>
				
						</div>
				
				<?php
					}
				?>
				<br/>
				
				<input type="button" id="<?php echo $shortcode_name; ?>" value="Generate Shortcode" class="button"/>
				
				<br/><br/><br/>
				
				<strong>Shortcode:</strong><br/>
				<textarea id="<?php echo $shortcode_name; ?>_code" style="width:90%;height:70px" rows="3" readonly="readonly" class="code_area" wrap="off"></textarea>
				
				</div>
				
			</div>
	
	<?php
			} //end shortcode foreach
		}
	?>
	
	</div>
</div>
<br style="clear:both"/>









        <div style="clear:both;"></div>   
</div>

 <?php
}

?>