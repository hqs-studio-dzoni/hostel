<?php
//************************************* Box Styles
function tfuse_box($atts, $content = null)
{
	extract(shortcode_atts(array('style' => '','type'=>'',), $atts));
	return '<div class="box ' . $style . ' '.$type.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('box', 'tfuse_box');

?>
