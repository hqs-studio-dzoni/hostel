<?php 
//************************************* Buttons

function tfuse_button($atts, $content = null)
{
	extract(shortcode_atts(array('link' => '#', 'style' => '','target' => '_self', 'size' => ''), $atts));
	if($size == 'large') $size = 'large_button';
	$out = '<a class="button_link '.$style.' '.$size.'" href="' . $link . '" target="'.$target.'"><span>' . do_shortcode($content) . '</span></a>';
	return $out;
}
add_shortcode('button', 'tfuse_button'); 

?>