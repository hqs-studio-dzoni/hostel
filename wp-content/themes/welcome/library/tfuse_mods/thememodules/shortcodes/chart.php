<?php
// ***********************Chart
function tfuse_chart($atts) {

	//extract short code attr
	extract(shortcode_atts(array(
		'width' => 590,
		'height' => 250,
		'type' => '',
		'title' => '',
		'data' => '',
		'label' => '',
		'colors' => '',
	), $atts));
	
	switch($type)
	{
		case '3dpie':
			$type = 'p3';
		break;
		case 'pie':
			$type = 'p';
		break;
		case 'line':
			$type = 'lc';
		break;
		case 'bar':
			$type = 'bvg';
		break;
		case 'map':
			$type = 'map';
		break;
	}
	if ($type)
	$return_html = '<img src="http://chart.apis.google.com/chart?cht='.$type.'&#038;chtt='.$title.'&#038;chl='.$label.'&#038;chco='.$colors.'&#038;chs='.$width.'x'.$height.'&#038;chd=t:'.$data.'&#038;chf=bg,s, '.$colors.'" alt="'.$title.'" class="frame img_nofade" />';

	return $return_html;
}
add_shortcode('chart', 'tfuse_chart');

?>