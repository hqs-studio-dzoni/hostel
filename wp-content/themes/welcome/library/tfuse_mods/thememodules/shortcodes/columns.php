<?php
//************************************* Columns
function tfuse_col_1($atts, $content = null)
{
	extract(shortcode_atts(array('style' => ''), $atts));
	return '<div class="col col_1 '.$style.'"><div class="inner">' . do_shortcode($content) . '</div></div>';
}
add_shortcode('col_1', 'tfuse_col_1');

function tfuse_col_1_2($atts, $content = null)
{
	extract(shortcode_atts(array('style' => ''), $atts));
	return '<div class="col col_1_2 '.$style.'"><div class="inner">' . do_shortcode($content) . '</div></div>';
}
add_shortcode('col_1_2', 'tfuse_col_1_2');

function tfuse_col_1_3($atts, $content = null)
{
	extract(shortcode_atts(array('style' => ''), $atts));
	return '<div class="col col_1_3 '.$style.'"><div class="inner">' . do_shortcode($content) . '</div></div>';
}
add_shortcode('col_1_3', 'tfuse_col_1_3');

function tfuse_col_1_7($atts, $content = null)
{
	extract(shortcode_atts(array('style' => ''), $atts));
	return '<div class="col col_1_7 '.$style.'"><div class="inner">' . do_shortcode($content) . '</div></div>';
}
add_shortcode('col_1_7', 'tfuse_col_1_7');

function tfuse_col_1_4($atts, $content = null)
{
	extract(shortcode_atts(array('style' => ''), $atts));
	return '<div class="col col_1_4 '.$style.'"><div class="inner">' . do_shortcode($content) . '</div></div>';
}
add_shortcode('col_1_4', 'tfuse_col_1_4');

function tfuse_col_1_5($atts, $content = null)
{
	extract(shortcode_atts(array('style' => ''), $atts));
	return '<div class="col col_1_5 '.$style.'"><div class="inner">' . do_shortcode($content) . '</div></div>';
}
add_shortcode('col_1_5', 'tfuse_col_1_5');

function tfuse_col_1_6($atts, $content = null)
{
	extract(shortcode_atts(array('style' => ''), $atts));
	return '<div class="col col_1_6 '.$style.'"><div class="inner">' . do_shortcode($content) . '</div></div>';
}
add_shortcode('col_1_6', 'tfuse_col_1_6');

function tfuse_col_1_12($atts, $content = null)
{
	extract(shortcode_atts(array('style' => ''), $atts));
	return '<div class="col col_1_12 '.$style.'"><div class="inner">' . do_shortcode($content) . '</div></div>';
}
add_shortcode('col_1_12', 'tfuse_col_1_12');

function tfuse_col_2_3($atts, $content = null)
{
	extract(shortcode_atts(array('style' => ''), $atts));
	return '<div class="col col_2_3 '.$style.'"><div class="inner">' . do_shortcode($content) . '</div></div>';
}
add_shortcode('col_2_3', 'tfuse_col_2_3');

function tfuse_col_2_5($atts, $content = null)
{
	extract(shortcode_atts(array('style' => ''), $atts));
	return '<div class="col col_2_5 '.$style.'"><div class="inner">' . do_shortcode($content) . '</div></div>';
}
add_shortcode('col_2_5', 'tfuse_col_2_5');

function tfuse_col_3_4($atts, $content = null)
{
	extract(shortcode_atts(array('style' => ''), $atts));
	return '<div class="col col_3_4 '.$style.'"><div class="inner">' . do_shortcode($content) . '</div></div>';
}
add_shortcode('col_3_4', 'tfuse_col_3_4');

function tfuse_col_3_5($atts, $content = null)
{
	extract(shortcode_atts(array('style' => ''), $atts));
	return '<div class="col col_3_5 '.$style.'"><div class="inner">' . do_shortcode($content) . '</div></div>';
}
add_shortcode('col_3_5', 'tfuse_col_3_5');

function tfuse_col_3_8($atts, $content = null)
{
	extract(shortcode_atts(array('style' => ''), $atts));
	return '<div class="col col_3_8 '.$style.'"><div class="inner">' . do_shortcode($content) . '</div></div>';
}
add_shortcode('col_3_8', 'tfuse_col_3_8');

function tfuse_col_4_5($atts, $content = null)
{
	extract(shortcode_atts(array('style' => ''), $atts));
	return '<div class="col col_4_5 '.$style.'"><div class="inner">' . do_shortcode($content) . '</div></div>';
}
add_shortcode('col_4_5', 'tfuse_col_4_5');

function tfuse_col_5_6($atts, $content = null)
{
	extract(shortcode_atts(array('style' => ''), $atts));
	return '<div class="col col_5_6 '.$style.'"><div class="inner">' . do_shortcode($content) . '</div></div>';
}
add_shortcode('col_5_6', 'tfuse_col_5_6');

function tfuse_col_5_8($atts, $content = null)
{
	extract(shortcode_atts(array('style' => ''), $atts));
	return '<div class="col col_5_8 '.$style.'"><div class="inner">' . do_shortcode($content) . '</div></div>';
}
add_shortcode('col_5_8', 'tfuse_col_5_8');

?>