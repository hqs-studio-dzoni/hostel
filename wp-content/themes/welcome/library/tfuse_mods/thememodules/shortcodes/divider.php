<?php
//************************************* Divider Styles
function tfuse_divider_space($atts, $content = null)
{
	return '<div class="divider_space"></div>';
}
add_shortcode('divider_space', 'tfuse_divider_space');

function tfuse_divider_thin($atts, $content = null)
{
	return '<div class="divider_thin"></div>';
}
add_shortcode('divider_thin', 'tfuse_divider_thin');

function tfuse_divider($atts, $content = null)
{
	return '<div class="divider"></div>';
}
add_shortcode('divider', 'tfuse_divider');

function tfuse_clear($atts, $content = null)
{
	return '<div class="clear"></div>';
}
add_shortcode('clear', 'tfuse_clear');

function tfuse_clearboth($atts, $content = null)
{
	return '<div class="clearboth"></div>';
}
add_shortcode('clearboth', 'tfuse_clearboth');

?>