<?php 

//************************************* FAQ
function tfuse_faq($atts, $content = null)
{
	global $faq_question, $faq_answer;
	$faq_question = $faq_answer = '';
	$get_faqs = do_shortcode($content);
	$k = 0;
	$out = '<div class="faq_list">';
	while(isset($faq_question[$k]))
	{
		$out .= $faq_question[$k];
		$out .= $faq_answer[$k];
		$k++;
	}
	$out .= '
	</div>';
	return $out;
}
add_shortcode('faq', 'tfuse_faq');

function tfuse_faq_question($atts, $content = null)
{
	global $faq_question;
	$faq_question[] = '<div class="faq_question"><span class="dropcap1">Q:</span>'.do_shortcode($content).'</div>';
	//return $out;
}
add_shortcode('faq_question', 'tfuse_faq_question');

function tfuse_faq_answer($atts, $content = null)
{
	global $faq_answer;
	$faq_answer[] = '<div class="faq_answer"><span class="dropcap1">A:</span>'.do_shortcode($content).'</div>';
	//return $out;
}
add_shortcode('faq_answer', 'tfuse_faq_answer');

?>