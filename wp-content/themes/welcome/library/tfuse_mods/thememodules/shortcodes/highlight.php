<?php
//********************HighLight
function tfuse_highlight($atts, $content) {

	//extract short code attr
	extract(shortcode_atts(array('color' => 'yellow'), $atts));
	
	$return_html.= '<div style="background:'.$colors.'">'.strip_tags($content).'</div>';
	
	return $return_html;
}
add_shortcode('highlight', 'tfuse_highlight');

?>