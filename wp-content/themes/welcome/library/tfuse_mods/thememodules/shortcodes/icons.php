<?php
//************************************* Icons
function tfuse_icon_check($atts, $content = null)
{
	$out = '<img src="'.get_bloginfo('template_directory').'/images/icons/icon_check.png" width="23" height="20" alt="check" />';
	return $out;
}
add_shortcode('icon_check', 'tfuse_icon_check');

function tfuse_icon_x($atts, $content = null)
{
	$out = '<img src="'.get_bloginfo('template_directory').'/images/icons/icon_x.png" width="23" height="20" alt="check" />';
	return $out;
}
add_shortcode('icon_x', 'tfuse_icon_x');
?>