<?php
//************************************* Image Frames
function tfuse_frame($atts, $content = null)
{
	extract(shortcode_atts(array('link' => '', 'target' => '_self', 'width' => '', 'height' => '', 'alt' => '', 'title' => '','type'=>'','src' => ''), $atts));
	if($width != '') $width = ' width="'.$width.'"';
	if($height != '') $height = ' height="'.$height.'"';
	if($alt != '') $alt = ' alt="'.$alt.'"';
	if($title != '') $title = ' title="'.$title.'"';
	if($link != '') $out = '<a href="'.$link.'" target="'.$target.'"><img src="'.$src.'" '.$width.$height.$alt.$title.' class="frame_'.$type.'" /></a>';
	else $out = '<img src="'.$src.'" alt="" '.$width.$height.' class="frame_'.$type.'" />';
	return $out;
}
add_shortcode('frame', 'tfuse_frame');

?>
