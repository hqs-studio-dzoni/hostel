<?php
//********************************** LightBox
function tfuse_lightbox($atts, $content = null) 
{
	extract(shortcode_atts(array('title' => '',	'link' => '', 'type' => 'image', 'prettyPhoto' => '','color' => '' ), $atts));
	$class = 'lightbox';
	
	if($type != 'image')
	{
		$class.= '_'.$type;
	}
	
	$return_html = '<a href="'.$link.'" style="color:'.$color.'" rel="prettyPhoto[\''.(($prettyPhoto=='')?'p'.'_'.rand(1,1000):$prettyPhoto).'\']" title="'.$title.'">'.do_shortcode($content).'</a>';
	
	return $return_html;
}
add_shortcode('lightbox', 'tfuse_lightbox');

function tfuse_lightbox_btn($atts, $content = null) 
{
	extract(shortcode_atts(array('title' => '',	'link' => '', 'type' => 'image', 'prettyPhoto' => '', 'color' => '' ), $atts));
	$class = 'lightbox';
	$style = 'light_'.$color;
	if($type != 'image')
	{
		$class.= '_'.$type;
	}
	$return_html = '<a href="'.$link.'" class="button_link '.$style.'" rel="prettyPhoto[\''.(($prettyPhoto=='')?'p'.'_'.rand(1,1000):$prettyPhoto).'\']" title="'.$title.'"><span>'.do_shortcode($content).'</span></a>';
	
	return $return_html;
}
add_shortcode('lightbox_btn', 'tfuse_lightbox_btn');

?>