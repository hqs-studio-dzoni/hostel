<?php
//************************************* Link more
function tfuse_link_more($atts, $content = null)
{
	extract(shortcode_atts(array('url' => '#', 'text' => ''), $atts));
	if($text == '') $text = 'more details';
	$out = '<a class="link-more" href="'.$url.'">'.$text.'</a>';
	return $out;
}
add_shortcode('link_more', 'tfuse_link_more');
?>