<?php
//************************************* List Styles

function tfuse_arrow_list($atts, $content = null)
{
	$content = str_replace('<ul>', '<ul class="list_arrows">', do_shortcode($content));
	return $content;
}
add_shortcode('arrow_list', 'tfuse_arrow_list');

function tfuse_check_list($atts, $content = null)
{
	$content = str_replace('<ul>', '<ul class="check_list">', do_shortcode($content));
	return $content;
}
add_shortcode('check_list', 'tfuse_check_list');

function tfuse_delete_list($atts, $content = null)
{
	$content = str_replace('<ul>', '<ul class="list_delete">', do_shortcode($content));
	return $content;
}
add_shortcode('delete_list', 'tfuse_delete_list');

?>