<?php
function tfuse_map($atts) {

	//extract short code attr
	extract(shortcode_atts(array(
		'width' => 590,
		'height' => 365,
		'lat' => 0,
		'long' => 0,
		'zoom' => 12,
		'type' => '',
		'address' => '',
	), $atts));


$return_html  = '<iframe width="' . $width . '" height="' . $height . '" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" ';
$return_html .= 'src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;geocode=&amp;q=' . $address . '&amp;aq=&amp;ie=UTF8&amp;hq=&amp;hnear=' . $address . '&amp;ll=' . $lat . ',' . $long . '&amp;t=' . $type . '&amp;z=' . $zoom . '&amp;iwloc=A&amp;output=embed">';
$return_html .= '</iframe> ';
return $return_html;

}
add_shortcode('map', 'tfuse_map');
?>