<?php
//************************************* Minigallery
function tfuse_minigallery($attr, $content = null)
{
	global $post;
	if ( isset( $attr['orderby'] ) ) {
		$attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
		if ( !$attr['orderby'] )
			unset( $attr['orderby'] );
	}

	extract(shortcode_atts(array(
		'order'      => 'ASC',
		'orderby'    => 'menu_order ID',
		'id'         => $post->ID,
		'include'    => '',
		'exclude'    => '',
		'style'    => 'box border box_white'
	), $attr));

	if ( !empty($include) ) {
		$include = preg_replace( '/[^0-9,]+/', '', $include );
		$_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

		$attachments = array();
		foreach ( $_attachments as $key => $val ) {
			$attachments[$val->ID] = $_attachments[$key];
		}
	} elseif ( !empty($exclude) ) {
		$exclude = preg_replace( '/[^0-9,]+/', '', $exclude );
		$attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	} else {
		$attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	}

	if ( empty($attachments) )
		return '';

	$out = '[raw]
	<!-- minigallery shortcode -->
	<div class="minigallery-list '.$style.'">
		<a class="prev">prev</a>
		<a class="next">next</a>
		<div class="minigallery">
		<ul>
	';

	foreach ( $attachments as $id => $attachment ) {
		$link = wp_get_attachment_image_src($id,'full',true);
		$image_link_attach = $link[0];
		$imgsrc = wp_get_attachment_image_src($id,array(139,90),false);
		$image_src = $imgsrc[0];
		$out .= '<li><a href="'.$image_link_attach.'" rel="prettyPhoto[gallery1]">'.
		tfuse_get_image(139, 90, 'img',$image_src , '', true, '')
		.'</a></li>';
	}

	$out .= '
		</ul>
		</div>
		<div class="clear"></div>
	</div>
	<!-- minigallery shortcode -->
	[/raw]';
	return $out;
}
add_shortcode('minigallery', 'tfuse_minigallery');
?>