<?php
//************************************* Quotes
function tfuse_quote_right($atts, $content = null)
{
	return '<div class="quote_right"><div class="inner1"><p>' . do_shortcode($content) . '</p></div></div>';
}
add_shortcode('quote_right', 'tfuse_quote_right');

function tfuse_quote_left($atts, $content = null)
{
	return '<div class="quote_left"><div class="inner1"><p>' . do_shortcode($content) . '</p></div></div>';
}
add_shortcode('quote_left', 'tfuse_quote_left');

function tfuse_blockquote($atts, $content = null)
{
	return '<blockquote><div class="inner1">' . do_shortcode($content) . '</div></blockquote>';
}
add_shortcode('blockquote', 'tfuse_blockquote');
?>