<?php
//************************************* Rows
function tfuse_row_box($atts, $content = null)
{
	extract(shortcode_atts(array('style' => ''), $atts));
	return '<div class="box '.$style.'">' . do_shortcode($content) . '<div class="clear"></div></div>';
}
add_shortcode('row_box', 'tfuse_row_box');

function tfuse_row($atts, $content = null)
{
	return '<div class="row">' . do_shortcode($content) . '</div>';
}
add_shortcode('row', 'tfuse_row');
?>