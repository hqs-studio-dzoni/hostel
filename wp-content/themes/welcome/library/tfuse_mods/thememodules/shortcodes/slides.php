<?php
//********************Slide Show
function tfuse_slideshow($atts, $content) {

	//extract short code attr
	extract(shortcode_atts(array(
		'width' => 400,
		'height' => 300,
	), $atts));
	
	$content = trim($content);
	$image_arr = preg_split("/(\r?\n)/", $content);

	$return_html = '<div class="slideshow" style="width:'.$width.'px;height:'.$height.'px"><div class="wrapper" style="width:'.$width.'px;height:'.intval($height+25).'px"><ul>';

	if(!empty($image_arr) && is_array($image_arr))
	{
		foreach($image_arr as $image)
		{
			$image = trim(strip_tags($image));
			
			if(!empty($image))
			{
				$return_html.= '<li>';
				$return_html.= tfuse_get_image($width, $height, 'img', $image, '', true, '');
				$return_html.= '</li>'. PHP_EOL;
			}
		}
	}
	
	$return_html.= '</ul></div></div><br class="clear"/><br class="clear"/>';
	
	return $return_html;
}
add_shortcode('slideshow', 'tfuse_slideshow');

//*************************** Nivo Slide
function tfuse_nivoslide($atts, $content) {

	//extract short code attr
	extract(shortcode_atts(array(
		'width' => 400,
		'height' => 300,
		'effect' => 'sliceDown',
		'pauseTime' => 5,
	), $atts));
	
	$content = trim($content);
	$image_arr = preg_split("/(\r?\n)/", $content);

	$rand_id = mt_rand();
	$return_html = '<div class="nivo_border" style="width:'.$width.'px;height:'.$height.'px;"><div id="'.$rand_id.'" class="nivoslide" style="width:'.$width.'px;height:'.$height.'px; visibility: hidden">';

	if(!empty($image_arr) && is_array($image_arr))
	{
		foreach($image_arr as $image)
		{
			$image = trim(strip_tags($image));
			
			if(!empty($image))
			{
				$return_html.= tfuse_get_image($width, $height, 'img', $image, '', true, '');
			}
		}
	}
	
	$return_html.= '</div></div><br class="clear"/>';
	
	$return_html.= "<script>jQuery(window).load(function() { jQuery('#".$rand_id."').nivoSlider({ pauseTime: ".intval($pauseTime*1000).", pauseOnHover: true, effect: '".$effect."', controlNav: true, captionOpacity: 1, directionNavHide: false, controlNavThumbs: true, controlNavThumbsFromRel:false, afterLoad: function(){ 
		jQuery('#".$rand_id."').css('visibility', 'visible');
	} }); });</script>";
	
	return $return_html;
}
add_shortcode('nivoslide', 'tfuse_nivoslide');

?>