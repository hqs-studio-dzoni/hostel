<?php
//**************************Style Box
function tfuse_styled_box($atts, $content) {

	//extract short code attr
	extract(shortcode_atts(array(
		'title' => '',
		'style' => '',
		'color' => '',
	), $atts));
	
	switch(strtolower($color))
		{
			case 'black':
				$bg_color = '#000000';
				$text_color = '#ffffff';
			break;
			default:
			case 'gray':
				$bg_color = '#666666';
				$text_color = '#ffffff';
			break;
			case 'white':
				$bg_color = '#f5f5f5';
				$text_color = '#444444';
			break;
			case 'blue':
				$bg_color = '#004a80';
				$text_color = '#ffffff';
			break;
			case 'yellow':
				$bg_color = '#f9b601';
				$text_color = '#ffffff';
			break;
			case 'red':
				$bg_color = '#9e0b0f';
				$text_color = '#ffffff';
			break;
			case 'orange':
				$bg_color = '#fe7201';
				$text_color = '#ffffff';
			break;
			case 'green':
				$bg_color = '#7aad34';
				$text_color = '#ffffff';
			break;
			case 'pink':
				$bg_color = '#d2027d';
				$text_color = '#ffffff';
			break;
			case 'purple':
				$bg_color = '#582280';
				$text_color = '#ffffff';
			break;
		}
	
	
	$return_html = '<div class="styled_box_title" style="background: -webkit-gradient(linear, left top, left bottom, from('.$bg_color.'), to('.$bg_color.'));background: -moz-linear-gradient(top,  '.$bg_color.',  '.$bg_color.');filter:  progid:DXImageTransform.Microsoft.gradient(startColorstr=\''.$bg_color.'\', endColorstr=\''.$bg_color.'\');border:1px solid '.$bg_color.';color:'.$text_color.';width:'.$width.';'.$style.'">'.$title.'</div>';
	$return_html.= '<div class="styled_box_content" style="border:1px solid '.$bg_color.';border-top:0;width:'.$width.'">'.html_entity_decode(do_shortcode($content)).'</div>';
	
	return $return_html;
}
add_shortcode('styled_box', 'tfuse_styled_box');

?>