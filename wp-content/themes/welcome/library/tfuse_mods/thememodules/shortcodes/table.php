<?php
//********************* tables shortcodes
function tfuse_table($atts, $content) {

//extract short code attr
	extract(shortcode_atts(array('color' => '',), $atts));
	$return_html= '<div class="table_'.strtolower($color).'">';
	$return_html.= html_entity_decode(do_shortcode($content));
	$return_html.= '</div>';	
	return $return_html;
}
add_shortcode('table', 'tfuse_table');

?>