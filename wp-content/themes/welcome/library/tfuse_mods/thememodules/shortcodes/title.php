<?php
//************************************* H Titles
function tfuse_title($atts, $content = null)
{
	extract(shortcode_atts(array(
		'h1' => '',
		'h2' => '',
		'h3' => '',
		'h4' => '',
		'h5' => '',
		'h6' => '',
	), $atts));
	$h = 'h2'; $csstyle = '';
	if(!is_array($atts)) $atts = array();
	foreach($atts as $key => $value) if($key != '') { $h = $key; $csstyle = 'class="'.$value.'"'; break; }
	$patterns[0] = '/border/';
	//$patterns[1] = '/blue/';
	//$patterns[2] = '/dark_blue/';
	$replacements[0] = 'title_border';
	//$replacements[1] = 'title_blue';
	//$replacements[2] = 'title_dark_blue';
	$csstyle = preg_replace($patterns,$replacements,$csstyle);
	return '<'.$h.' '.$csstyle.'>'.do_shortcode($content).'</'.$h.'>';
}
add_shortcode('title', 'tfuse_title');

?>