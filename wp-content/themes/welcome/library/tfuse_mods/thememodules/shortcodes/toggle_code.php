<?php
//************************************* Toggle Content
function tfuse_toggle_content($atts, $content = null)
{
	extract(shortcode_atts(array('title' => ''), $atts));
	$out = '<h3 class="toggle box">' . $title . '</h3>';
	$out .= '<div class="toggle_content">';
	$out .= do_shortcode($content);
	$out .= '</div>';
	return $out;
}
add_shortcode('toggle_content', 'tfuse_toggle_content');

function tfuse_toggle_code($atts, $content = null)
{
	extract(shortcode_atts(array('title' => '', 'brush' => 'plain'), $atts));
	$out = '<h3 class="toggle box">' . $title . '</h3>';
	$out .= '<div class="highlighter">';
	$out .= '<pre class="brush: '.$brush.'">';
	$out .= $content;
	$out .= '</pre>';
	$out .= '</div>';
	return $out;
}
add_shortcode('toggle_code', 'tfuse_toggle_code');

function tfuse_code($atts, $content = null)
{
	extract(shortcode_atts(array('brush' => 'plain'), $atts));
	$out = '<pre class="brush: '.$brush.'">';
	$out .= $content;
	$out .= '</pre>';
	return $out;
}
add_shortcode('code', 'tfuse_code');
?>