<?php
function tfuse_html5video($atts) {
	//extract short code attr
	extract(shortcode_atts(array(
		'width' => 640,
		'height' => 385,
		'poster' => '',
		'mp4' => '',
		'webm' => '',
		'ogg' => '',
	), $atts));
	
	$custom_id = time().rand();
	
	$return_html = '<div class="video-js-box vim-css"> 
    <video id="example_video_1" class="video-js" width="'.$width.'" height="'.$height.'" controls="controls" preload="auto" poster="'.$poster.'"> 
      <source src="'.$mp4.'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\' /> 
      <source src="'.$webm.'" type=\'video/webm; codecs="vp8, vorbis"\' /> 
      <source src="'.$ogg.'" type=\'video/ogg; codecs="theora, vorbis"\' /> 
      <object id="flash_fallback_1" class="vjs-flash-fallback" width="640" height="264" type="application/x-shockwave-flash"
        data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf"> 
        <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" /> 
        <param name="allowfullscreen" value="true" /> 
        <param name="flashvars" value=\'config={"playlist":["'.$poster.'", {"url": "'.$mp4.'","autoPlay":false,"autoBuffering":true}]}\' /> 
        <img src="'.$poster.'" width="640" height="264" alt="Poster Image"
          title="No video playback capabilities." /> 
      </object> 
    </video> 
  </div> ';
	
	return $return_html;
}
add_shortcode('html5video', 'tfuse_html5video');

function tfuse_youtube($atts, $content = null) 
{

extract(shortcode_atts(array('link' => '','width' => '660', 'height' => '400'	), $atts));
	
	$video_id = substr($link,31,11);
	$return_html = '<div>';
	$return_html.= '<object type="application/x-shockwave-flash" data="http://www.youtube.com/v/'.$video_id.'&hd=1" style="width:'.$width.'px;height:'.$height.'px"><param name="wmode" value="opaque"><param name="movie" value="http://www.youtube.com/v/'.$video_id.'&hd=1" /></object>';
	$return_html.= '</div>'. PHP_EOL;
	return $return_html;
}
add_shortcode('youtube', 'tfuse_youtube');

function tfuse_vimeo($atts, $content) {

	//extract short code attr
	extract(shortcode_atts(array(
		'width' => 640,
		'height' => 385,
		'link' => '',
	), $atts));
	
	$custom_id = time().rand();
	$video_id = substr($link,17,8);
	$return_html = '<object width="'.$width.'" height="'.$height.'"><param name="allowfullscreen" value="true" /><param name="wmode" value="opaque"><param name="allowscriptaccess" value="always" /><param name="movie" value="http://vimeo.com/moogaloop.swf?clip_id='.$video_id.'&amp;server=vimeo.com&amp;show_title=0&amp;show_byline=0&amp;show_portrait=0&amp;color=00ADEF&amp;fullscreen=1" /><embed src="http://vimeo.com/moogaloop.swf?clip_id='.$video_id.'&amp;server=vimeo.com&amp;show_title=0&amp;show_byline=0&amp;show_portrait=0&amp;color=00ADEF&amp;fullscreen=1" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="'.$width.'" height="'.$height.'" wmode="transparent"></embed></object>';
	
	return $return_html;
}
add_shortcode('vimeo', 'tfuse_vimeo');
?>