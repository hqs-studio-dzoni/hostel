<?php
class TF_Widget_Text extends WP_Widget {

	function TF_Widget_Text() {
		$widget_ops = array('classname' => 'widget_text', 'description' => __('Arbitrary text or HTML'));
		$control_ops = array('width' => 400, 'height' => 350);
		$this->WP_Widget('text', __('TFuse Text'), $widget_ops, $control_ops);
	}

	function widget( $args, $instance ) {
		extract($args);
		$title = apply_filters( 'widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);
		$text = apply_filters( 'widget_text', $instance['text'], $instance );
		$template = empty( $instance['template'] ) ? 'box_black' : $instance['template'];
		//if (is_home()) $template = '';
		
		$before_widget = '<div  class="box '.$template.' widget_text"><div class="inner">';
		$after_widget = '</div></div>';
		$before_title = '<h3 class=bordered>';
		$after_title = '</h3>';
		
		
		echo $before_widget;
		if ( !empty( $title ) ) { echo $before_title . html_entity_decode($title) . $after_title; } ?>
			<div class="textwidget"><?php echo $instance['filter'] ? wpautop($text) : $text; ?></div>
		<?php
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = $new_instance['title'];
		if ( current_user_can('unfiltered_html') )
			$instance['text'] =  $new_instance['text'];
		else
			$instance['text'] = stripslashes( wp_filter_post_kses( addslashes($new_instance['text']) ) ); // wp_filter_post_kses() expects slashed
		$instance['filter'] = isset($new_instance['filter']);
		
		if ( in_array( $new_instance['template'], array( 'box_black', 'box_gray' ) ) ) {
			$instance['template'] = $new_instance['template'];
		} else {
			$instance['template'] = 'box_black';
		}		
		
		return $instance;
	}

	function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'text' => '' ) );
		$title = $instance['title'];
		$text = format_to_edit($instance['text']);
		$template = esc_attr( $instance['template'] );		
?>
		<p>
			<label for="<?php echo $this->get_field_id('template'); ?>"><?php _e( 'Template:' ); ?></label>
			<select name="<?php echo $this->get_field_name('template'); ?>" id="<?php echo $this->get_field_id('template'); ?>" class="widefat">
				<option value="box_black"<?php selected( $instance['template'], 'box_black' ); ?>><?php _e('Black'); ?></option>
				<option value="box_gray"<?php selected( $instance['template'], 'box_gray' ); ?>><?php _e('Gray'); ?></option>
			</select>
		</p>

		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>

		<textarea class="widefat" rows="16" cols="20" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>"><?php echo $text; ?></textarea>

		<p><input id="<?php echo $this->get_field_id('filter'); ?>" name="<?php echo $this->get_field_name('filter'); ?>" type="checkbox" <?php checked(isset($instance['filter']) ? $instance['filter'] : 0); ?> />&nbsp;<label for="<?php echo $this->get_field_id('filter'); ?>"><?php _e('Automatically add paragraphs'); ?></label></p>

<?php
	}
}


function TFuse_Unregister_WP_Widget_Text() {
	unregister_widget('WP_Widget_Text');       
}
add_action('widgets_init','TFuse_Unregister_WP_Widget_Text');

register_widget('TF_Widget_Text');
?>