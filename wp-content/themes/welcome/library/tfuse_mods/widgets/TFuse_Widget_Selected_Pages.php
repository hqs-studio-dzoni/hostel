<?php
class TFuse_Widget_Selected_Pages extends WP_Widget {

	function TFuse_Widget_Selected_Pages() {
		$widget_ops = array('description' => __( 'Show Selected Pages', 'tfuse') );
		parent::WP_Widget(false, __('TFuse Selected Pages', 'tfuse'),$widget_ops);      
	}

	function widget($args, $instance) {  
		extract( $args );
		$title = esc_attr($instance['title']);
		$subtitle = esc_attr($instance['subtitle']);
		$pages = $instance['pages'];
		$template = empty( $instance['template'] ) ? 'box_black' : $instance['template'];
		
		$before_widget = '<div  class="box '.$template.' widget_selected_pages"><div class="inner">';
		$after_widget = '</div></div>';
		$before_title = '<h3 class=bordered>';
		$after_title = '</h3>';
		
		echo $before_widget;
		if ( $title )
			echo $before_title . html_entity_decode($title) . $after_title;
		?>

            	<?php 
  				if ( is_array($pages) ) { ?>
                
                        <ul>
					
							<?php 
							$k=0;							
							foreach ($pages as $key=>$val) {
                                
								$k++;
								if ($k==1)             $first = '  class="first" '; else $first = '';
								if ($k==count($pages)) $last  = '  class="last" ';  else $last  = '';
								
								$page = get_post($key);    
								echo '<li '.$first.$last.'><a href="' . get_page_link($key) . '">' . $page->post_title . '</a></li>';								
								$page = get_post($key);    
                            } ?>
                            
                        </ul>
                <?php 
				}
				?>
	   <?php			
	   echo $after_widget;
   }

   function update($new_instance, $old_instance) {                
       return $new_instance;
   }

   function form($instance) {        
		$title = esc_attr($instance['title']);
		$subtitle = esc_attr($instance['subtitle']);
		$pages = esc_attr($instance['pages']);
		$template = esc_attr( $instance['template'] );
 		?>
 		<p>
			<label for="<?php echo $this->get_field_id('template'); ?>"><?php _e( 'Template:' ); ?></label>
			<select name="<?php echo $this->get_field_name('template'); ?>" id="<?php echo $this->get_field_id('template'); ?>" class="widefat">
				<option value="box_black"<?php selected( $instance['template'], 'box_black' ); ?>><?php _e('Black'); ?></option>
				<option value="box_gray"<?php selected( $instance['template'], 'box_gray' ); ?>><?php _e('Gray'); ?></option>
			</select>
		</p>
 		
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:','tfuse'); ?></label>
            <input type="text" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $title; ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('pages'); ?>"><?php _e('Select Pages List','tfuse'); ?></label>
            <?php 
				$tfuse_pages = array();  
				$tfuse_pages_obj = get_pages();
				if (is_array($tfuse_pages_obj)) {
					foreach ($tfuse_pages_obj as $tfuse_page) { ?>
                    	<br /><br />
                        <?php 
                        if ( esc_attr($instance['pages'][$tfuse_page->ID]) ) $checked = ' checked="checked" '; else $checked = '';
						?>
                        
						<input <?php echo $checked; ?> type="checkbox" name="<?php echo $this->get_field_name('pages'); ?>[<?php echo $tfuse_page->ID;?>]" value="1" id="<?php echo $this->get_field_id('pages'); ?>" />&nbsp;&nbsp;<?php echo $tfuse_page->post_title; ?>
                        <?php
 					}
				}
			?>            
        </p>
		<?php
	}
} 
register_widget('TFuse_Widget_Selected_Pages');
 
?>