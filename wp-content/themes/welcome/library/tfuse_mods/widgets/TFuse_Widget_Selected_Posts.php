<?php
class TFuse_Widget_Selected_Posts extends WP_Widget {

	function TFuse_Widget_Selected_Posts() {
		$widget_ops = array('description' => __( 'Show Selected Posts', 'tfuse') );
		parent::WP_Widget(false, __('TFuse Selected Posts', 'tfuse'),$widget_ops);      
	}

	function widget($args, $instance) {  
		extract( $args );
		(isset($instance['title'])) ? $title = esc_attr($instance['title']) : $title = '';
		(isset($instance['subtitle'])) ? $subtitle = esc_attr($instance['subtitle']) : $subtitle = '';
		(isset($instance['posts'])) ? $posts = $instance['posts'] : $posts = array();
		$template = empty( $instance['template'] ) ? 'box_black' : $instance['template'];
		
		$before_widget = '<div  class="box '.$template.' widget_recent_entries">';
		$after_widget = '</div>';
		$before_title = '<h3>';
		$after_title = '</h3>';
		
		echo $before_widget;
		if ( $title )
			echo $before_title . html_entity_decode($title) . $after_title;
		?>

            	<?php 
  				if ( is_array($posts) ) { ?>
                        <ul>
							<?php 
							$k=0;							
							foreach ($posts as $key=>$val) {
                                
								$k++;
								if ($k==1)             $first = '  class="first" '; else $first = '';
								if ($k==count($posts)) $last  = '  class="last" ';  else $last  = '';
								
								$page = get_post($key); 
								
								$large_image = get_post_meta($key, PREFIX . "_post_image", true);
								$medium_image = get_post_meta($key, PREFIX . "_post_image_medium", true);
								$small_image = get_post_meta($key, PREFIX . "_post_image_small", true);
								$src_image = '';
								
								if($medium_image != '')
									$src_image = $medium_image;
								elseif($large_image != '')
									$src_image = $large_image;
								elseif($small_image != '')
									$src_image = $small_image;
									
								$dateArr = explode(" ", $page->post_date_gmt);
								
								  
								echo '<li '.$first.$last.'>
									<a href="' . get_permalink($key) . '">'; tfuse_get_image(50, 50, 'img', $src_image, '', false, 'thumbnail'); echo '</a>
									<a href="' . get_permalink($key) . '">' . $page->post_title . '</a> 
									<div class="date">' . $dateArr[0] . '</div> 
								</li>';
                            } ?>
                        </ul>
                <?php 
				}
				?>
	   <?php			
	   echo $after_widget;
   }

   function update($new_instance, $old_instance) {                
       return $new_instance;
   }

   function form($instance) {        
		(isset($instance['title'])) ? $title = esc_attr($instance['title']) : $title = '';
		(isset($instance['subtitle'])) ? $subtitle = esc_attr($instance['subtitle']) : $subtitle = '';
		(isset($instance['posts'])) ? $posts = $instance['posts'] : $posts = array();
		$template = esc_attr( $instance['template'] );
 		?>
		<p>
			<label for="<?php echo $this->get_field_id('template'); ?>"><?php _e( 'Template:' ); ?></label>
			<select name="<?php echo $this->get_field_name('template'); ?>" id="<?php echo $this->get_field_id('template'); ?>" class="widefat">
				<option value="box_black"<?php selected( $instance['template'], 'box_black' ); ?>><?php _e('Black'); ?></option>
				<option value="box_gray"<?php selected( $instance['template'], 'box_gray' ); ?>><?php _e('Gray'); ?></option>
			</select>
		</p>
 		
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:','tfuse'); ?></label>
            <input type="text" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $title; ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('posts'); ?>"><?php _e('Select Posts List','tfuse'); ?></label>
            <?php 
				$tfuse_posts = array();  
				$tfuse_posts_obj = get_posts('numberposts=-1');
				if (is_array($tfuse_posts_obj)) {
					foreach ($tfuse_posts_obj as $tfuse_post) { ?>
                    	<br /><br />
                        <?php 
                        if ( isset($instance['posts'][$tfuse_post->ID]) ) $checked = ' checked="checked" '; else $checked = '';
						?>
                        
						<input <?php echo $checked; ?> type="checkbox" name="<?php echo $this->get_field_name('posts'); ?>[<?php echo $tfuse_post->ID;?>]" value="1" id="<?php echo $this->get_field_id('posts'); ?>" />&nbsp;&nbsp;<?php echo $tfuse_post->post_title; ?>
                        <?php
 					}
				}
			?>            
        </p>
		<?php
	}
} 
register_widget('TFuse_Widget_Selected_Posts');
 
?>