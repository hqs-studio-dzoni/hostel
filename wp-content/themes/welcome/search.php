<?php 
get_header(); 
	
	$cat_ID = get_query_var('cat'); 
	$disable_header = get_option(PREFIX . '_category_disable_header_welcome_bar_' . $cat_ID); 
?>

 
<div class="middle">
<div class="container padding50px">

	<?php 
		if ( get_option(PREFIX . '_category_sidebar_position_' . $cat_ID)==1 ) { $content = 'suffix_1'; $sidebar = ''; }
	elseif ( get_option(PREFIX . '_category_sidebar_position_' . $cat_ID)==2 ) { $content = ''; $sidebar = 'suffix_1'; }
	elseif ( get_option(PREFIX.'_sidebar_position')==1 ) { $content = ''; $sidebar = 'suffix_1'; }
	else { $content = 'suffix_1'; $sidebar = ''; }
	?>

 	<!-- middle content -->
	<div class="container_24">
	
		<?php if ( $sidebar!=='' ) { ?>
			<!-- sidebar -->
			<div class="grid_6 <?php echo $sidebar; ?>">
				<?php get_sidebar(); ?>
			</div>
			<!--/ sidebar -->
		<?php } ?>
		
    	<div class="grid_17 <?php echo $content; ?>">
		  <div class="text">
			<?php if(have_posts()) : $count = 0; ?>
				<?php
					//top area for shortcodes
					$category_content_top = get_option(PREFIX . '_category_content_top_' . $cat_ID);
					if($category_content_top)
					{
						$category_content_top = html_entity_decode($category_content_top ,ENT_QUOTES, 'UTF-8');
						echo $category_content_top = apply_filters('themefuse_shortcodes',$category_content_top);
						echo '<div class="divider_space"></div>';
					}
				?>
			<?php while(have_posts()) : the_post(); $count++; ?>
			
			    <div class="news-item">
	                <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
					<div class="entry">
						<?php the_excerpt(); ?>
       				</div>
                    <div class="news-meta"><a href="<?php the_permalink() ?>" class="link-more alignleft">Find out more</a> 
					<?php if(get_post_meta($post->ID, PREFIX . "_post_single_comments", true) != 'true') { ?>
					<a href="<?php comments_link(); ?>"><?php comments_number('0 comments', '1 comment', '% comments') ?> </a></div>
					<?php } ?>
              	</div>

			<?php endwhile; ?>
				<?php
					//bottom area for shortcodes
					$category_content_bottom = get_option(PREFIX . '_category_content_bottom_' . $cat_ID);
					if($category_content_bottom)
					{
						$category_content_bottom = html_entity_decode($category_content_bottom ,ENT_QUOTES, 'UTF-8');
						echo $category_content_bottom = apply_filters('themefuse_shortcodes',$category_content_bottom);
						echo '<div class="divider_space"></div>';
					}
				?>
					<?php tfuse_pagination('cat=' . $cat_ID); ?>
			<?php else: ?>
				<h5><?php _e('Sorry, no posts matched your criteria.', 'tfuse') ?></h5>
			<?php endif; ?>
		   </div>
		</div>
		
		<?php if ( $content!='' ) { ?>
			<!-- sidebar -->
			<div class="grid_6 <?php echo $sidebar; ?>">
				<?php get_sidebar(); ?>
			</div>
			<!--/ sidebar -->
		<?php } ?>
	
		<div class="clear"></div>
	</div>
	<!--/ middle content -->
    
<?php get_footer(); ?>