<?php
if ( get_post_meta($post->ID, PREFIX.'_post_enable_slider', true)=='true' ) $slider = true; else $slider = false;
$template_directory = get_bloginfo('template_directory');
get_header();

	$disable_header = get_post_meta($post->ID, PREFIX . "_post_disable_header_welcome_bar", true);
	$disable_slider = get_post_meta($post->ID, PREFIX . "_post_disable_slider", true);
?>

<?php if ( $disable_slider == 'false' || $disable_header == 'false' ) { ?> 
	<div class="header">
	<!-- header slider -->
	<?php if ($disable_slider == 'false') { ?>
	
		<div class="container">
		
			<?php
			if(tfuse_meta_exist(PREFIX . '_post_slider_data_sliderdata_1', $post->ID))
			{
			?>
			<div id="slides">
				<div class="slides_container">
					<?php $k = 1; while(tfuse_meta_exist(PREFIX . '_post_slider_data_sliderdata_' . $k, $post->ID))
					{
						$slideArr = get_post_meta($post->ID, PREFIX . '_post_slider_data_sliderdata_' . $k, true);
						$k++;
						$slideArr["slider_post_title"] = tfuse_qtranslate($slideArr["slider_post_title"]);
						if($slideArr["slider_post_link"] != '')
						{
						?>
						<div>
							<a href="<?php echo $slideArr["slider_post_link"]; ?>" target="<?php echo $slideArr["slider_post_target"]; ?>"><?php tfuse_get_image(960, 379, 'img', $slideArr['img'], '', false, 'slider-img') ?></a>
							<?php if ( $slideArr["slider_post_title"]!='' ) { ?><div class="caption"><p><?php echo $slideArr["slider_post_title"]; ?></p></div><?php } ?>
						</div>
						<?php
						}
						else
						{ ?>
						<div>
							<?php tfuse_get_image(960, 379, 'img', $slideArr['img'], '', false, 'slider-image'); ?>
							<?php if ( $slideArr["slider_post_title"]!='' ) { ?><div class="caption"><p><?php echo $slideArr["slider_post_title"]; ?></p></div><?php } ?>
						</div>
						<?php
						}
					}
					?>
				</div>
				<a href="#" class="prev">Previous</a>
				<a href="#" class="next">Next</a>		
			</div>
			<?php
			} ?>
			
		</div>   
		<!--/ header slider -->    
			
	<?php } ?>
	</div>
	<div class="header-line"></div>
<?php } ?>

<div class="middle">
<div class="container <?php if ($disable_header == 'true') { ?>padding50px<?php } ?>">

	<?php 
		if ( get_post_meta($post->ID, PREFIX.'_post_sidebar_position', true)==1 ) { $content = 'suffix_1'; $sidebar = ''; }
	elseif ( get_post_meta($post->ID, PREFIX.'_post_sidebar_position', true)==2 ) { $content = ''; $sidebar = 'suffix_1'; }
	elseif ( get_post_meta($post->ID, PREFIX.'_post_sidebar_position', true)==3 ) { $content = ''; $sidebar = ''; }
	elseif ( get_option(PREFIX.'_sidebar_position')==1 ) { $content = ''; $sidebar = 'suffix_1'; }
	else { $content = 'suffix_1'; $sidebar = ''; }
	?>

	<?php if ($disable_header == 'false') {  

		if ( get_post_meta($post->ID, PREFIX . "_post_welcome_bar_source", true) == 'header1' ) {
		
			$post_image 		= get_post_meta($post->ID, PREFIX . "_post_header_image", true);
			$post_image_url 	= get_post_meta($post->ID, PREFIX . "_post_header_image_url", true);
			$post_image_target 	= get_post_meta($post->ID, PREFIX . "_post_header_image_url_target", true);
			$post_title 		= tfuse_qtranslate( get_post_meta($post->ID, PREFIX . "_post_header_title", true) );
			?>
			<?php if ($disable_header == 'false') { ?>
			<div class="header-title-image">
				<?php if ( $post_image!='' && $post_image_url!='' ) { ?><div class="image"><a href="<?php echo $post_image_url; ?>" target="<?php echo $post_image_target; ?>"><?php tfuse_get_image(708, 124, 'img', $post_image, '', false, 'slider-image'); ?></a></div><?php } 
				  elseif ( $post_image!='' ) { ?><div class="image"><?php tfuse_get_image(708, 124, 'img', $post_image, '', false, 'slider-image'); ?></div><?php } ?>
				<?php if ( $post_title!='' ) { ?><h1><?php echo $post_title ?></h1><?php } ?>
			</div>
			<?php } 
			
		} else {  
		
			if(tfuse_meta_exist(PREFIX . '_post_images_data_sliderdata_1', $post->ID))
			{
			?>
			<!-- baners top -->
			<div class="baners_top">
				<?php $k = 1; while(tfuse_meta_exist(PREFIX . '_post_images_data_sliderdata_' . $k, $post->ID))
				{
					$slideArr = get_post_meta($post->ID, PREFIX . '_post_images_data_sliderdata_' . $k, true);
					$k++;
					$slideArr["image_post_title"] = tfuse_qtranslate($slideArr["image_post_title"]);
					if($slideArr["image_post_link"] != '')
					{
					?>
					<div class="baner-item">
						<div class="baner-img"><a href="<?php echo $slideArr["image_post_link"]; ?>" target="<?php echo $slideArr["image_post_target"]; ?>"><?php tfuse_get_image(230, 73, 'img', $slideArr['img'], '', false, 'slider-img') ?></a></div>
						<h2><?php echo $slideArr["image_post_title"] ?></h2>
					</div>
					<?php
					}
					else
					{ ?>
					<div class="baner-item">
						<div class="baner-img"><?php tfuse_get_image(230, 73, 'img', $slideArr['img'], '', false, 'slider-img') ?></div>
						<h2><?php echo $slideArr["image_post_title"] ?></h2>
					</div>
					<?php
					}
				}
				?>
			</div>    
			<!--/ baners top -->
			<?php } ?>
		
		<?php 
		}
	}
	?>
	
	<!-- middle content -->
	<div class="container_24">
	
		<?php if ( $sidebar!=='' && get_post_meta($post->ID, PREFIX.'_post_sidebar_position', true) < 3 ) { ?>
			<!-- sidebar -->
			<div class="grid_6 <?php echo $sidebar; ?>">
				<?php get_sidebar(); ?>
			</div>
			<!--/ sidebar -->
		<?php } ?>

		<?php if (have_posts()) : $count = 0; ?>
		<?php while (have_posts()) : the_post(); $count++; ?>	
			
			<?php if ( get_post_meta($post->ID, PREFIX.'_post_sidebar_position', true) < 3) { ?>
			<!-- content -->
			<div class="grid_17 <?php echo $content; ?>">
			<?php } ?>
			  <div class="text">
			  
					<?php
					$post_video = get_post_meta($post->ID, PREFIX . "_post_video", true);
					$large_image = get_post_meta($post->ID, PREFIX . "_post_image", true);
					$medium_image = get_post_meta($post->ID, PREFIX . "_post_image_medium", true);
					$small_image = get_post_meta($post->ID, PREFIX . "_post_image_small", true);
					$disablevideo = get_post_meta($post->ID, PREFIX . "_post_single_video", true);
					$disableimage = get_post_meta($post->ID, PREFIX . "_post_single_image", true);
					$disableprety = get_option(PREFIX . "_disable_lightbox");
					$src_image = '';

					if($post_video != '' && $disablevideo == 'true')
						$media = $post_video;
					elseif($large_image != '')
						$media = $large_image;
					elseif($medium_image != '')
						$media = $medium_image;
					else
						$media = $small_image;

					if($medium_image != '')
						$src_image = $medium_image;
					elseif($large_image != '')
						$src_image = $large_image;
					elseif($small_image != '')
						$src_image = $small_image;

					if($disablevideo != 'true' && $post_video != '')
					{
						echo '<div class="video_embed">';
						if(get_post_meta($post->ID, PREFIX.'_post_sidebar_position', true) == 3)
							echo tfuse_get_embed(960, 555, PREFIX . "_post_video");
						else
							echo tfuse_get_embed(670, 380, PREFIX . "_post_video");
						echo "</div><br />";
					}

					if($src_image != '')
					{
						//if no sidebar
						if(get_post_meta($post->ID, PREFIX.'_post_sidebar_position', true) == 3)
						{
							$img_width = 960;
							$img_height = 295;
						}
						else
						{
							$img_width = 670;
							$img_height = 240;
						}

						$img_in = '<img src="' . tfuse_get_image($img_width, $img_height, 'src', $src_image, '', true) . '" alt="' . get_the_title() . '" class="blog_img" width="'.$img_width.'" height="'.$img_height.'" />';

						if($img_in != '' && $disableimage != 'true')
						{
							if($disableprety != 'true')
							{
							?>
								<a href="<?php echo $media; ?>" rel="prettyPhoto[gallery]"><?php echo $img_in ?></a>
								<br /><br />
							<?php
							}
							else
							{
								echo $img_in;
								echo "<br /><br />";
							}
						}
						?>

						<div style="display:none;" class="gallery-image">
						<?php
						//get image from medial ibrary
						$attachments = get_children( array(
								'post_parent' => $post->ID,
								'numberposts' => -1,
								'post_type' => 'attachment',
								'post_mime_type' => 'image')
								);

						if($post_video != '' && $disablevideo == 'true')
						{
							if($large_image != '') $media = $large_image; elseif($medium_image != '') $media = $medium_image; else $media = $small_image;
						?>
							<a href="<?php echo $media; ?>" rel="prettyPhoto[gallery]"><?php echo $img_in ?></a>
						<?php
						}

						if(!empty( $attachments ))
						{
							$size = 'full';
							foreach ($attachments as $att_id => $attachment)
							{
								$src = wp_get_attachment_image_src($att_id, $size, true);
								$image_link_attach = $src[0];
							?>
								<a href="<?php echo $image_link_attach; ?>" rel="prettyPhoto[gallery]"><?php echo wp_get_attachment_image($att_id, $size); ?></a>
							<?php
							}
						}
						//end attachement
						?>
						</div>

					<?php } ?>
			  
 
 					<?php the_content(); ?>
					&nbsp
					<?php if ( get_post_meta($post->ID, PREFIX."_post_single_comments", true)=='false' ) { comments_template(); } ?>
			  </div>
			<?php if ( get_post_meta($post->ID, PREFIX.'_post_sidebar_position', true) < 3) { ?>
			</div>
			<!--/ content -->
			<?php } ?>

		<?php endwhile; else: ?>
	
			<?php if ( get_post_meta($post->ID, PREFIX.'_post_sidebar_position', true) < 3) { ?>
			<!-- content -->
			<div class="grid_17 <?php echo $content; ?>">
			<?php } ?>
			  <div class="text">
					<?php _e('Sorry, no posts matched your criteria.', 'tfuse') ?>
			  </div>
			</div>
			<!--/ content -->
			
		<?php endif; ?>
		
		<?php if ( $content!='' && get_post_meta($post->ID, PREFIX.'_post_sidebar_position', true) < 3) { ?>
			<!-- sidebar -->
			<div class="grid_6 <?php echo $sidebar; ?>">
				<?php get_sidebar(); ?>
			</div>
			<!--/ sidebar -->
		<?php } ?>
	
		<div class="clear"></div>
	</div>
	<!--/ middle content -->
    
<?php get_footer(); ?>