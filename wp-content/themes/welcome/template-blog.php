<?php 
/*
Template Name: Blog Page
*/
if ( get_post_meta($post->ID, PREFIX.'_page_enable_slider', true)=='true' ) $slider = true; else $slider = false;
$template_directory = get_bloginfo('template_directory');
get_header();
	$cat_ID = get_query_var('cat');
	$disable_header = get_post_meta($post->ID, PREFIX . "_page_disable_header_welcome_bar", true);
	$disable_slider = get_post_meta($post->ID, PREFIX . "_page_disable_slider", true);
	if(tfuse_meta_exist(PREFIX . '_page_slider_data_sliderdata_1', $post->ID)) $disable_slider == 'false'; else $disable_slider = 'true';
	
	$cat = get_post_meta($post->ID, PREFIX."_blog_page_cat", true);
	
?>

<?php if ( $disable_slider == 'false' || $disable_header == 'false' ) { ?> 
	<div class="header <?php if ($disable_slider == 'false') { echo 'homepage'; } ?>">
	<!-- header slider -->
	<?php if ($disable_slider == 'false') { ?>
	
		<div class="container">
		
			<?php
			if(tfuse_meta_exist(PREFIX . '_page_slider_data_sliderdata_1', $post->ID))
			{
			?>
			<div id="slides">
				<div class="slides_container">
					<?php $k = 1; while(tfuse_meta_exist(PREFIX . '_page_slider_data_sliderdata_' . $k, $post->ID))
					{
						$slideArr = get_post_meta($post->ID, PREFIX . '_page_slider_data_sliderdata_' . $k, true);
						$k++;
						$slideArr["slider_page_title"] = tfuse_qtranslate($slideArr["slider_page_title"]);
                    ?>

                        <div>
                        <?php if($slideArr["slider_page_link"] != '') { ?>
                            <a href="<?php echo $slideArr["slider_page_link"]; ?>" target="<?php echo $slideArr["slider_page_target"]; ?>"><?php tfuse_get_image(960, 379, 'img', $slideArr['img'], '', false, 'slider-img') ?></a>
                        <?php } else { ?>
                            <?php tfuse_get_image(960, 379, 'img', $slideArr['img'], '', false, 'slider-image'); ?>
                        <?php } ?>
                            <?php if ( $slideArr["slider_page_title"]!='' ) { ?><div class="caption"><p><?php echo $slideArr["slider_page_title"]; ?></p></div><?php } ?>
                        </div>

					<?php
					}
					?>
				</div>
				<a href="#" class="prev">Previous</a>
				<a href="#" class="next">Next</a>		
			</div>
			<?php
			}
            ?>
			
		</div>   
		<!--/ header slider -->    
			
	<?php } ?>
	</div>
	<div class="header-line"></div>
<?php } ?>

<div class="middle">
<div class="container <?php if ($disable_header == 'true') { ?>padding50px<?php } ?>">

	<?php 
		if ( get_post_meta($post->ID, PREFIX.'_page_sidebar_position', true)==1 ) { $content = 'suffix_1'; $sidebar = ''; }
	elseif ( get_post_meta($post->ID, PREFIX.'_page_sidebar_position', true)==2 ) { $content = ''; $sidebar = 'suffix_1'; }
	elseif ( get_option(PREFIX.'_sidebar_position')==1 ) { $content = ''; $sidebar = 'suffix_1'; }
	else { $content = 'suffix_1'; $sidebar = ''; }
	?>

	<?php if ($disable_header == 'false') {  

		if ( get_post_meta($post->ID, PREFIX . "_page_welcome_bar_source", true) == 'header1' ) {
		
			$page_image 		= get_post_meta($post->ID, PREFIX . "_page_header_image", true);
			$page_image_url 	= get_post_meta($post->ID, PREFIX . "_page_header_image_url", true);
			$page_image_target 	= get_post_meta($post->ID, PREFIX . "_page_header_image_url_target", true);
			$page_title 		= tfuse_qtranslate( get_post_meta($post->ID, PREFIX . "_page_header_title", true) );
?>
			<?php if ($disable_header == 'false') { ?>
			<div class="header-title-image">
				<?php if ( $page_image!='' && $page_image_url!='' ) { ?><div class="image"><a href="<?php echo $page_image_url; ?>" target="<?php echo $page_image_target; ?>"><?php tfuse_get_image(708, 124, 'img', $page_image, '', false, 'slider-image'); ?></a></div><?php } 
				  elseif ( $page_image!='' ) { ?><div class="image"><?php tfuse_get_image(708, 124, 'img', $page_image, '', false, 'slider-image'); ?></div><?php } ?>
				<?php if ( $page_title!='' ) { ?><h1><?php echo $page_title ?></h1><?php } ?>
			</div>
			<?php } 
			
		}
        elseif(tfuse_meta_exist(PREFIX . '_page_images_data_sliderdata_1', $post->ID))
		{
		?>
			<!-- baners top -->
			<div class="baners_top">
				<?php $k = 1; while(tfuse_meta_exist(PREFIX . '_page_images_data_sliderdata_' . $k, $post->ID))
				{
					$slideArr = get_post_meta($post->ID, PREFIX . '_page_images_data_sliderdata_' . $k, true);
					$k++;
					$image_page_title = tfuse_qtranslate($slideArr["image_page_title"]);
				?>
					<div class="baner-item">
						<div class="baner-img">
						<?php
						if($slideArr["image_page_link"] != '')
						{
						?>
							<a href="<?php echo $slideArr["image_page_link"] ?>" target="<?php echo $slideArr["image_page_target"]; ?>"><?php tfuse_get_image(230, 73, 'img', $slideArr['img'], '', false, 'slider-img') ?></a>
						<?php
						} else { ?>
							<?php tfuse_get_image(230, 73, 'img', $slideArr['img'], '', false, 'slider-img') ?>
						<?php } ?>
						</div>
						<h2><?php echo $image_page_title ?></h2>
					</div>
				<?php
				}
				?>
			</div>
			<!--/ baners top -->

		<?php
		}
	}
	?>
	
	<!-- middle content -->
	<div class="container_24">
	
		<?php if ( $sidebar!=='' ) { ?>
			<!-- sidebar -->
			<div class="grid_6 <?php echo $sidebar; ?>">
				<?php get_sidebar(); ?>
			</div>
			<!--/ sidebar -->
		<?php } 

		wp_reset_query();
		
		$paged = ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1;  
		
		query_posts("cat=".$cat."&paged=$paged");  

		?>
		
    	<div class="grid_17 <?php echo $content; ?>">
		  <div class="text">
			<?php if(have_posts()) : $count = 0; ?>
				<?php
					//top area for shortcodes
					$category_content_top = tfuse_qtranslate( get_option(PREFIX . '_category_content_top_' . $cat_ID) );
					if($category_content_top)
					{
						echo $category_content_top = apply_filters('themefuse_shortcodes',$category_content_top);
						echo '<div class="divider_space"></div>';
					}
				?>
			<?php while(have_posts()) : the_post(); $count++; ?>
			
			    <div class="news-item">
	                <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
					<div class="entry">
						<?php the_excerpt(); ?>
       				</div>
                    <div class="news-meta"><a href="<?php the_permalink() ?>" class="link-more alignleft">Find out more</a> 
					<?php if(get_post_meta($post->ID, PREFIX . "_post_single_comments", true) != 'true') { ?>
					<a href="<?php comments_link(); ?>"><?php comments_number('0 comments', '1 comment', '% comments') ?> </a></div>
					<?php } ?>
              	</div>

			<?php endwhile; ?>

				<?php
					//bottom area for shortcodes
					$category_content_bottom = get_option(PREFIX . '_category_content_bottom_' . $cat_ID);
					if($category_content_bottom)
					{
						$category_content_bottom = html_entity_decode($category_content_bottom ,ENT_QUOTES, 'UTF-8');
						echo $category_content_bottom = apply_filters('themefuse_shortcodes',$category_content_bottom);
						echo '<div class="divider_space"></div>';
					}
				?>
				
				<?php tfuse_pagination(); ?>
					
			<?php else: ?>
			
				<h5><?php _e('Sorry, no posts matched your criteria.', 'tfuse') ?></h5>
				
			<?php endif;  wp_reset_query(); ?>
		   </div>
		</div>
		
		<?php if ( $content!='' ) { ?>
			<!-- sidebar -->
			<div class="grid_6 <?php echo $sidebar; ?>">
				<?php get_sidebar(); ?>
			</div>
			<!--/ sidebar -->
		<?php } ?>
	
		<div class="clear"></div>
	</div>
	<!--/ middle content -->
        
<?php get_footer(); ?>