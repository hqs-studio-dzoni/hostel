<?php 
/*
Template Name: Full Width
*/

$template = 'full_width';
if ( get_post_meta($post->ID, PREFIX.'_page_enable_slider', true)=='true' ) $slider = true; else $slider = false;
$template_directory = get_bloginfo('template_directory');
get_header();

	$disable_header = get_post_meta($post->ID, PREFIX . "_page_disable_header_welcome_bar", true);
	$disable_slider = get_post_meta($post->ID, PREFIX . "_page_disable_slider", true);
	if(tfuse_meta_exist(PREFIX . '_page_slider_data_sliderdata_1', $post->ID)) $disable_slider == 'false'; else $disable_slider = 'true';
?>

<?php if ( $disable_slider == 'false' || $disable_header == 'false' ) { ?> 
	<div class="header <?php if ($disable_slider == 'false') { echo 'homepage'; } ?>">
	<!-- header slider -->
	<?php if ($disable_slider == 'false') { ?>
	
		<div class="container">
		
			<?php
			if(tfuse_meta_exist(PREFIX . '_page_slider_data_sliderdata_1', $post->ID))
			{
			?>
			<div id="slides">
				<div class="slides_container">
					<?php $k = 1; while(tfuse_meta_exist(PREFIX . '_page_slider_data_sliderdata_' . $k, $post->ID))
					{
						$slideArr = get_post_meta($post->ID, PREFIX . '_page_slider_data_sliderdata_' . $k, true);
						$k++;
						$slideArr["slider_page_title"] = tfuse_qtranslate($slideArr["slider_page_title"]);
                    ?>

                        <div>
                        <?php if($slideArr["slider_page_link"] != '') { ?>
                            <a href="<?php echo $slideArr["slider_page_link"]; ?>" target="<?php echo $slideArr["slider_page_target"]; ?>"><?php tfuse_get_image(960, 379, 'img', $slideArr['img'], '', false, 'slider-img') ?></a>
                        <?php } else { ?>
                            <?php tfuse_get_image(960, 379, 'img', $slideArr['img'], '', false, 'slider-image'); ?>
                        <?php } ?>
                            <?php if ( $slideArr["slider_page_title"]!='' ) { ?><div class="caption"><p><?php echo $slideArr["slider_page_title"]; ?></p></div><?php } ?>
                        </div>

					<?php
					}
					?>
				</div>
				<a href="#" class="prev">Previous</a>
				<a href="#" class="next">Next</a>		
			</div>
			<?php
			}
            ?>
			
		</div>   
		<!--/ header slider -->    
			
	<?php } ?>
	</div>
	<div class="header-line"></div>
<?php } ?>

<div class="middle">
<div class="container <?php if ($disable_header == 'true') { ?>padding50px<?php } ?>">

	<?php if ($disable_header == 'false') {  

		if ( get_post_meta($post->ID, PREFIX . "_page_welcome_bar_source", true) == 'header1' ) {
		
			$page_image 		= get_post_meta($post->ID, PREFIX . "_page_header_image", true);
			$page_image_url 	= get_post_meta($post->ID, PREFIX . "_page_header_image_url", true);
			$page_image_target 	= get_post_meta($post->ID, PREFIX . "_page_header_image_url_target", true);
			$page_title 		= tfuse_qtranslate( get_post_meta($post->ID, PREFIX . "_page_header_title", true) );
			
			?>	
			<?php if ($disable_header == 'false') { ?>
			<div class="header-title-image">
				<?php if ( $page_image!='' && $page_image_url!='' ) { ?><div class="image"><a href="<?php echo $page_image_url; ?>" target="<?php echo $page_image_target; ?>"><?php tfuse_get_image(708, 124, 'img', $page_image, '', false, 'slider-image'); ?></a></div><?php } 
				  elseif ( $page_image!='' ) { ?><div class="image"><?php tfuse_get_image(708, 124, 'img', $page_image, '', false, 'slider-image'); ?></div><?php } ?>
				<?php if ( $page_title!='' ) { ?><h1><?php echo $page_title; ?></h1><?php } ?>
			</div>
			<?php } 
			
		}
        elseif(tfuse_meta_exist(PREFIX . '_page_images_data_sliderdata_1', $post->ID))
		{
		?>
			<!-- baners top -->
			<div class="baners_top">
				<?php $k = 1; while(tfuse_meta_exist(PREFIX . '_page_images_data_sliderdata_' . $k, $post->ID))
				{
					$slideArr = get_post_meta($post->ID, PREFIX . '_page_images_data_sliderdata_' . $k, true);
					$k++;
					$image_page_title = tfuse_qtranslate($slideArr["image_page_title"]);
				?>
					<div class="baner-item">
						<div class="baner-img">
						<?php
						if($slideArr["image_page_link"] != '')
						{
						?>
							<a href="<?php echo $slideArr["image_page_link"] ?>" target="<?php echo $slideArr["image_page_target"]; ?>"><?php tfuse_get_image(230, 73, 'img', $slideArr['img'], '', false, 'slider-img') ?></a>
						<?php 
						} else { ?>
							<?php tfuse_get_image(230, 73, 'img', $slideArr['img'], '', false, 'slider-img') ?>
						<?php } ?>
						</div>
						<h2><?php echo $image_page_title ?></h2>
					</div>
				<?php
				}
				?>
			</div>    
			<!--/ baners top -->
		
		<?php 
		}
	}
	?>
	
	<!-- middle content -->
	<div class="container_24">
	
		<?php if (have_posts()) : $count = 0; ?>
		<?php while (have_posts()) : the_post(); $count++; ?>	
			
			<!-- content -->
			  <div class="text">
					<?php the_content(); ?>
					&nbsp
					<?php if ( get_post_meta($post->ID, PREFIX."_page_single_comments", true)=='true' ) { comments_template(); } ?>
			  </div>
			<!--/ content -->

		<?php endwhile; else: ?>
	
			<!-- content -->
			  <div class="text">
					<?php _e('Sorry, no posts matched your criteria.', 'tfuse') ?>
			  </div>
			<!--/ content -->
			
		<?php endif; ?>

		<div class="clear"></div>
	</div>
	<!--/ middle content -->
    
<?php get_footer(); ?>