<?php
/*
Template Name: Reservation_2
*/

$error = true; if(isset($_POST['email'])) { include(THEME_FUNCTIONS . '/sendmail.php'); }
if ( get_post_meta($post->ID, PREFIX.'_page_enable_slider', true)=='true' ) $slider = true; else $slider = false;
$template_directory = get_bloginfo('template_directory');
get_header();

	$disable_header = get_post_meta($post->ID, PREFIX . "_page_disable_header_welcome_bar", true);
	$disable_slider = get_post_meta($post->ID, PREFIX . "_page_disable_slider", true);
	if(tfuse_meta_exist(PREFIX . '_page_slider_data_sliderdata_1', $post->ID)) $disable_slider == 'false'; else $disable_slider = 'true';
?>

<?php if ( $disable_slider == 'false' || $disable_header == 'false' ) { ?> 
	<div class="header <?php if ($disable_slider == 'false') { echo 'homepage'; } ?>">
	<!-- header slider -->
	<?php if ($disable_slider == 'false') { ?>
	
		<div class="container">
		
			<?php
			if(tfuse_meta_exist(PREFIX . '_page_slider_data_sliderdata_1', $post->ID))
			{
			?>
			<div id="slides">
				<div class="slides_container">
					<?php $k = 1; while(tfuse_meta_exist(PREFIX . '_page_slider_data_sliderdata_' . $k, $post->ID))
					{
						$slideArr = get_post_meta($post->ID, PREFIX . '_page_slider_data_sliderdata_' . $k, true);
						$k++;
						$slideArr["slider_page_title"] = tfuse_qtranslate($slideArr["slider_page_title"]);
                    ?>

                        <div>
                        <?php if($slideArr["slider_page_link"] != '') { ?>
                            <a href="<?php echo $slideArr["slider_page_link"]; ?>" target="<?php echo $slideArr["slider_page_target"]; ?>"><?php tfuse_get_image(960, 379, 'img', $slideArr['img'], '', false, 'slider-img') ?></a>
                        <?php } else { ?>
                            <?php tfuse_get_image(960, 379, 'img', $slideArr['img'], '', false, 'slider-image'); ?>
                        <?php } ?>
                            <?php if ( $slideArr["slider_page_title"]!='' ) { ?><div class="caption"><p><?php echo $slideArr["slider_page_title"]; ?></p></div><?php } ?>
                        </div>

					<?php
					}
					?>
				</div>
				<a href="#" class="prev">Previous</a>
				<a href="#" class="next">Next</a>		
			</div>
			<?php
			}
            ?>
			
		</div>   
		<!--/ header slider -->    
			
	<?php } ?>
	</div>
	<div class="header-line"></div>
<?php } ?>

<div class="middle">
<div class="container <?php if ($disable_header == 'true') { ?>padding50px<?php } ?>">

	<?php 
		if ( get_post_meta($post->ID, PREFIX.'_page_sidebar_position', true)==1 ) { $content = 'suffix_1'; $sidebar = ''; }
	elseif ( get_post_meta($post->ID, PREFIX.'_page_sidebar_position', true)==2 ) { $content = ''; $sidebar = 'suffix_1'; }
	elseif ( get_option(PREFIX.'_sidebar_position')==1 ) { $content = ''; $sidebar = 'suffix_1'; }
	else { $content = 'suffix_1'; $sidebar = ''; }
	?>

	<?php if ($disable_header == 'false') {  

		if ( get_post_meta($post->ID, PREFIX . "_page_welcome_bar_source", true) == 'header1' ) {
		
			$page_image 		= get_post_meta($post->ID, PREFIX . "_page_header_image", true);
			$page_image_url 	= get_post_meta($post->ID, PREFIX . "_page_header_image_url", true);
			$page_image_target 	= get_post_meta($post->ID, PREFIX . "_page_header_image_url_target", true);
			$page_title 		= tfuse_qtranslate( get_post_meta($post->ID, PREFIX . "_page_header_title", true) );
?>
			<?php if ($disable_header == 'false') { ?>
			<div class="header-title-image">
				<?php if ( $page_image!='' && $page_image_url!='' ) { ?><div class="image"><a href="<?php echo $page_image_url; ?>" target="<?php echo $page_image_target; ?>"><?php tfuse_get_image(708, 124, 'img', $page_image, '', false, 'slider-image'); ?></a></div><?php } 
				  elseif ( $page_image!='' ) { ?><div class="image"><?php tfuse_get_image(708, 124, 'img', $page_image, '', false, 'slider-image'); ?></div><?php } ?>
				<?php if ( $page_title!='' ) { ?><h1><?php echo $page_title ?></h1><?php } ?>
			</div>
			<?php } 
			
		}
        elseif(tfuse_meta_exist(PREFIX . '_page_images_data_sliderdata_1', $post->ID))
		{
		?>
			<!-- baners top -->
			<div class="baners_top">
				<?php $k = 1; while(tfuse_meta_exist(PREFIX . '_page_images_data_sliderdata_' . $k, $post->ID))
				{
					$slideArr = get_post_meta($post->ID, PREFIX . '_page_images_data_sliderdata_' . $k, true);
					$k++;
					$image_page_title = tfuse_qtranslate($slideArr["image_page_title"]);
				?>
					<div class="baner-item">
						<div class="baner-img">
						<?php
						if($slideArr["image_page_link"] != '')
						{
						?>
							<a href="<?php echo $slideArr["image_page_link"] ?>" target="<?php echo $slideArr["image_page_target"]; ?>"><?php tfuse_get_image(230, 73, 'img', $slideArr['img'], '', false, 'slider-img') ?></a>
						<?php
						} else { ?>
							<?php tfuse_get_image(230, 73, 'img', $slideArr['img'], '', false, 'slider-img') ?>
						<?php } ?>
						</div>
						<h2><?php echo $image_page_title ?></h2>
					</div>
				<?php
				}
				?>
			</div>
			<!--/ baners top -->

		<?php
		}
	}
	?>
	
	<!-- middle content -->
	<div class="container_24">
	
		<?php if ( $sidebar!=='' ) { ?>
			<!-- sidebar -->
			<div class="grid_6 <?php echo $sidebar; ?>">
				<?php get_sidebar(); ?>
			</div>
			<!--/ sidebar -->
		<?php } ?>

		<?php if (have_posts()) : $count = 0; ?>
		<?php while (have_posts()) : the_post(); $count++; ?>	
			
			<!-- content -->
			<div class="grid_17 <?php echo $content; ?>">
			  <div class="text">
				<?php if ( get_the_content()!='' ) the_content(); ?>
                   
                <form  id="commentform" action="" method="post" class="ajax_form reservationForm" name="contactForm">
					<input type="hidden" name="temp_url" value="<?php bloginfo('template_directory'); ?>" />
					<input type="hidden" id="tempcode" name="tempcode" value="<?php echo base64_encode(get_option('admin_email')); ?>" />
					<input type="hidden" id="myblogname" name="myblogname" value="<?php bloginfo('name'); ?>" />
					<?php if (!isset($error) || $error == true){ ?> 
                	<!-- column 1 -->
                    <div class="column_3">
                        <div class="row field_text">
                            <label><?php _e('Your name', 'tfuse') ?>:</label><br />
                            
                            <input name="yourname" value="" id="name" class="inputtext required" size="40" type="text" />
						</div>

                        <div class="row field_text">
                            <label><?php _e('Your phone', 'tfuse') ?>:</label><br />
                            
                            <input name="phone" value="" id="phone" class="inputtext" size="40" type="text" />
						</div>
                        
                        <div class="row field_text">
                            <label><?php _e('Your email address', 'tfuse') ?>:</label><br />
                            
                            <input name="email" value="" id="email" class="inputtext" size="40" type="text" />
						</div>
                    </div>
                    <!--/ column 1 -->
                    
                    <!-- column 2 -->
                <div class="column_3">
                    	<div class="row field_date">
                          <label><?php _e('Choose', 'tfuse') ?> <strong><?php _e('check-in', 'tfuse') ?></strong> <?php _e('date', 'tfuse') ?>:</label><br />
                            <div id="date_in"></div>
                            <input name="date_in_input" value="" id="date_in_input" type="hidden" />
                            <div class="timeimputrow">
                            <label><strong><?php _e('Check-in', 'tfuse') ?></strong> <?php _e('time', 'tfuse') ?>:</label>
                            <input name="time_in_input" value="<?php echo date('h:i A') ?>" id="time_in_input" class="inputtext time" type="text" />
                            </div>
						</div>
                  </div>
                    <!--/ column 2 -->
                    
                    <!-- column 3 -->
                <div class="column_3 omega">
                    	<div class="row field_date">
                            <label><?php _e('Choose', 'tfuse') ?> <strong><?php _e('check-out', 'tfuse') ?></strong> <?php _e('date', 'tfuse') ?>:</label><br />
                            <div id="date_out"></div>
                            <input name="date_out_input" value="" id="date_out_input" type="hidden" />
                            <div class="timeimputrow">
                            <label><strong><?php _e('Check-out', 'tfuse') ?></strong> <?php _e('time', 'tfuse') ?>:</label>
                            <input name="time_in_output" value="<?php echo date('h:i A') ?>" id="time_in_output" class="inputtext time" type="text" />
                            </div>
						</div>
                  </div>
                    <!--/ column 3 -->
                    
                    <div class="clear"></div>
                    
					<div class="row field_textarea <?php if (isset($the_messageclass)) echo $the_messageclass; ?>">
						<label><?php _e('Message', 'tfuse') ?>:</label><br />
						<textarea id="message" name="message" class="textarea textarea_middle" cols="40" rows="10"><?php  if (isset($the_message)) echo $the_message ?></textarea>
					</div>
					<div class="clear"></div>
                    
                    <div class="field_submit">
                        <input type="submit" id="send" value="<?php _e('Send Now', 'tfuse') ?>" class="btn-send" />
                        <p class="notice"><?php _e('Please note that this is not an actual reservation, but only a request for one.', 'tfuse') ?> <strong><?php _e('We will contact you for a confirmation shortly after. Thank you!', 'tfuse') ?></strong></p>
                    </div>
	
					<?php } else { ?> 
					
						<br>
						<h2 style="width:100%;"><?php _e('Your message has been sent!', 'tfuse') ?></h2>
						<div class="confirm">
							<p class="textconfirm"><br /><?php _e('Thank you for contacting us,', 'tfuse') ?><br/><?php _e('We will get back to you within 2 business days.', 'tfuse') ?></p>
						</div>
				
					<?php } ?>
                </form>
				<?php if ( get_post_meta($post->ID, PREFIX."_page_single_comments", true)=='true' ) { comments_template(); } ?>
			  </div>
			</div>
			<!--/ content -->

		<?php endwhile; else: ?>
	
			<!-- content -->
			<div class="grid_17 <?php echo $content; ?>">
			  <div class="text">
					<?php _e('Sorry, no posts matched your criteria.', 'tfuse') ?>
			  </div>
			</div>
			<!--/ content -->
			
		<?php endif; ?>
		
		<?php if ( $content!='' ) { ?>
			<!-- sidebar -->
			<div class="grid_6 <?php echo $sidebar; ?>">
				<?php get_sidebar(); ?>
			</div>
			<!--/ sidebar -->
		<?php } ?>
	
		<div class="clear"></div>
	</div>
	<!--/ middle content -->
    
<?php get_footer(); ?>