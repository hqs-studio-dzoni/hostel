<?php
/*
Template Name: Reservation
*/

$error = true; if(isset($_POST['email'])) { include(THEME_FUNCTIONS . '/sendmail.php'); }
if ( get_post_meta($post->ID, PREFIX.'_page_enable_slider', true)=='true' ) $slider = true; else $slider = false;
$template_directory = get_bloginfo('template_directory');
get_header();

	$disable_header = get_post_meta($post->ID, PREFIX . "_page_disable_header_welcome_bar", true);
	$disable_slider = get_post_meta($post->ID, PREFIX . "_page_disable_slider", true);
	if(tfuse_meta_exist(PREFIX . '_page_slider_data_sliderdata_1', $post->ID)) $disable_slider == 'false'; else $disable_slider = 'true';
?>

<?php if ( $disable_slider == 'false' || $disable_header == 'false' ) { ?> 
	<div class="header <?php if ($disable_slider == 'false') { echo 'homepage'; } ?>">
	<!-- header slider -->
	<?php if ($disable_slider == 'false') { ?>
	
		<div class="container">
		
			<?php
			if(tfuse_meta_exist(PREFIX . '_page_slider_data_sliderdata_1', $post->ID))
			{
			?>
			<div id="slides">
				<div class="slides_container">
					<?php $k = 1; while(tfuse_meta_exist(PREFIX . '_page_slider_data_sliderdata_' . $k, $post->ID))
					{
						$slideArr = get_post_meta($post->ID, PREFIX . '_page_slider_data_sliderdata_' . $k, true);
						$k++;
						$slideArr["slider_page_title"] = tfuse_qtranslate($slideArr["slider_page_title"]);
                    ?>

                        <div>
                        <?php if($slideArr["slider_page_link"] != '') { ?>
                            <a href="<?php echo $slideArr["slider_page_link"]; ?>" target="<?php echo $slideArr["slider_page_target"]; ?>"><?php tfuse_get_image(960, 379, 'img', $slideArr['img'], '', false, 'slider-img') ?></a>
                        <?php } else { ?>
                            <?php tfuse_get_image(960, 379, 'img', $slideArr['img'], '', false, 'slider-image'); ?>
                        <?php } ?>
                            <?php if ( $slideArr["slider_page_title"]!='' ) { ?><div class="caption"><p><?php echo $slideArr["slider_page_title"]; ?></p></div><?php } ?>
                        </div>

					<?php
					}
					?>
				</div>
				<a href="#" class="prev">Previous</a>
				<a href="#" class="next">Next</a>		
			</div>
			<?php
			}
            ?>
			
		</div>   
		<!--/ header slider -->    
			
	<?php } ?>
	</div>
	<div class="header-line"></div>
<?php } ?>

<div class="middle">
<div class="container <?php if ($disable_header == 'true') { ?>padding50px<?php } ?>">

	<?php 
		if ( get_post_meta($post->ID, PREFIX.'_page_sidebar_position', true)==1 ) { $content = 'suffix_1'; $sidebar = ''; }
	elseif ( get_post_meta($post->ID, PREFIX.'_page_sidebar_position', true)==2 ) { $content = ''; $sidebar = 'suffix_1'; }
	elseif ( get_option(PREFIX.'_sidebar_position')==1 ) { $content = ''; $sidebar = 'suffix_1'; }
	else { $content = 'suffix_1'; $sidebar = ''; }
	?>

	<?php if ($disable_header == 'false') {  

		if ( get_post_meta($post->ID, PREFIX . "_page_welcome_bar_source", true) == 'header1' ) {
		
			$page_image 		= get_post_meta($post->ID, PREFIX . "_page_header_image", true);
			$page_image_url 	= get_post_meta($post->ID, PREFIX . "_page_header_image_url", true);
			$page_image_target 	= get_post_meta($post->ID, PREFIX . "_page_header_image_url_target", true);
			$page_title 		= tfuse_qtranslate( get_post_meta($post->ID, PREFIX . "_page_header_title", true) );
?>
			<?php if ($disable_header == 'false') { ?>
			<div class="header-title-image">
				<?php if ( $page_image!='' && $page_image_url!='' ) { ?><div class="image"><a href="<?php echo $page_image_url; ?>" target="<?php echo $page_image_target; ?>"><?php tfuse_get_image(708, 124, 'img', $page_image, '', false, 'slider-image'); ?></a></div><?php } 
				  elseif ( $page_image!='' ) { ?><div class="image"><?php tfuse_get_image(708, 124, 'img', $page_image, '', false, 'slider-image'); ?></div><?php } ?>
				<?php if ( $page_title!='' ) { ?><h1><?php echo $page_title ?></h1><?php } ?>
			</div>
			<?php } 
			
		}
        elseif(tfuse_meta_exist(PREFIX . '_page_images_data_sliderdata_1', $post->ID))
		{
		?>
			<!-- baners top -->
			<div class="baners_top">
				<?php $k = 1; while(tfuse_meta_exist(PREFIX . '_page_images_data_sliderdata_' . $k, $post->ID))
				{
					$slideArr = get_post_meta($post->ID, PREFIX . '_page_images_data_sliderdata_' . $k, true);
					$k++;
					$image_page_title = tfuse_qtranslate($slideArr["image_page_title"]);
				?>
					<div class="baner-item">
						<div class="baner-img">
						<?php
						if($slideArr["image_page_link"] != '')
						{
						?>
							<a href="<?php echo $slideArr["image_page_link"] ?>" target="<?php echo $slideArr["image_page_target"]; ?>"><?php tfuse_get_image(230, 73, 'img', $slideArr['img'], '', false, 'slider-img') ?></a>
						<?php
						} else { ?>
							<?php tfuse_get_image(230, 73, 'img', $slideArr['img'], '', false, 'slider-img') ?>
						<?php } ?>
						</div>
						<h2><?php echo $image_page_title ?></h2>
					</div>
				<?php
				}
				?>
			</div>
			<!--/ baners top -->

		<?php
		}
	}
	?>
	
	<!-- middle content -->
	<div class="container_24">
	
		<?php if ( $sidebar!=='' ) { ?>
			<!-- sidebar -->
			<div class="grid_6 <?php echo $sidebar; ?>">
				<?php get_sidebar(); ?>
			</div>
			<!--/ sidebar -->
		<?php } ?>

		<?php if (have_posts()) : $count = 0; ?>
		<?php while (have_posts()) : the_post(); $count++; ?>	
			
			<!-- content -->
			<div class="grid_17 <?php echo $content; ?>">
			  <div class="text">
				<?php if ( get_the_content()!='' ) the_content(); ?>
                   
                <form  id="commentform" action="" method="post" class="ajax_form reservationForm" name="contactForm">
					<input type="hidden" name="temp_url" value="<?php bloginfo('template_directory'); ?>" />
					<input type="hidden" id="tempcode" name="tempcode" value="<?php echo base64_encode(get_option('admin_email')); ?>" />
					<input type="hidden" id="myblogname" name="myblogname" value="<?php bloginfo('name'); ?>" />
					<?php if (!isset($error) || $error == true){ ?> 
                	<!-- column 1 -->
                    <div class="column_3">
                        <div class="row field_text">
                            <label><?php _e('Your name', 'tfuse') ?>:</label><br />
                            
                            <input name="yourname" value="" id="name" class="inputtext required" size="40" type="text" />
						</div>

                        <div class="row field_text">
                            <label><?php _e('Your phone', 'tfuse') ?>:</label><br />
                            
                            <input name="phone" value="" id="phone" class="inputtext" size="40" type="text" />
						</div>
                        
                        <div class="row field_text">
                            <label><?php _e('Your email address', 'tfuse') ?>:</label><br />
                            
                            <input name="email" value="" id="email" class="inputtext" size="40" type="text" />
						</div>
						<!-- Number of nights -->
						<div class="row field_text">
                            <label>Number of nights staying:</label><br />
                            
                            <input name="numOfNights" value="" id="numOfNights" class="inputtext" placeholder="Select from calendars" size="40" type="text" />
						</div>
						<div class="row field_text">
                            <label>Number of guests:</label><br />
                            
                            <input name="numOfGuests" value="" id="numOfGuests" class="inputtext" size="40" type="text" />
						</div>
						<!-- Room selection -->
						<div class="row field_select alignleft">
							<label>Select a room:</label><br />
							<select class="select_styled" name="contact_select_1" id="contact_select_1">
								<option value="">Select a room</option>
								<option value="8-bed, <?php echo get_option('8_bed'); ?>">Paul Gauguin 8-bed mixed dorm - <?php echo get_option('8_bed') ?></option>
								<option value="6-bed">Toulouse Lautrec 6-bed mixed dorm - <?php echo get_option('6_bed') ?></option>
								<option value="4-bed">Van Gogh 4-bed mixed dorm - <?php echo get_option('4_bed') ?></option>
								<option value="private">Pablo Picasso private twin/triple room - <?php echo get_option('private_twin_triple') ?></option>
								<option value="en-suite">Montmartre En Suite for one person - <?php echo get_option('mont_for_one') ?></option>
								<option value="en-suite">Montmartre En Suite for two persons - <?php echo get_option('mont_for_two') ?></option>
							</select>
						</div>
                    </div>
                    <!--/ column 1 -->
                    
                    <!-- column 2 -->
                <div class="column_3">
                    	<div class="row field_date">
                          <label><?php _e('Choose', 'tfuse') ?> <strong><?php _e('check-in', 'tfuse') ?></strong> <?php _e('date', 'tfuse') ?>:</label><br />
                            <div id="date_in"></div>
                            <input name="arival_date" value="" id="date_in_input" type="hidden" />
						</div>
                  </div>
                    <!--/ column 2 -->
                    
                    <!-- column 3 -->
                <div class="column_3 omega">
                    	<div class="row field_date">
                          <label><?php _e('Choose', 'tfuse') ?> <strong><?php _e('check-out', 'tfuse') ?></strong> <?php _e('date', 'tfuse') ?>:</label><br />
                            <div id="date_out"></div>
                          <input name="checkout_date" value="" id="date_out_input" type="hidden" onclick="dateDiff();" />
						</div>
                  </div>
                    <!--/ column 3 -->
   				  
   				  <!-- Disscount calculation output -->
   				  <p id="disscount"></p>
                                  <div class="price_container">
					<p id="price"></p>
					<a id="display" href="#">Click to see/refresh total price</a>
				  </div>
				  <!-- Disscount calculation output end-->

                    
                    <div class="clear"></div>
                    
					<div class="row field_textarea <?php if (isset($the_messageclass)) echo $the_messageclass; ?>">
						<label><?php _e('Message', 'tfuse') ?>:</label><br />
						<textarea id="message" name="message" class="textarea textarea_middle" cols="40" rows="10"><?php  if (isset($the_message)) echo $the_message ?></textarea>
                                                <label class="checkme">
                                                    <input class="styleme" type="checkbox" name="transfer" value="transfer">Include transfer from airport (+20&#8364;)
                                                </label>
					</div>
					<div class="clear"></div>
                    
                    <div class="field_submit">
                        <input type="submit" id="send" value="<?php _e('Send Now', 'tfuse') ?>" class="btn-send" />
                        <p class="notice"><?php _e('Please note that this is not an actual reservation, but only a request for one.', 'tfuse') ?> <strong><?php _e('We will contact you for a confirmation shortly after. Thank you!', 'tfuse') ?></strong></p>
                    </div>
	
					<?php } else { ?> 
					
						<br>
						<h2 style="width:100%;"><?php _e('Your message has been sent!', 'tfuse') ?></h2>
						<div class="confirm">
							<p class="textconfirm"><br /><?php _e('Thank you for contacting us,', 'tfuse') ?><br/><?php _e('We will get back to you as soon as possible.', 'tfuse') ?></p>
						</div>
				
					<?php } ?>
                </form>

				<?php if ( get_post_meta($post->ID, PREFIX."_page_single_comments", true)=='true' ) { comments_template(); } ?>
			  </div>
			</div>
			<!--/ content -->

		<?php endwhile; else: ?>
	
			<!-- content -->
			<div class="grid_17 <?php echo $content; ?>">
			  <div class="text">
					<?php _e('Sorry, no posts matched your criteria.', 'tfuse') ?>
			  </div>
			</div>
			<!--/ content -->
			
		<?php endif; ?>
		
		<?php if ( $content!='' ) { ?>
			<!-- sidebar -->
			<div class="grid_6 <?php echo $sidebar; ?>">
				<?php get_sidebar(); ?>
			</div>
			<!--/ sidebar -->
		<?php } ?>
	
		<div class="clear"></div>
	</div>
	<!--/ middle content -->
	<script type="text/javascript">
	//Show the calculated price to guest function
		$("#display").click(function(event) {
			event.preventDefault();
  			var nights = +$('input[name=numOfNights]').val();//get the # of nights
  			var guests = +$('input[name=numOfGuests]').val();//get the # of guests staying
  			var selRoomString = $("#contact_select_1 option:selected").text();//The select option data string.
  			var roomPrice = parseInt(selRoomString.match(/€([^ ]*)/)[1], 10);//select option data price only
  			  			
  			//No disscount math
  			var noDisscount = nights * guests * roomPrice;
  			  			
  			//Display logic
  			if (nights != 0 && nights < 7 ) {
  				//No disscount display
  				document.getElementById("price").innerHTML="Total price is " + noDisscount + " &euro;";
  			};

  			if (nights > 7) {
  				//10% disscount math and display
  				var disscountTen = noDisscount - (nights * guests * roomPrice)/100*10;
  				document.getElementById("price").innerHTML="Total price with 10% disccount is " + disscountTen + " &euro;";
  			};

  			if ( nights > 15) {
  				//20% disscount math and display
  				var disscountTwenty = noDisscount - (nights * guests * roomPrice)/100*20;
  				document.getElementById("price").innerHTML="Total price with 20% disccount is " + disscountTwenty + " &euro;";
  			};
  			
		});
	</script>

	<script>
    window.onclick = dateDiff;
        function dateDiff()
        {
            /// get 1st date to variable d1 - variable is string at the moment
            d1 = $('#date_in_input').val();

            /// get 2nd date to variable d2 - variable is string at the moment
            d2 = $('#date_out_input').val();

            // set 1st date, cut parts of d1 string and use them to create date object named date1
            var date1 = new Date();
            date1.setYear    (parseInt(d1.substr(6, 10), 10)); //set year
            date1.setMonth   (parseInt(d1.substr(3, 5), 10)-1);//set month (months starts from 1 not 0, thats why its -1
            date1.setDate    (parseInt(d1.substr(0, 2), 10));//set day

            // set 2nd date, cut parts of d2 string and use them to create date object named date2
            var date2 = new Date();
            date2.setYear    (parseInt(d2.substr(6, 10), 10));//set year
            date2.setMonth   (parseInt(d2.substr(3, 5), 10)-1);//set month
            date2.setDate    (parseInt(d2.substr(0, 2), 10));//set day

            var diff = (date2-date1)/1000/60/60/24;  // difference between two dates in milliseconds
            var display = Math.round(diff).toFixed(0); // sometimes it bugs and show some decimals, thats why we cut em here just in case
            //console.log(display);//and here we fuking go!
            document.getElementById("numOfNights").value = display;

            //Let's show them what they get by checking in for more than 5 nights or 15 nights
            if (display >= 7 && display < 15) {
            	var message1 = "You will get a 10% disscount!"
            } else if (display < 7) {
            	var message1 = "";
            } else if (display >= 15) {
            	var message1 = "WOW, You will get a 20% disscount";
            };
    		document.getElementById("disscount").innerHTML=message1;
    		var fancy = document.getElementById("disscount");
			fancy.style.opacity = "1";
        }
</script>
    
<?php get_footer(); ?>